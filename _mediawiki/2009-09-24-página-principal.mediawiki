<big>'''MediaWiki ha sido instalado con éxito.'''</big>

Consulta la [http://meta.wikimedia.org/wiki/Help:Contents Guía de usuario] para obtener información sobre el uso del software wiki.

== Empezando ==

* [http://www.mediawiki.org/wiki/Manual:Configuration_settings Lista de ajustes de configuración]
* [http://www.mediawiki.org/wiki/Manual:FAQ PMF sobre MediaWiki]
* [https://lists.wikimedia.org/mailman/listinfo/mediawiki-announce Lista de correo de anuncios de distribución de MediaWiki]