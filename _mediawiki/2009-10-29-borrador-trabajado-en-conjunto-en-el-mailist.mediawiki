-Intro:

Un fenómeno masivo, fenómeno que nadie hace nada por controlar, y la
disposición penal no sirve para nada. (Zafaroni)
Quema de Libros por parte de Agrupaciones Anacrónicas.
Hakim Bey me parece excelente para la intro, si alguien que ya lo
tiene bien leido puede poner citas y contexto que sean útiles, estaría
buenisimo.

La sociedad cambia, así como nuestro entendimiento de ella y si nos aferramos al pasado, intentando fútilmente mantener el statu quo en vez de abrazar el cambio y sacar lo mejor de él, la sociedad misma sera la perjudicada. 

Hoy tenemos la posibilidad de aceptar el cambio y darle a la humanidad el regalo mas importante y maravilloso que jamas ha existido. La posibilidad de que desde cualquier lugar del planeta tengamos acceso a toda la cultura, a toda la información, a todo el conocimiento y a todos los individuos de nuestra especie. 

Esta oportunidad es demasiado importante como para dejar que intereses políticos, económicos o ideológicos nos priven aprovecharla, por esto internet debe ser universal, anónimo y neutral. ( propuesto por Rodrigo)



-Sobre la Libre Cultura

-Sobre la Libertad de Información y Expresión

-Sobre los "Aprietes" en Argentina a la Libertad de Información y
Expresión
Caso Potel, Canon Digital, convenio CADRA-UBA y
otras universidades, Caso

-Copyrights:
Reformar los Copyrights; legalizar formalmente el intercambio de
Cultura. Asegura que cuando una obra creativa sea comercializada
masivamente, el beneficiado sea el Artista y no los poseedores de los
derechos amparados en leyes de Restricción y anacronismo Cultural.

-Marcas Registradas:
El abuso de las marcas registradas, como sustituto de o añadido a los
derechos de autor, es algo que no se puede permitir que continue, dado
que mina la confianza del público. Acorde con ésto, el material
sujeto
a derechos de autor no podrá ser susceptible de convertirse en marca
registrada, y del mismo modo las marcas registradas no podrán quedar
sujetas a derechos de autor.

-Patentes:
No al patentamiento de la vida animal o vegetal.

-Sobre el Estado Argentino ( Estado Transparente )
Software libre para la administración pública (obligación de su uso
en
la administracion pública).

-Sobre Derechos del  Individuo (  defensa de la privacidad, etc. )

-Sobre otros temas fuera de este manifiesto y nuestra política
general.