Contra todo intento de estigmatizacion de nuestro nombre, reivindicamos a los Piratas, la Piratería y sus luchas.

Si en aquel mundo dominado por monopolios depredadores, por la esclavitud, la autocracia y el racismo, los Piratas se dieron a si mismos igualdad, libertad, 

autodeterminación y auténtica convivencia multicultural, ignorando toda corrección política de su tiempo, entonces es que reivindicamos su bandera, su legado 

y su nombre. 

Porque nosotros crecimos en la periferia de lo que algunos llaman ilegalidad, navegando caóticos mares culturales, habitando unas pocas y remotas islas liberadas, y no conocemos otra forma de organizamos que no sea bajo un principio de convivencia igualitaria: las Redes de Pares. 

Resistimos también los monopolios, del conocimiento, la cultura, y el patentamiento de la vida, y asi como nos resistimos a las metrópolis del presente que 

depredan los recursos naturales en sus colonias: el planeta entero. 

Y como buenos Piratas y Filibusteros nos parecemos en algo más a nuestros predecesores: asaltamos naves, las del egoísmo y el saqueo, para quebrar la 

opresión de las falsas leyes de la escasez, del copyright y sus artificiales feudos inmateriales, fraudulentos espejismos propagandísticos que nos restringen 

el acceso a este nuevo mundo de la abundancia multicolor, donde todos podemos crear, copiar, multiplicar y compartir. 

Como Piratas que somos creemos en la libertad, en la diversidad, en la auto-organización, en la horizontalidad, en sentirnos pares e iguales, en democratizar 

la creación, en la libertad de expresión y pensamiento, y como nacimos compartiendo, creemos en compartir la mayor riqueza que atesoramos como humanidad: la 

cultura, las ideas y el conocimiento, que liberados del lastre analógico y anacrónico de la escasez, sólo queda liberarlos del pesado lastre de la codicia. 

Como Piratas que somos, no somos ingenuos, y sabemos que las mismas armas digitales que nos liberan, nos pueden encarcelar, cercenar y mutilar. 

Por eso defendemos la privacidad, el anonimato, las redes libres y la neutralidad de Internet, como herramientas de liberación que nos permitan seguir 

sosteniendo nuestra autonomía, capacidad de organizarnos y resistirnos a las leyes cercenantes. 

Como Piratas que somos aprendemos sobre la marcha, construimos sobre la marcha, aprendemos de las históricas luchas por la democratización del conocimiento y 

la libertad de expresión y aprendemos de las luchas por proteger nuestros bienes comunes, los recursos naturales: mares, vientos, llanuras y montañas. 

Aprendemos que la libertad, la solidaridad y el compromiso colectivo son el único camino sustentable para seguir existiendo como seres humanos en este 

planeta.