[[Categoría:Grog&Tor]]
[[Categoría:Organización]]

= Grog&Tor -- All your base are belong to us! =

== Llévame con tu líder ==

Coordinamos de organización a organización a través de los medios oficiales que nos propongan. Por ejemplo ir a una asamblea, hablar con una delegada/referente, etc.

Preferimos hablar con una sola entidad a la vez, para evitar malos entendidos o fallas en la comunicación.

== Condiciones para alojarla ==

* Tiene que haber Internet. Esto es opcional pero altamente recomendado. Podemos acercarnos a conseguir conexión antes del taller.
* No se cobra entrada. Puede haber gorra, barra, etc. para sostener el espacio, pero no podemos pedirle a todas las personas que quieran participar que aporten.
* Aclaramos que nosotras llevamos libros a un precio fijo y stickers y otros materiales a voluntad.
* Se tiene que poder beber grog durante o después en el mismo lugar o mudándonos a algún bar cercano. Preferimos que sea ahí mismo.

== Algunos temas a resolver ==

* ¿Tienen medidas de seguridad para difundir la dirección/lugar?
* ¿Se puede beber en el espacio taller?
* ¿Cuáles son las condiciones de uso del espacio que no están mencionadas acá?

== Cumplimos ==

* Dejar todo tan o más limpio de lo que lo encontramos
* Acuerdos que tengamos durante la organización del taller

== No podemos cumplir ==

* Participación activa en espacios de decisión ajenos. Por ejemplo no participamos en asambleas más allá de lo requerido para coordinar una Grog&Tor. Hacemos muchas cosas como para invadir otros espacios :P
* Condiciones sorpresa. Si no nos dijeron algo de antemano, no podemos comprometernos a cumplirlo.