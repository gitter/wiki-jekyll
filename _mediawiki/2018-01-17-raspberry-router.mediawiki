[[Categoría:Software Libre]]
[[Categoría:Redes Libres]]
[[Categoría:Raspberry]]
[[Categoría:OpenWrt]]

Este artículo es para crear un router basado en Raspberry Pi 3 usando LEDE/OpenWrt.

= Requisitos =

* Computadora de escritorio / Notebook con GNU/Linux
* Raspberry Pi 3
* Monitor HDMI (o adaptador HDMI/VGA y monitor VGA)
* Teclado USB
* Fuente micro USB de 5V y 2.5A, o Powerbank equivalente o cable USB para conectar a la computadora
* Tarjeta microSD de al menos 500MB
* Adaptador SD/USB o lector de tarjetas SD + adaptador SD/microSD

= Instalar LEDE/OpenWrt =

Descargar la imagen de OpenWrt para RPi3:

<code>
wget https://downloads.lede-project.org/releases/17.01.4/targets/brcm2708/bcm2710/lede-17.01.4-brcm2708-bcm2710-rpi-3-ext4-sdcard.img.gz
</code>

En este caso estamos bajando la imagen para la versión 17.01.4 de LEDE.  Para encontrar versiones nuevas, podemos navegar a https://downloads.lede-project.org/releases, buscar la última versión e ir bajando hasta el ''target'' (la imagen específica para el dispositivo) brcm2708 que es el de RPi3 y así hasta que encontramos una imagen que contenga "rpi-3" en el nombre de archivo .img.

Descomprimir la imagen:

<pre>
gunzip lede-17.01.4-brcm2708-bcm2710-rpi-3-ext4-sdcard.img.gz
</pre>

Quemarla en la tarjeta de memoria.  En este caso la tarjeta es <code>/dev/sdb</code>.  Para averiguar cuál es la tarjeta, insertarla y ver el final de <code>dmesg | tail</code>.  También se puede usar <code>dmesg -w</code> para ver la inserción de la tarjeta en vivo.  Si estamos usando el lector de tarjetas SD de la notebook, la tarjeta es algo como <code>/dev/mmlbk0</code>.

'''IMPORTANTE:''' En ningún caso usar <code>/dev/sda</pre> porque seguramente es el disco de la computadora y la vamos a cagar.

<pre>
sudo dd status=progress if=lede-17.01.4-brcm2708-bcm2710-rpi-3-ext4-sdcard.img of=/dev/sdb
</pre>

Donde <code>if=</code> va acompañado del nombre de archivo de la imagen y <code>of=</code> del dispositivo de la tarjeta.

Cuando termine, sacar la tarjeta microSD y colocarla en la RPi3.  Enchufar el monitor y el teclado y '''con la fuente desenchufada''', conectar la fuente.  Luego enchufar la fuente a 220V.