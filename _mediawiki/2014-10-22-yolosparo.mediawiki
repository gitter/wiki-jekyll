== ¿Qué pasó? ==
El Proyecto de Ley de Hidrocarburos ya recibió media sanción en el Senado y de ser aprobado por el Congreso significaría la total entrega de nuestros recursos naturales y la condena de nuestra soberanía energética y salud por las próximas tres generaciones.

El proyecto establece un tope a las regalías del 12%, el más bajo de latinoamérica y uno de los más bajos del mundo, a la vez que pone un tope del 3% a los impuestos provinciales a las petroleras.

Permite a las petroleras aumentar el precio interno hasta precios internacionales y sacar del país el 20% de la producción (60% para la producción marina).

Afianza el fracking en el país, un método sumamente contaminante y dependiente de patentes extranjeras, aumentando los tiempos de concesión a 35 años y permitiéndoles usar y contaminar el agua subterranea.

Finalmente, no controla la explotación, sino que confía en que las petroleras digan la verdad sobre la cantidad extraída, renueva las concesiones sin licitación ni canon alguno y no plantea una reconversión energética a tecnologías renovables.

== Que es Yo Los Paro? ==
Yo los paro es una plataforma desarrollada por el ppar que permite agilizar el contacto entre ciudadanos preocupados por el fracking con diputados atravez de redes sociales, telefonos y otros intermediarios. El fin es apelar a la conciencia de diputados haciendo publica las demanda social por una argentina libre de fracking. // corregir --minitrue


== Bitácora ==

== Actividades /Necesidades ==


== Documentos/links ==

proyecto de ley:
pagina web: yolosparo.org
twitter: https://twitter.com/hoylosparo
facebook:

[[Categoría:2014]]
[[Categoría:Minitrue]]
[[Categoría:Propuestas_en_curso]]
[[Categoría:Propuestas_por_Minitrue]]

[[Categoría:2014]]
[[Categoría:Minitrue]]
[[Categoría:Propuestas_en_curso]]
[[Categoría:Propuestas_por_Minitrue]]