[[Category:Guías]]

==Objetivos para un Barco Callejero==

* Crear un grupo de unas 5 o 6 personas que se junten semanalmente.
* Generar ideas de actividades de difusión en la calle y convocatoria a marchas.
* Llevar una agenda de actividades en la calle, marchas y demás.
* Salir a panfletear a algunos lugares concretos (el [[Barco_de_Comunicación|barco de propaganda]] se encarga de crear los panfletos, posters y comunicados)
* Crecer y dividirse o fomentar otros barcos callejeros.
* Coordinar con otros [[Barco_Callejero|barcos callejeros]] para activar en las marchas.

==Convocatoria original==

Informa en la asamblea general tu intencion de crear un Barco y pedile al Barco de Infraestructura que 

te pase los mails de los inscriptos en tu comuna. A modo de ejemplo la invitación que mandamos para la comuna 3. 

'''A quien confirme pasale tu cel para comunicarse en caso de que no se encuentren.'''

<pre>
Te esta llegando este email por que te anotaste en la Comuna XX en el Partido Pirata.

Los viernes a las 19hs nos reunimos en Plaza XXXX para formar un Barco[0] Callejero.

Los objetivos del Barco Callejero son:

* Crear un grupo de unas 5 o 6 personas que se junten semanalmente.
* Generar ideas de actividades de difusión en la calle y convocatoria a marchas.
* Llevar una agenda de actividades en la calle, marchas, festivales y demás.
* Salir a panfletear a algunos lugares concretos.
* Crear un grupo mas grande o varios pequeños para activar en las marchas.

Si te interesa cualquiera de estos temas y queres participar mas activamente en el Partido
venite este viernes a las 19hs a Plaza XXXX, frente a la XXXX.

Por favor responde este email para confirmar.

[0] Los Barcos Piratas son las unidades de participación y acción directa del Partido Pirata. http://partidopirata.com.ar/documentos/CartaOrganica.html#art%C3%ADculo-11-barcos-piratas
</pre>

==Actividades==

Basadas en las aventuras de la comuna 3.

* En principio nos encontramos en un lugar publico, una plaza es un buen lugar y volanteamos o vamos a algun lado. 
Tengan una forma de identificarse, mantengan el punto de encuentro y tengan los celulares a mano.

* Buenos lugares para volantear son plazas o parques, facultades y colegios; identifiquen los que haya en su comuna. 

* Luego de la volanteada nos vamos a la casa de algun pirata o cafe o lugar tranquilo a donde charlas, tirar ideas y debatir.

::* Siempre conviene llevar una bitacora de las reuniones donde ir anotando los temas para tratarlos todos, volver a ellos y compartirlos con los demas barcos y piratas.

::* En cada reunion debemos dejar establecido el lugar a volantear la proxima semana y ver si tenemos suficientes volantes. De ser necesario diseñar o imprimir volantes comunicarse con el [[Barco_de_Comunicación|barco de propaganda]]

::* Bizcochitos y mates nunca estan de mas. Picada y cerveza puede pintar pero evitemos que se haga vicio!!!