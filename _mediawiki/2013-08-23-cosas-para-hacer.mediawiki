= Cosas que podés hacer para ser pirata =

== Formarte en temas piratas ==

Como piratas valoramos la autoformación y la discusión informada. Para eso tenemos que formarnos y estar al tanto de la última información referida a los temas piratas.

Acá hay una selección de [[Libros|libros]] y [[Blogs|blogs]] que podés seguir. Compartí los tuyos!

== Hablar del Partido Pirata ==

Nadie se banca a lxs densxs que militan hasta en el baño, pero con mencionarnos y decir que somos copados vamos pasando la bola y haciéndonos conocer. Está bueno ser pirata, contémosle a nuestrxs amigxs P)

== Hablar con otros piratas ==

Tenemos distintos medios de comunicación y participación, a saber:

* [http://asambleas.partidopirata.com.ar/listinfo/general/ Asamblea General]: Nuestra lista de correo principal
* [https://webchat.freenode.net/?channels=ppar Canal de Chat]: Trabajamos y nos divertimos!
* [[Como%20organizar%20un%20encuentro%20pirata|Off Topics]]: Después de trabajar nos juntamos a charlar un rato [[AFK|lejos del teclado]].

== Hacer cosas a lo pirata ==

A los piratas nos gusta la [[Adhocracia|adhocracia]], esto quiere decir que si vemos una necesidad, tenemos una idea copada para hacer y que tiene que ver con los principios piratas, no tenemos que esperar a que lo haga otrx, podemos hacernos cargo y llevarlo a cabo nosotrxs!

Sólo hay una condición, consultar con lxs demás por si hay desacuerdo e invitar a otrxs piratas a participar. Los detalles los podés ver en la sección [[http://partidopirata.com.ar/documentos/CartaOrganica.html#título-iii-de-los-mecanismos-de-democracia-directa|Mecanismos de la Democracia Directa]] de nuestra [http://partidopirata.com.ar/documentos/CartaOrganica.html Carta Orgánica].

Recordá la canción:

<blockquote>Si tu tienes muchas ganas de piratear<br />Si tu tienes muchas ganas de piratear<br />Si tu tienes la razón<br />Y no hay oposición<br />No te quedes con las ganas de piratear
</blockquote>
== Abordar un barco pirata ==

A veces compartimos intereses con otrxs piratas, para eso están los barcos piratas! Los barcos son grupos de piratas que se reunen para trabajar en temas específicos, como el [http://asambleas.partidopirata.com.ar/listinfo/tiempopirata/ diario pirata], las [https://red.anillosur.net/g/ppar comunas] de CABA, las [https://twitter.com/PartidoPirataAr redes sociales].

== Escribir como pirata ==

Si te gusta escribir o querés practicar, podés enviar notas a nuestro periódico [http://tiempopirata.org Tiempo Pirata].

== Diseñar como pirata ==

Si te gusta el diseño, podés contribuir a nuestra sección de [[Categoría:Media|Medios]] con posters, volantes, memes, diseños para remeras o lo que se te ocurra.