= Reunión por proyecto de ley Argentina Digital =

Objetivo: encontrar una estrategia legal para sugerir una enmienda constructiva que cubra los puntos que consideramos fundamentales para garantizar el derecho
humano a las comunicaciones en el Proyecto de Ley [[Propuesta:LeyArgentinaDigital]].

''Lugar'': La Barbarie.

''Hora'': Martes 4 de Noviembre, 18hs

''Participantes'': aza, charly, @MilanesVale, rodrigo, seykron

== Temas ==

Desarrollamos tres puntos que consideramos fundamentales: la neutralidad de la red, la privacidad y las redes libres.

Rodrigo dio una introducción sobre el funcionamiento actual de las concesiones, las tarifas, y las instituciones que intervienen.

Revisamos los artículos de la ley que tratan cada uno de los tres puntos que planteamos.

Hablamos sobre el debate que se llevó a cabo hoy en la cámara de senadores. [https://www.youtube.com/watch?v=VDhnR6vu1KY Este es el debate].

Acordamos que para el jueves vamos a tener un documento con las sugerencias de enmiendas que queremos introducir en el proyecto, así podemos llevarlos y repartirlos en la segunda parte del debate en la cámara de senadores.

aza y seykron van a contactarse con los asesores de la senadora Di Perna y Pino para preguntarles si pueden colaborar introduciendo los temas en la agenda del jueves.

== Neutralidad de la Red ==

Hubo un dictámen de comisión sobre un proyecto de Neutralidad de la Red que tiene todos los elementos que esperábamos y donde se eliminaron los puntos que metieron las empresas y CAPIF sobre contenido lícito e ilícito. [http://www.vialibre.org.ar/2014/10/22/dictamen-favorable-a-la-neutralidad-de-la-red-en-el-senado-nacional/ Este es el dictámen].

La senadora Morandini habló sobre el trabajo en comisión que se viene realizando sobre la Neutralidad de la Red y dijo que no debe ignorarse. La respuesta del Secretario fue "no queríamos volver tan compleja esta ley, pero está alineada con ese dictámen". En todo caso, la senadora Morandini va a ponerse firme con este tema y no hay nada que podamos agregar. Podríamos preguntar si hay alguna forma en la que podamos colaborar.

== Privacidad ==

Morandini destaca en su discurso que el Artículo 5 que habla sobre la inviolabilidad de los datos es más laxo que el de la ley vigente. El técnico-legal-político que hicimos hoy indicaría que el Artículo 5 del proyecto es suficiente para cubrir todos los casos que se nos ocurrieron.

== Redes libres y autoridad de Aplicación ==

La idea es promover las redes libres como una via de acceso barato y rápido a internet, sin depender de empresas de telecomunicaciones y promoviendo la gestión comunitaria.

La autoridad de aplicación es el punto clave y más controvertido. En el debate Morandini y Pino insistieron en este punto. Morandini destacó que "el que dicta las normas se controla a sí mismo", y que esto es una contradicción si hablamos de DDHH. La respuesta del Secretario fue "la Constitución dice que el poder ejecutivo es el que instrumenta las leyes".

Como la ley establece que las TICs son [[http://infoleg.mecon.gov.ar/infolegInternet/anexos/135000-139999/139207/norma.htm Servicios Universales (SU)]] acordamos entre todos tratar de luchar por una enmienda en este punto que:

# Indique que la autoridad de aplicación es un órgano autárquico y autónomo
# Que tenga representación de la oposición y de un sector colegiado que tengan voz y voto en la misma proporción

Respecto a los fundamentos para la habilitación de las redes libres, acordamos desarrollar lo siguiente:

* Articular las redes libres con el programa Conectar Igualdad y Argentina Conectada
* Complementar las [http://www.argentinaconectada.gob.ar/arg/258/14554/nucleos-de-acceso-al-conocimiento-nac.html NAC] de Argentina Conectada con redes libres para evitar la dependencia de las TELCOs.
* Destinar parte del Fondo Fiducidario que hoy se generó con el 1% de la renta de las TELCOS a la promoción de redes libres.