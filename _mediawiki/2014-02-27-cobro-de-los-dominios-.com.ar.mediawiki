[[Categoría:Propuestas]]
[[Categoría:Comunicados]]

Propongo: establecer una posición frente al cobro de dominios .ar y diseñar una o varias campañas

Posición: http://pad.partidopirata.com.ar/p/PosicionNicAr

== El Partido Pirata frente al cobro de dominios .ar ==

Las piratas nos enteramos con asombro y preocupación la decisión de Nic.ar de comenzar a cobrar los nombres de dominio para Argentina.  En la actualidad, los dominios de tipo .com.ar son prácticamente los únicos que se pueden registrar de forma gratuita en Internet.

Entendemos que Nic.ar usa argumentos falaces para justificar el cobro de dominios. El acceso gratuito a recursos clave de Internet es una ventaja, no una desventaja frente al resto del mundo: reduce la brecha digital y la dependencia de terceras partes que medien el acceso.  Si bien el ".com" nunca reflejó realmente el carácter libre de los nombres de dominio, creemos que justificar el cobro regular bajo el manto de la protección de marcas y otros intereses comerciales es un retroceso y una contradicción con la postura de este gobierno respecto a la pretendida inclusión social en el mundo digital (ver Conectar Igualdad, [http://www.pagina12.com.ar/diario/economia/2-240482-2014-02-24.html Argentina Conectada], etc.)

Cobrar por un recurso prácticamente infinito redunda en una ridiculez que sacrifica derechos adquiridos. Hay otras formas de controlar el parking y el registro masivo de dominios que no implican quitarlos del alcance de quienes no pueden pagarlos, sino que es tarea del Estado discriminar qué usuarios realizan actividades especulativas para aplicar las sanciones que correspondan.  Crear escasez artificial sobre los nombres de dominio provoca exactamente lo contrario a lo que se propone, abriéndole el juego a la verdadera especulación, la de los que tienen los recursos para hacerlo. Se repite un modelo de restricciones utilizado en otros países, en donde la falta de alternativas libres y gratuitas genera un negocio de intermediarios proveedores.

Creemos que el Estado debe proveer un servicio de Registro de Dominio libre e igualitario para todas las personas independientemente de su poder adquisitivo. Como parte de la nueva regulación el Estado debe garantizar una alternativa libre (aunque la misma no sean dominios .com.ar). La regulación anunciada es una medida recaudatoria, excusa para cercenar los 200 dominios que Nic.ar otorgaba "gratuitamente" obligando a reempadronarse a $200 por dominio.

Lxs piratas proponemos:

* Que se publiquen las resoluciones pertinentes, fundamentando esta medida más allá de comentarios mediáticos en favor de intereses empresariales

* Que se de marcha atrás en esta medida retrógrada, manteniendo la gratuidad del registro de dominios y buscando alternativas a la especulación que favorezcan a todxs, por ejemplo creando un TLD pago específico para marcas registradas, "lucr.ar", ".marca.ar", , etc.

* Que dejen de registrarse y publicarse datos personales en el registro de dominio

* Que se abran TLDs alternativos para organizaciones y colectivos que no se encuadren en la categoría ".com.ar", por ejemplo ".alt.ar"