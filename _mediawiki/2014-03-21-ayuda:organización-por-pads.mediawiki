[[Categoría:Organización]]
[[Categoría:Eventos]]

Esqueleto para organizar un evento usando un [http://pad.partidopirata.com.ar pad]. Ir a editar/ver fuente, copiar y pegar en un pad nuevo. Viene [http://wiki.hackcoop.com.ar/RFC:Organización_por_pads del wiki del HackLab].

= Evento =

Escribir una descripción del evento, hora y lugar acá.

== Difusión ==

=== Grupos a los que invitar ===

Hacer una lista de los grupos, personas, etc. a los que invitar personalmente al evento.  Un "che, estamos haciendo esto, venís? podés participar con tu proyecto", mueve bastante.

=== Gacetilla ===

Escribir un texto invitación al evento, qué se va a hacer, qué va a pasar, por qué, etc.

=== Poster ===

Diseñar un poster y otro material gráfico

== Organización ==

=== Cosas para hacer ===

* Listar tareas
* Asignarse como responsables al lado de cada una entre paréntesis ([[Hacker:Fauno]])
  Los comentarios o el progreso de una tarea se pueden poner inmediatamente abajo

=== Cosas hechas ===

Copiar y pegar las [[#Cosas para hacer]] acá una vez que estén terminadas.  La idea es poder tener un espacio limpio donde se anota lo que falta hacer y se puede visualizar fácilmente sin tener que filtrar leyendo, además de tener un segundo espacio donde se visualiza lo que ya se hizo.  Es una especie de Kanban en texto plano :)