[[Categoría:Posiciones]]

= Contra-posición del Partido Pirata sobre las cámaras de seguridad =

Sistematización de este [http://asambleas.partidopirata.com.ar/archivos/general/2013-July/004577.html hilo de la Asamblea General].  Se trabajó en [https://pad.partidopirata.com.ar/p/Camaras este pad].


''Ya que los piratas se oponen a la vigilancia, ¿qué contrapropuesta hacen para poder resolver los problemas que las cámaras de seguridad intentan resolver?''

Los piratas queremos solucionar el problema de fondo, necesitamos recuperar los programas de desarrollo social, que en CABA están siendo desarticulados por el gobierno macrista. Dejar de comprar cámaras y otras &quot;soluciones&quot; tecnocráticas que son la maquinaria de la exclusión para desarrollar programas de inclusión, erradicación de la pobreza, educación.

Ya que la inseguridad responde en gran medida a la exclusión social y a los factores de riesgo más severos como la pobreza y la indigencia, se debe trabajar para abordar estos fenómenos e integrar a las personas que viven esta situación en la sociedad. El cooperativismo como modelo de cohesión social brinda una solución factible a mediano plazo, ya que este tipo de instituciones son constituídas por las mismas personas que están involucradas en el problema. Estas organizaciones promueven la colaboración entre pares, proponen un modelo de trabajo integrado, tratan la conflictividad como un problema cotidiano a resolver en conjunto, y ofrecen un desarrollo económico a menor escala que incluye al grupo de riesgo en el mercado local abriendo así un nuevo mercado interno basado en la cooperación en lugar de la competencia. Las cooperativas también pueden ofrecer una vivienda provisional, transformando el lugar de trabajo en un hogar colectivo hasta que cada familia logre conseguir su vivienda como resultado de las operaciones de la cooperativa. Para constituir instituciones de esta naturaleza es necesario el financiamiento y el soporte operativo de la Ciudad.

La educación es otro factor determinante para abordar la cuestión de la inseguridad.

<blockquote>Tenemos que pensar en un sistema preventivo basado en erradicar la pobreza limitando el monopolio y las corporaciones. Un fuerte cinturón social basado en el cooperativismo y la educación libre. -- Galleguindio</blockquote>


== Referencias ==

* Marcha &quot;[http://www.marcha.org.ar/1/index.php/nacionales/88-laborales/2032-despiden-a-trabajadores-en-desarrollo-social-porteno Despiden a trabajadores en Desarrollo Social porteño]&quot;
* Diario Z &quot;[http://www.diarioz.com.ar/nota/defensoria-del-pueblo-denuncian-que-el-macrismo-reprime-a-indigentes/ Defensoría del Pueblo: denuncian que el macrismo reprime a indigentes]&quot;
* Wikipedia &quot;[http://es.wikipedia.org/wiki/Gestiones_de_Mauricio_Macri_en_el_Gobierno_de_la_Ciudad_de_Buenos_Aires Gestiones de Mauricio Macri en el Gobierno de la Ciudad de Buenos Aires]&quot;
* [http://www.aciamericas.coop/IMG/AnalisisdelModeloCooperativo.pdf Análisis del Modelo Cooperativo en el Nuevo Escenario Económico]