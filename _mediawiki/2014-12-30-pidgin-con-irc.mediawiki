En el IRC pirata nos juntamos a conversar en tiempo real, intercambiar información, pelotear ideas... es como nuestro grupo de Whatsapp pero en un servidor seguro y pirata P)

Esta guía te muestra los pasos para configurarlo en Pidgin, el cliente de mensajería multitodo.  Si estás usando GNU/Linux, seguramente ya lo tenés instalado.  Si estás en un sistema operativo que se llama Windows, lo tenés que [https://pidgin.im descargar desde su sitio].  Si estás en otro que se llama Mac OSX, no hay Pidgin, pero hay Adium que es similar.

También hay otra guía para usar Pidgin de forma segura y con otro sistema libre que se llama Jabber: [[Pidgin con OTR]]

= Configuración =

Para agregar la conexión a IRC en Pidgin, primero hay que abrir el gestor de cuentas, en el menú "Cuentas".

[[Archivo:001_pidgin.png]]

Al agregar una nueva cuenta, hay que crearla con el protocolo IRC y sólo poner nuestro nick y el servidor al que nos queremos conectar, en este caso '''irc.pirateirc.net''' que es donde está nuestro canal de chat.

[[Archivo:002_configuracion_general.png]]

Aunque para que la conexión sea segura, hay que hacer algunos cambios en la parte de avanzadas, especialmente el número de puerto y habilitar SSL.

[[Archivo:003_avanzadas.png]]

Una vez que está creada la cuenta, Pidgin la va a recordar y cada vez que lo abramos se va a conectar por su cuenta a menos que le digamos lo contrario en el Gestor de cuentas que ya vimos.

Ahora hay que agregar un canal de chat.

[[Archivo:004_agregar_un_chat.png]]

Los canales de IRC se identifican por tener un numeral adelante (como los hashtags de Twitter).  El nuestro es '''#ppar'''.

[[Archivo:005_configurar_chat.png]]

Al completar este paso por única vez ya vamos a tener el canal en nuestra lista de contactos.  También se le puede poner un avatar, haciéndole click derecho y eligiendo la opción "configurar icono personalizado".

[[Archivo:006_chat_en_la_lista.png]]

Y al hacerle doble click al canal ya entramos a '''#ppar'' P)

[[Archivo:007_ventana_de_chat.png]]

[[Categoría:Software Libre]]
[[Categoría:Guías]]