  ______ __              __          _______               __   
 |   __ \__|.----.---.-.|  |_.-----.|    ___|.-----.-----.|  |_ 
 |    __/  ||   _|  _  ||   _|  -__||    ___||  -__|__ --||   _|
 |___|  |__||__| |___._||____|_____||___|    |_____|_____||____|
 

El primer Festival Pirata se llevó a cabo en paralelo a la Asamblea General del Partido Pirata Internacional, el sábado 12 y el domingo 13 de Marzo del 2011.

==Lugar==

'''[http://oncelibre.com.ar/ OnceLibre], Pueyrredon 19 3º piso, Buenos Aires.'''

==Repercusión en los medios==

===Previa===

* [http://ovejafm.com/programa-106-2011 La Oveja Electrónica]

===Durante===

* [http://www.pagina12.com.ar/diario/suplementos/espectaculos/16-21041-2011-03-14.html Página12]
* [http://derechoaleer.org Derecho a leer]
** [http://derechoaleer.org/2011/03/piratefest-dia-1.html Sábado 12]
** [http://derechoaleer.org/2011/03/piratefest-dia-2.html Domingo 13]
* [http://zele.biz/?p=571 Sobre el InstallFest]
* [http://galeria.buenosaireslibre.org/Presentaciones/PirateFest-OnceLibre-2011/ Fotos de BuenosAiresLibre]

===Posterior===

* FM Rock&Pop, entrevista en ''Apagá la tele''

==¿Quiénes participaron?==

* [http://elblogdemec.blogspot.com/ María Elena Casañas (MEC)]: charlas ''Cultura libre para todos? Ni lo sueñes!'' y ''¿Cómo migrar al software libre?''.
* Multiespacio [http://oncelibre.com.ar OnceLibre] y [http://articultores.net/ Articultores]: nos brindaron el lugar y dieron talleres de bombas de semillas.
* [http://www.ubuntu-ar.org Comunidad de Ubuntu Argentina]: se hizo cargo del InstallFest durante los dos días.
* [http://www.buenosaireslibre.org/ Buenos Aires Libre]: charla ''Introducción a BuenosAiresLibre'' y conectividad wi-fi durante el evento.
* [http://parabolagnulinux.org/ Parabola GNU/Linux-libre]: charla ''Libertad del Software para la libertad política''.
* [http://www.vialibre.org.ar/ Fundación Vía Libre]: charla ''Derecho de autor y licencias libres. Algunas propuestas y soluciones''.
* [http://www.mccwn.com.ar/ Clandestina Weekend Nerd]: charla ''Emancipacion Intelectual, un paso a la revolución productiva y cultural''.
* [http://www.808m.cc 808m]: charla ''Netlabels y discográficas en la web'' y se encargaron de [[#M.C3.BAsica|musicalizar]] el domingo.
* [http://wikimedia.org.ar Wikimedia]: charla ''Wikipedia: relevancia enciclopédica o cómo hacer para que no me borren mi artículo''.
* [http://redpanal.com Red Panal]: ''Charla sobre música libre''.
* [http://www.flia.org.ar F.L.I.A.]: charla ''Todos los derechos reversados o el copyleft editorial''.
* [http://drupal.usla.org.ar/ U.S.L.A.]: nos prestaron material invaluable como el proyector y la pantalla.

==Cronograma==
<small>Final</small>

=== Sábado 12 de Marzo ===

{| class="wikitable"
|- style="background-color:#D8D8D8;" 
|+Sábado 12 de Marzo
|-
|
! scope="col" | Sala Bouchard
! scope="col" | Sala Emilio Salgari
! scope="col" | Talleres
|-
! scope="row" | 14:00 - 15-00
| 
|
| 
|-
! scope="row" | 15:00 - 16-00
| MEC: "Cultura libre para todos? Ni lo sueñes!"
|
| Articultores: Taller de bombas semilla
|-
! scope="row" | 16:00 - 17:00
| Matías Lennie: Charla sobre música libre
| 
| OpenPGP, encriptación de correo y Web of Trust
|-
! scope="row" | 17:00 - 18:00
| [[NicolasReynolds|Nicolas Reynolds]] "Libertad del Software para la libertad política"
| 
| Facebook, opciones de privacidad
|-
! scope="row" | 18:00 - 19:00
| MEC: "¿Cómo migrar al software libre?"
| Sergio Pernas y Osiris Gómez "Introducción a BuenosAiresLibre"
| 
|-
! scope="row" | 19:00 - 20:00
| Beatriz Busaniche: "Derecho de autor y licencias libres. Algunas propuestas y soluciones."
| 
| 
|-
! scope="row" | 20:00 - 21-00
| 
|
| 
|-
|}

=== Domingo 13 de Marzo ===

{| class="wikitable"
|- style="background-color:#D8D8D8;" 
|+Domingo 13 de Marzo
|-
|
! scope="col" | Sala Bouchard
! scope="col" | Sala Emilio Salgari
|-
! scope="row" | 14:00 - 15-00
| [[Ernesto Bazzano (el Bazza)|Ernesto Bazzano (el Bazza)]]: "[[Aplicaciones creadas con filosofía GNU]]"
| 
| 
|-
! scope="row" | 15:00 - 16-00
| Marilina Winik, Matías Reck de "Milena Caserola", Ezequiel Ábalos, Pablo Strucchi de ")El asunto(", Tomas Manoukian y Alex Schmied de "Pasajeros de Edición" y Marilina Winik (F.L.I.A): "Todos los derechos reversados o el copyleft editorial"
| 
| 
|-
! scope="row" | 16:00 - 17-00
| Federico Sánchez Maidana: "Netlabels y discográficas en la web"
| Ivan Ivanoff: Emancipacion Intelectual, un paso a la revolución productiva y cultural. Parte I.
| 
|-
! scope="row" | 17:00 - 18-00
| Sebastián Bassi: "Wikipedia: relevancia enciclopédica o cómo hacer para que no me borren mi artículo"
| Ivan Ivanoff: Emancipacion Intelectual, un paso a la revolución productiva y cultural. Continuación.
| 
|-
! scope="row" | 18:00 - 19-00
| 
| Chapa: "me cago en la cultura, especialmente en la libre" 1
| 
|-
! scope="row" | 19:00 - 20-00
| 
| Chapa: "me cago en la cultura, especialmente en la libre" 2 
| Articultores: Taller de bombas semilla
|-
! scope="row" | 20:00
| 
| PPAr cierre, cena general
|
|-

|}

==Música==

Performance sonora '''durante el Domingo 13''' a partir de las 17 hs. (después de la charla de Netlabels), a cargo de:

{| class="wikitable"
|- style="background-color:#D8D8D8;" 
|+Domingo 13 de Marzo
|-
! scope="row" | 17:00 a 17:45 
| Juan Farcik (808m, AR)
|-
! scope="row" | 18:00 a 19:00 
| Jin Yerei (808m, AR)
|-
! scope="row" | 19:00 a 19:20 
| EYE (Guido Flichman) + invitados (Cine Shampoo, AR)
|-
! scope="row" | 19:20 a 20:00 
| Artista Sorpresa que llega desde Italia!
|-
! scope="row" | 20:00 a 20:30 
| Juan José Calarco (Impulsive Habitat, AR)
|-
! scope="row" | 20:30 a 21:00 
| Joaquín Gutiérrez Hadid (Impulsive Habitat, 808m; AR)
|-
|}


==Material digital==

* Enrique Chaparro, ''Me cago en la cultura, especialmente en la libre'' ([http://www.ivoox.com/enrique-chaparro-me-cago-cultura-especialmente_md_572847_1.mp3 audio])
* Jin Yerei, ''Escenario Orgánico-Sonoro, Marzo 2011'' ([http://soundcloud.com/808m/jin-yerei-escenario-organico/ audio])
* Sebastián Bassi, ''Wikipedia: relevancia enciclopédica o cómo hacer para que no me borren mi artículo'' ([http://derechoaleer.org/files/piratefest/piratefest-2011-presentacion-bassi-wikipedia.pdf pdf])