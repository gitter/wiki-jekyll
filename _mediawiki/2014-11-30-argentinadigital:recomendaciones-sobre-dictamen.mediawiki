[[Categoría:Noviembre 2014]]
[[Categoría:Argentina Digital]]
[[Categoría:Recomendaciones a proyectos de ley]]

* Propuesto en [[Propuesta:LeyArgentinaDigital]]
* Escrito en: [https://pad.partidopirata.com.ar/p/ArgDigDictamenMinoria Pad:ArgDigDictamenMinoria]

= Recomendaciones sobre el dictámen de Argentina Digital =

Las siguientes recomendaciones son una respuesta al [http://wiki.partidopirata.com.ar/Archivo:Dictamen_Argenti%20na_Digital.pdf dictámen del proyecto de ley Argentina Digital].

== Comentarios generales - impresiones ==

Dado el poco tiempo creo que lo mas factible es arreglar las definiciones y ver como queda. Puntos a tener en cuenta: no necesitar licencias para nada q no necesitara, proteger a los usuarios y sus derechos.

En principio, obtener licencia para cualquier cosa que no sea usar una banda de frecuencias que requiera licencia, es un absurdo y una puerta abierta a la censura: &quot;No te doy licencia porque no me gusta tu cara, o porque te llamás Clarín. O te la duermo por diez años&quot;. Debe bastar con simple registro, que se considere consentido si no hay observaciones de la autoridad regulatoria en 30 días corridos.

Incluir cuestiones que tienen que ver con la privacidad, que tome las prácticas de vigilancia estatal como abusivas.

El objeto y la finalidad de la ley expresan claramente cuál es el espíritu de la norma. Sin embargo, la redacción del resto del proyecto no sólo contradicen el objeto y la finalidad, sino que habilitan prácticas que pueden poner en peligro a los usuarios. Los primeros dos artículos expresan lo siguiente:

'''Artículo 1. Objeto''': posibilitar el acceso de la totalidad de los habitantes de la República Argentina a los servicios de la información y las comunicaciones en condiciones sociales y geográficas equitativas, con los más altos parámetros de calidad.

'''Artículo 2. Finalidad''': a. garantizar el derecho humano a las comunicaciones y a las telecomunicaciones, reconocer a las Tecnologías de la Información y las Comunicaciones (TIC) como un factor preponderante en la independencia tecnológica. b. Establecer con claridad la distinción entre los mercados de generación de contenidos y de transporte de manera que la influencia en uno de esos mercados no genere prácticas que impliquen distorsiones en otro.

== Modificaciones ==

=== ARTÍCULO 6.- Definiciones generales. ===

<ol style="list-style-type: lower-alpha;">
<li><p>Autoridad de Aplicación: el Poder Ejecutivo Nacional designará la Autoridad de Aplicación de la presente ley. El mismo será un organismo descentralizado y autárquico en el ámbito del Poder Ejecutivo Nacional, y su Directorio debe contar, al menos, con representación de las minorías parlamentarias y miembros colegiados de universidades nacionales elegidos por concurso público. Será objeto decontrol por parte de la Sindicatura General de la Nación y de la Auditoría General de la Nación.</p></li>
<li><p>Recursos Asociados: son los servicios, la infraestructura física y otros recursos o elementos pertenecientes a una red de telecomunicaciones que hacen posible o respaldan la prestación de Servicios de TIC a través de dicha red o dicho servicio, o tienen la posibilidad de hacerlo. Incluirán, entre otros, edificios o entradas de edificios, el cableado de edificios, antenas, torres y otras construcciones de soporte, conductos, mástiles, bocas de acceso y distribuidores. Se excluye toda infraestructura, recurso o tecnología perteneciente a los Usuarios de Servicios TIC.</p></li>
<li><p>Tecnologías de la información y las comunicaciones (TIC): es cualquier tecnología que permita el procesamiento de contenido, como por ejemplo voz, datos, texto, video e imágenes, entre otros.</p></li>
<li><p>Telecomunicación: es toda transmisión, emisión o recepción de información de cualquier naturaleza a través de medios electromagnéticos.</p></li>
<li><p>Servicio de Tecnologías de la Información y las Comunicaciones (Servicios de TIC): es el prestador de servicios de Telecomunicaciones.</p></li>
<li><p>Servicio de Telecomunicación: es el servicio de transmisión, emisión o recepción de escritos, signos, señales, imágenes, sonidos o información de cualquier naturaleza, por hilo, radioelectricidad, medios ópticos u otros sistemas electromagnéticos, a través de redes de telecomunicaciones.</p></li></ol>

=== TÍTULO VII: Consideraciones generales sobre los Servicios de TIC ===

Agregar los siguientes artículos que estuvieron incluídos en el [http://www.vialibre.org.ar/2014/10/22/dictamen-%20favorable-a-la-neutralidad-de-la-red-en-el-senado-nacional/ dictámen de neutralidad de la red] pero se eliminaron en Argentina Digital.

ARTÍCULO n°.- '''Gestión de Tráfico'''. Los prestadores de servicios de telecomunicaciones, públicos o privados, que brinden servicios de conectividad mediante cualquier modalidad o soporte, sólo podrán eximirse de cumplir las obligaciones de neutralidad de red impuestas en la presente ley a través de medidas de gestión de tráfico, cuando acrediten que la adopción de dichas medidas tiene por finalidad evitar una situación de extrema congestión de la red, que altere su correcto funcionamiento o para preservar la seguridad de la red. Antes de ser implementadas deberán ser informadas a la Autoridad de Aplicación.

ARTÍCULO n°.- '''Exigencias de Gestión de Tráfico'''. Las medidas de gestión de tráfico que afecten la neutralidad serán temporarias y excepcionales, y en ningún caso podrán ser adoptadas en base al autor, fuente de origen, destino o propiedad de los contenidos, aplicaciones, servicios o protocolos, ni afectar la libre competencia. Deberá preservarse la privacidad y protegerse los datos personales.

=== ARTÍCULO 62.- Obligaciones. Los licenciatarios de servicios de TIC tienen las siguientes obligaciones: ===

Eliminar inciso i) ya que habilita el espionaje masivo e ilegal, y va en contra de la neutralidad de la red y la inviolabilidad de las comunicaciones. El PE bajo ninguna circunstancia debe estar ''sobre'' la ley.

Agregar nuevo inciso sobre transparencia de gestión:

<ol start="14" style="list-style-type: lower-alpha;">
<li>Publicar de forma accesible y notificar inmediatamente y de manera efectiva a todos los usuarios sobre modificaciones en las condiciones de prestación de los Servicios de TIC, incluyendo, pero no limitado a, las políticas de gestión de tráfico. Estas notificaciones deberán incluir información sobre las necesidades y condiciones que justifiquen la modificación de los servicios prestados y deberán ser accesibles por los usuarios mediante las herramientas determinadas por la prestadora.</li></ol>