-
* Unknown_copyright|Ni idea / No me importa
* Licencias libres:
** PD|PD: Dominio público
** CC-by-sa-2.5|Creative Commons Attribution ShareAlike 2.5
** CC-by-2.5|Creative Commons Attribution 2.5 
** GFDL|GFDL: GNU Free Documentation License
** GPL|GPL: GNU General Public License
** LGPL|LGPL: GNU Lesser General Public License
** PPL: Licencia de Producción de Pares