* Las personas a que se refiere el artículo 24 de la Ley 23.298.
* Los condenados por delitos electorales.
* Los sancionados por actos de fraude electoral en los comicios internos del Partido.
* Los que tengan alguna condena por crímenes de lesa humanidad o hayan participado en algún gobierno de facto.
* Los que hubieren incurrido o incurrieren en violaciones de los derechos humanos.
* Los que hubieren incurrido o incurrieren en violaciones de los Principios Fundacionales Piratas.
* Los que fueren expulsados del Partido conforme a las disposiciones de la presente Carta Orgánica.