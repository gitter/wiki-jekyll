---
title: Translations:Carta Orgánica/43/es
layout: post
categories: []
---

El reglamento y las propuestas no podrán contradecir el espíritu de esta
Carta Orgánica, ni la Declaración de Principios Fundacionales del
Partido, antes bien profundizarlos.
