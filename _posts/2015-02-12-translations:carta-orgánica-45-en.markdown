---
title: Translations:Carta Orgánica/45/en
layout: post
categories: []
---

Every Party\'s decision, in any of its decision-making instances, will
be taken by Consensus. If Relevant Dissent exists in proportion defined
by Reglament, decisions will be taken by Agreement.
