---
title: Propuesta:ReuniónProyectoSur
layout: post
categories: []
---

Propuesta
---------

La idea es empezar a implementar un mecanismo de participación directa
similar al nuestro adentro de Proyecto Sur. Los objetivos que se
manifestaron a grandes razgos son poder organizar actividades, generar
una base de conocimiento colectivo, coordinar acciones de base y tomar
decisiones sobre las actividades parlamentarias de sus legisladores. Ya
se acercaron al PdR y le quisieron vender un software, y por supuesto
esto no es lo que está buscando.

Proponen que los ayudemos a diseñar la organización en base a nuestra
experiencia en la democracia directa por disenso. Pino es presidente de
la Comisión de Medio Ambiente y tienen muchas trabas políticas para
hacer cosas, así que ofrecen abrir la comisión para que podamos ir y
participar activamente de las reuniones que se hagan para avanzar con
proyectos que probablemente nos interesan. Están en contra del fracking,
la minería a cielo abierto, la fumigación con glifosato, etc. La idea es
que las luchas de base que se dan hoy en movimientos aislados puedan
hacer presión en el Congreso para que traten las leyes ambientales que
nos interesan.

Reunión
-------

**Lugar**: Congreso Nacional, Bloque de Proyecto Sur.

**Hora**: Miércoles 8 de Octubre, 18hs (puntual)
