---
title: Observatorio Penal
layout: post
categories: []
---

Estaba pensando en la posible cronología sobre procesos, censuras etc.y
recuerdo las siguientes, tal vez podrían ponerse en una introducción en
el manifiesto:

Febrero de 2008:

Los procesos por descarga de música de la CAPIF, en realidad no fueron
procesos, no llegaron a serlo, fueron aprietes, acá encuentran algo más
de información:

<http://tinyurl.com/yfr7k7b>

Acá está la nota de la Rolling Stone Argentina

<http://www.rollingstone.com.ar/archivo/nota.asp?nota_id=983757>

Hay algo más grave en todo esto y es que Fibertel-Clarín dió los datos
de los usuarios de p2p sin que interviniera una orden judicial, ante el
sólo requerimiento de la CAPIF le pasaron los datos , algo que se
repetiría luego en otro caso.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

En Febrero de 2008 aparece el primer proyecto de Canon Digital impulsado
por Morgado:

<http://tinyurl.com/ygtssa8>

Curiosamente, o no tan curiosamente, el proyecto presentado por Filmus
no tuvo tanta repercusión\....

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Creo que en el 2008 no pasó más nada, pero no estoy seguro.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_

2009:

Marzo:

Proyecto de canon digital impulsado por Filmus:

<http://tinyurl.com/yh29zva>

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Marzo de 2009 : Procesamiento a Horacio Potel:

<Http://horaciopotel.partido-pirata.com.ar>

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ Mayo de 2009:

Clarín manda en cana al que había hecho un photoshop alterando lo que
decía una bandera en un partido de fútbol al colocar en el diario el
nombre, apellido y lugar en donde vívía quien había hecho el photoshop.
Obvio que usó que el que había hecho el cartel era usuario de
Fibertel\....Así que si hacés algo contra Clarín, ellos te mandan en
cana con pitos y cadena.

<http://tinyurl.com/yffhbbz>

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ Mayo de 2009:

Censura de videos en youtube por parte de Clarín-Artear:

<http://tinyurl.com/ykft7lr>

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Agosto de 2009:

Cierre del site Qué te pasa Clarín por presiones judiciales:

<http://tinyurl.com/nmrm87>

\_\_\_\_\_\_\_\_\_

Setiembre de 2009:

Proyecto de Ley Mordaza del Senador Jenefes:

<http://tinyurl.com/yl4lozv>

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Octubre de 2009:

Cierre de Bibliofyl por presiones judiciales:

<http://tinyurl.com/ygjzooz>

\_\_\_\_\_\_\_\_\_\_\_\_

Esto creo que fue en mayo o junio de este año:

También tenemos el acuerdo CADRA-UBA y otras universidades nacionales:

<http://tinyurl.com/yc5sxzg>

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Ésto sería exclusivamente en el ámbito de Internet y de Derechos de
Autor.

Hay algo que dijo Eugenio Zafaroni y que me llamó la atención:

Habla del aborto, pero podríamos pensarlo para el tema de descargas de
archivos con derechos de autor:

\"--¿El aborto es una de esas medidas urgentes que hoy integran la
agenda de la Corte?

--Habría que empezar a pensar en qué se hace con el aborto. Es un
fenómeno masivo, fenómeno que nadie hace nada por controlar, y la
disposición penal no sirve para nada.\"

\"--En el caso del aborto, la aplicación del Código Penal deriva en una
forma de discriminación social.

--Es lo que ocurre. Por eso: si la norma penal es ineficaz como tutela,
aflojemos la norma penal para darle la tutela por otra vía. Alguien me
va a decir: si se vuelve masivo el homicidio, ¿haría lo mismo?
Lamentablemente sí: cuando se vuelve masivo el homicidio se llama
guerra, y eso termina en un armisticio, no en el Código Penal. Hoy el
número de muertos es equivalente al que puede haber en una guerra, si
contamos como vida a los fetos. Los fetos no son de papel, hablar de que
la tutela viene muy bien porque está en el Código Penal, cuando en
treinta años no me pasó ni una docena de casos por la mano, ¿qué clase
de tutela es esa? Ninguna. Es una excusa para que el Estado se
desentienda totalmente del problema. Total está penalizado. Y yo, como
juez, lo tutelo.\"

<http://tinyurl.com/yfgr6j4>
