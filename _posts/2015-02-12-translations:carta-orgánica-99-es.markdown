---
title: Translations:Carta Orgánica/99/es
layout: post
categories: []
---

Un Pirata imputado de sanción puede designar hasta un (1) Contramaestre,
debiendo ser siempre notificados fehacientemente los Piratas y Asambleas
pertinentes y debidamente registrado.
