---
title: Translations:Carta Orgánica/80/es
layout: post
categories: []
---

Todas las responsabilidades delegadas en los Piratas Afiliados serán de
carácter revocable y rotativo.
