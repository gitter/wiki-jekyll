---
title: Translations:Carta Orgánica/10/es
layout: post
categories: []
---

Se considera simpatizante adherente del Partido Pirata a toda persona
sin distinción alguna que bajo sus propias facultades manifieste acuerdo
y conformidad con los Principios fundacionales del Partido según esta
Carta Orgánica.
