---
title: Ayuda:Traducir
layout: post
categories:
- Traducción
---

[Categoría:Traducción](Categoría:Traducción "wikilink")

Guía de traducción pirata!

La idea de esta guía es delinear algunas herramientas y técnicas que nos
ayudaron a coordinar esfuerzos de traducción en algún momento. Si querés
colaborar con nosotrxs por favor seguila y contribuí con cosas nuevas!

Lista de traducción
-------------------

Lxs traductorxs nos organizamos en la lista de correo:
<http://asambleas.partidopirata.com.ar/listinfo/traduccion>

Si vas a colaborar regularmente por favor suscribite.

Proceso de traducción
---------------------

### Sugerir un artículo

Enviá el link a traduccion\@asambleas.partidopirata.com.ar

### Traducir

Creá un pad con el texto y envialo como respuesta al mail original. Para
crear un pad, hay que visitar <http://pad.partidopirata.com.ar> y
apretar el botón \"New Pad\". Luego copiar el link y enviarlo por mail.
**Tip**: se le puede poner un nombre al pad para que sea más fácil de
distinguir.

Siempre separá cada párrafo con una línea en blanco, es más fácil de
leer!

Empezá la traducción de cada párrafo justo debajo, para que sea más
fácil hacer la lectura de corrección luego.

Repetir hasta terminar!
