---
title: Translations:Carta Orgánica/16/es
layout: post
categories: []
---

Se asume que el Pirata que se afilie adhiere a los principios del
partido, pudiéndose retirar la afiliación de comprobarse fehacientemente
que no adscribe a los mismos.
