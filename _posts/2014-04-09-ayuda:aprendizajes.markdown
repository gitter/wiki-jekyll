---
title: Ayuda:Aprendizajes
layout: post
categories:
- Guías
- Organización
- Democracia directa
---

[Categoría:Guías](Categoría:Guías "wikilink")
[Categoría:Organización](Categoría:Organización "wikilink")
[Categoría:Democracia directa](Categoría:Democracia_directa "wikilink")

Algunos aprendizajes sobre la organización adhocrática:

-   las invitaciones al aire no convocan, hay que invitar a la gente que
    queres que vaya directamente. recordarles que las esperas, que esa
    reunion puede interesarles, romperles las bolas si es necesario.

<!-- -->

-   tener un temario

<!-- -->

-   nadie está obligada a participar; si te dejan colgada no importa,
    pero dar menos prioridad a esa gente la próxima vez (ahorrarse los
    pings)

<!-- -->

-   visibilizar/comunicar lo que se estuvo haciendo ayuda un montón, a
    poca gente le gusta caer de paracaidista. o sea, hacer resumenes,
    sacar fotos, etc. ayuda a que la gente sepa qué va a pasar. además
    atrae gente como uno o al menos espanta fachos y creepies (mejor
    sola que mal acompañada :P)

<!-- -->

-   frustrarse hace mal, quejarse de frustracion espanta a las demas en
    lugar de generar empatía

<!-- -->

-   si no va nadie, hacer algo igual (pensar en adhocracia, democracia
    por disenso, \"nada puede malir sal\"). ademas no te vas con la
    sensación de fracaso y tenes algo para visibilizar.

<!-- -->

-   en hackerspaces.org le dicen \"el patron de los martes\", ya que no
    se puede combinar un dia (imaginen las idas y vueltas hasta que
    todas dicen si yo puedo), se elige arbitrariamente el martes.

<!-- -->

-   la otra es que las citas tienen que tener hora de inicio y fin, cosa
    que si alguien planea llegar mas tarde sepa que no va a llegar :P
