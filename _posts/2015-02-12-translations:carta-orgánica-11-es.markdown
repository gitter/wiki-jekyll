---
title: Translations:Carta Orgánica/11/es
layout: post
categories: []
---

Asimismo se considerará simpatizante "tácito" a todo quien en sus
prácticas tienda a compartir y producir tanto información como bienes
comunes materiales, culturales y/o digitales.
