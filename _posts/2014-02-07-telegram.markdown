---
title: Telegram
layout: post
categories: []
---

Esta es una guia preliminar sobre Telegram y su seguridad o falta de
ella.

-   los clientes son libres (gpl2+)

<!-- -->

-   ni rastros del servidor (codigo fuente, donde esta, como funciona)

<!-- -->

-   hay que poner exactamente el numero de telefono con el que la otra

persona se registró para que lo tome como un contacto (posibilidades:
+54 11 5\... , +54 9 11 \..., +54 11 15\...)

-   no se puede recargar la lista de contactos a mano asi que hay que

reiniciarlo cada vez (al menos el cliente oficial).

-   prometen que esta todo cifrado y ofrecen 200k usd de recompensa para

los que encuentren agujeros de seguridad

-   es bastante rapido

<!-- -->

-   ni idea si se guardan los datos cifrados en el celular, aunque
    \"cerrar

la sesion\" parece que borra todo

-   solo se puede descargar de google play asi que le tuve que pedir a

alguien que me lo pase porque no lo pienso activar.

-   en el github vi que incluyen un par de blobs binarios en el codigo

fuente pero alguien ya lo arregló así que en cualquier momento sale por
f-droid.org

-   es esteticamente lindo pero dificil de encontrar informacion por el

nombre generico

dado el estado de los clientes jabber (sin canales, envio de archivos,
bugs horrendos, perdida de mensajes\...) en android esta bastante bien
(show me the server!!)

creé un grupo pirata, si quieren unirse pasenme sus numeros en privado,
tal como dice en \"ajustes\" P)
