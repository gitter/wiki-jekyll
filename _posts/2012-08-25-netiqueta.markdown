---
title: Netiqueta
layout: post
categories: []
---

Responder abajo, recortar y citar lo que se responde
----------------------------------------------------

Cuando respondes a un mail pensa en quien lo recibe. Es mas facil para
esta persona entender tu mensaje si la respuesta esta debajo del texto
al que se responde. Tambien si solo respondes una parte del texto podes
recortar el resto.

No cambiar de tema
------------------

El *asunto* del mail dice de que se trata la conversación. Para evitar
irnos por las ramas y hacerle perder el tiempo a los demás antes de
responder fijate si estas hablando sobre el tema nombrado en el
*asunto*.

En caso de que aun quieras tratar el tema podes enviar un mensaje con un
nuevo *asunto* citando el mensaje anterior y agregandole \"era *asunto
anterior*\"
