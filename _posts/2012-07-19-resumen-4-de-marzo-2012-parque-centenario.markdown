---
title: Resumen 4 de marzo 2012 Parque centenario
layout: post
categories: []
---

nos juntamos en parque centenario, conocimos a javier que estuvo
visitando el PP alemán y nos contó cómo está la situación allá (mucho
optimismo), además se acercó eduardo, un profesor de la matanza que nos
invita a participar en la sede del flisol que están organizando allá. es
el 28 de abril, estaría bueno ir a hacer una charla-debate.

con respecto a los principios mucho no pudimos conversar, pero aza me
había dicho que había estado arreglando el texto (pero no estaba en el
pad así que esta es una tirada de bolas indirecta).

charlamos sobre los requisitos para conformar el partido, quedamos que
fran va a hablar con una amiga abogada para determinar qué es
exactamente lo que tenemos que hacer. lo ideal sería hacerlo esta semana
para poder imprimir las planillas de recolección de firmas y llevarlas
al Z-day (sabado 11 si no recuerdo mal).

sobre el piratefest, en el hacklab estamos confirmando el lugar, queda
en barracas a un par de cuadras de montes de oca. dijimos que estaría
bueno más que hacer charlas, empezar a practicar la organización
horizontal que tanto nos gusta (esto lo digo en mis palabras eh) y en
lugar de talleres donde se diferencia a los que saben de los que no,
hacer divisiones temáticas en el programa e invitar a todos a
participar-debatir. es decir no caer en el experto que viene a bajar
línea.

además el día anterior habíamos estado charlando con un compañero de la
sala alberdi, y que estaría bueno que participen del piratefest contando
su experiencia y además acercándonos organizativamente. fran también va
a avisar a sus conocidos de velatropa. estaría bueno invitar a otros
espacios autogestionados también.

sobre la fecha, confirmar que es 14 y 15 de abril al mismo tiempo que la
asamblea general del PPI.

otra cosa que hablamos fue en empezar a usar las plataformas de
organización de trabajo como crabgrass (el de we.riseup.net) o chili (el
que usamos en el hacklab), de forma que las cosas que hay que hacer
queden bien determinadas y no se pierdan entre los mails de la lista.

le delegué la bandera a paulo.
