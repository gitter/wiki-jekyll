---
title: Guía para redes sociales
layout: post
categories: []
---

Sacado de la internet, para traducir.
<https://docs.google.com/document/d/1pueKyX2VQsfOMWTmUDAJkueaMKsdrQlOSxZf_1Hvc5A/edit>

I started a public document on Google which I shared hoping people would
add their own tips about best practices for Social Media when it comes
to news gathering and distributing news. I decided to post it here as
well in its current form. If you'd like to add your own tips, click the
link above.

Twitter
-------

Build lists of sources using Twitter lists. Use this to get ahead of
your competition, news breaks on Twitter more often now than it does on
mainstream news sources. (How to create Twitter Lists)

Follow accounts that help inform you about the topics you cover and
retweet important news they share to inform your followers. Retweet your
competition, if you're sharing the best news from everywhere, people
will follow you for everything they need to know.

Use TweetDeck to monitor multiple lists on one screen, I prefer the old
version of Tweetdeck because it has more options.

Hand craft your tweets for all your new articles. Ask questions. "Do you
agree with my take here?" "Is \_\_\_\_\_\_\_ the next great point
guard?" Questions have the highest engagement and will help build your
audience. (But if you ask questions, have some intention of using the
feedback in some way - otherwise it's faux engagement)

Share interesting articles by other people. Retweet great articles that
they link to on their own Twitter accounts.

Use hashtags like \#wikileaks \#syria \#ows related to your topic etc so
your tweets are seen by a larger audience.

Do Twitter searches (search.twitter.com) and look up the subjects you
write about. Respond to people who tweet about things you're writing
about, get their attention. Don't link them to your articles right away,
build a relationship over time and they'll follow you and get the links
by following your feed.

Search Topsy.com to find the most relevant and influential tweets

Search Research.ly's PeopleBrowsr for old tweets that you might need.
Enter in a keyword or a username, and you can search a number of days
back (like 60 days back for 2 months ago) to narrow your search. They go
back about 2 years for free right now.

Read "The 100 Twitter Rules To Live By" and live by them

Sign up for a free \@muckrack account and get daily digests of top
journalists' tweets about your subject of interest. Crucial for journos
& PR pros.

Use MuckRack.com as a way to find journalists by company or by beat and
add them to Twitter lists to monitor news that breaks in those topics.

Incredible curation tools guide that I was sent this AM (via \@scoopit):

Consider balancing the types of tweets with a ratio of posts addressing:
(1) what you do; (2) what you love; and (3) who you are.

Use a URL shortening service, like <http://bit.ly> so you maximize the
room you have to share information in your tweets and so you can track
how many people are clicking and sharing your tweet. Try to keep your
Tweets under 120 characters so others can add comments to your tweet in
a RT.

Don't start tweets with a Twitter handle unless you want that Tweet to
be seen by a limited audience (by the person whose handle you've started
the Tweet with, and only anyone who follows both of you)

Another good place to build source lists is with directories that media
companies offer: New York Times and Reuters for example

Be careful about the sources you find on Twitter. Verify before you
retweet and add a caveat if you're unsure. When in doubt, don't tweet
and spread misinformation, dig deeper and verify first. There's no harm
in asking questions, sometimes crowdsourcing can help verify. "Is
\_\_\_\_\_ a legitimate account/source?" Consider using a service like
Storyful to help verify socially sourced reports.

Credit your sources with a hat tip, a retweet, anything. People are
appreciative to see their content (or their find) shared.

Facebook
--------

Turn on Subscribe feature on your personal Facebook. This will let you
share stuff you only want to share with the people who you want to
receive it.

Run polls asking people what they think about a specific story or
subject. In a sports example, who do they think is the best player at
any given position, or anything else that will drive debate and
comments. Do they think the Goldman Sachs' resignation letter author is
sincere? Be provocative, get them to think and weigh in.

Post videos and photos, don't just make it a feed with all links to your
articles. Mix it up.

Find Facebook Groups related to the subjects you cover. Get involved in
the conversations there. Over time they'll head to your page and then to
your website and you'll build up a readership.

There are close to a billion people on Facebook, it's a huge driver of
traffic once you get the ball rolling. It won't happen overnight but
it's worth putting in the time because you'll eventually have a reliable
major source of traffic to your website.

Create an interest list that focuses on a topic you want to help gather
information about, or to make it easy for people to follow other people
with similar interests. Example: Reuters Journalists list

Tumblr
------

Get on Tumblr and share your articles. Follow people who write about the
topics you cover, follow people who blog about college sports if that's
your focus. Reblog their posts and they'll follow you back.

I try to reblog and share more than I post of my own. I do about a 40/60
split between my content and the content I share of others. (I run
<http://sbnation.tumblr.com>)

Tag your posts \#gif \#tech \#news \#politics \#news \#sports
\#collegefb \#collegebb \#basketball \#football etc so they're seen by
tag editors who will then promote them on the respective tag pages. You
can find widely used tags at <http://www.tumblr.com/explore>.

Pinterest
---------

Originate all pins to places on your Website or social sites so that
re-pins always come back to you.

Other
-----

Keep an eye on your competitors. Watch what they're doing, steal some of
their good ideas and put your own spin on them. Over time you'll build a
friendly relationship with some of them and they may actually link to
your stuff, you should do the same. There's a link economy when it comes
to blogs and it's built on sharing each other's stuff, it helps make
your own content stronger and more well rounded and vice versa.

Using a tool like TrendSpottr for real-time viral content discovery can
also help to identify emerging stories that have high viral potential
and engagement. It identifies the top trending content (links, hashtags,
sources) for any keyword, topic or even Twitter list URL. GT example,
this is a link to find the most trending content about Syria in real
time. For HootSuite users, TrendSpottr is also available in HootSuite's
new App Directory. Useful overview videos also available.

Use Storify to combine different types of social content and add context
between them to make it more understandable in a narrative format.

Use Storyful.com/pro to help verify reports and tweets you see on
Twitter, Facebook and YouTube. Follow \@StoryfulPro on Twitter to see
alerts, curated Twitter Lists and links to verified content on
<http://storyful.com/pro> Requests and questions can be sent to
Storyful's global team of curators 24/7 on curator\@storyful.com

Consider offering readers a way to ask questions and receive a video
answer. There's ways to explain via video that you can't quite get
across via text. One recommendation is to use something like VYou.com.
Answers can be archived and accessed by other readers. Can eventually
act as an FAQ.

Quora
-----

`       A place to ask questions and showcase your knowledge on a subject.`\
`       One example of a great, useful Quora thread: How does an investigative reporter get started?`\
`       Could also be used for readers to ask questions to understand more about complicated subjects: What exactly is a Credit Default Swap?`
