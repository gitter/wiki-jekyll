---
title: Actas:2014-10-07
layout: post
categories:
- Actas
- Actas
---

Reunión en La Barbarie
======================

Participantes:

-   aza
-   charly
-   fauno
-   melany
-   minitrue
-   seykron
-   tom

Temas: evento Foco Libre, Amparo por TPB, colaboración con Proyecto Sur,
Impresión de Utopías

Foco Libre
----------

Estuvimos trabajando en la organización de la mesa pirata y la
charla-debate que vamos a dar. En el pad hay una lista de cosas para
hacer, hay que asignarse las tareas.

PAD: <https://pad.partidopirata.com.ar/p/Propuesta_CharlaFocoLibre>

Amparo por TPB
--------------

Queremos armar una campaña de difusión del \#AmparoPirata. Algunas de
las ideas para hacer son:

-   Publicar todo el contenido libre que encontremos en TPB.
-   Publicar nuestros propios trabajos (fotos, escritos, videos, etc) en
    TPB.
-   Pedirle a artistas que distribuyen su trabajo libremente que lo
    suban a TPB.
-   Hacer una fiesta pirata para publicar contenido entre todos.
-   Pedirle a TPB que publiquen un banner de la campaña.

Las cosas para hacer son:

-   Crear la propuesta en la wiki (cof cof minitrue)
-   Crear los flyers (voluntarios?)
-   Escribir una guía de cómo publicar un torrent en TPB (voluntarios?)
-   Empezar a subir contenido a TPB (TODOS)

Colaboración con Proyecto Sur
-----------------------------

La idea es ir a ver cuáles son sus ideas, intenciones, propuestas,
expectativas, etc, y tratar de hacer un poco de social insertion con
nuestra forma de organización y participación.

Impresión de Utopías
--------------------

En Defensa del SL presta impresora para imprimir los interiores de los
libros, hay que coordinar con Gutemberg (seykron?) hacer al menos 30
tapas de cada uno y juntarse antes del 21 para armar los libros.
Minitrue juntó plata para pagar las tapas.

[Categoría:Actas](Categoría:Actas "wikilink")

[Categoría:Actas](Categoría:Actas "wikilink")
