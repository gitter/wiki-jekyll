---
title: TextSecure (SMS seguro)
layout: post
categories:
- Software Libre
- Barco de infraestructura
- Privacidad
- Guías
- Android
---

[Categoría:Software Libre](Categoría:Software_Libre "wikilink")
[Categoría:Barco de
infraestructura](Categoría:Barco_de_infraestructura "wikilink")
[Categoría:Privacidad](Categoría:Privacidad "wikilink")
[Categoría:Guías](Categoría:Guías "wikilink")
[Categoría:Android](Categoría:Android "wikilink")

![[Descargar TextSecure sin
GooglePlay](http://kiwwwi.com.ar/pastes/org.thoughtcrime.securesms_1.0.6.apk)
y [comprobar la
firma](http://kiwwwi.com.ar/pastes/org.thoughtcrime.securesms_1.0.6.apk.asc)](TextSecure.png "Descargar TextSecure sin GooglePlay y comprobar la firma")

[TextSecure](https://www.whispersystems.org/#textsecure) es una app
libre para Android que reemplaza la app de Mensajes de texto. La
diferencia es que los mensajes se guardan de forma cifrada en el
celular, por lo que sin contraseña no se pueden leer. Además, si la otra
persona tiene TextSecure instalado también, se cifran los mensajes, por
lo que si alguien intercepta los mensajes sólo va a leer basura.

La aplicación se instala desde GooglePlay, todavía no está en F-Droid.
Para lxs que no queremos asociar una cuenta de Google al celular, se
puede descargar desde el botón a la derecha. El .apk fue descargado
desde GPlay a un Android y luego exportado con GhostCommander a una
computadora con GNU/Linux y luego firmado con GPG P)

Luego de instalarlo, pide una contraseña que no hay que olvidarse y
luego nos pregunta si queremos importar los mensajes de texto que ya
recibimos a la base de datos cifrada. Si nos interesa mantenerlos,
decimos que sí y luego los borramos desde la aplicación de mensajes
insegura.

A partir de este momento todos los mensajes de texto van a llegar a
TextSecure. Si la otra persona lo está usando, la primera vez que nos
mande un mensaje nos va a preguntar si queremos intercambiar llaves. Con
este paso todos los mensajes que mandemos y recibamos de este contacto
van a estar cifrados en el camino de un celular al otro.
