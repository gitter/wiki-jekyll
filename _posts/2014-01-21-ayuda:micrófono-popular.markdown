---
title: Ayuda:Micrófono popular
layout: post
categories:
- Autoformación
- Traducción
---

[Categoría:Autoformación](Categoría:Autoformación "wikilink")
[Categoría:Traducción](Categoría:Traducción "wikilink")

Traducido de <http://occupytogether.wikispot.org/People's_Mic>

El Micrófono Popular se usa cuando no se puede usar sonido amplificado y
hay mucha gente como para escuchar fácilmente a la oradora.

La oradora dice algunas palabras por vez y hace una pausa para que la
gente que la escuchó repita la frase a la gente que está más lejos. Si
la multitud es muy grande, van a hacer falta varios ecos.

El uso efectivo del Micrófono Popular requiere que las personas oradoras
sean concisas y evitan la tentación de decir frases largas.

El \"Chequeo de Micrófono\" es cuando la oradora quiere llamar la
atención sobre el Micrófono y asegurarse que todas puedan oír: Todas
repiten el chequeo hasta que la oradora pueda ser escuchada por la
multitud.

Hay otras ventajas al uso del Micrófono además del beneficio utilitario
del sonido amplificado, como:

1\. Enfoca la atención sobre un problema y alienta a las personas a
hablar desde donde está, en lugar de enfocarse en las conversaciones
paralelas

2\. Involucra a las participantes en la conversación, porque deben
escuchar cuidadosamente a la oradora para poder repetir exactamente lo
que dijo

3\. Promueve la empatía con puntos de vista distintos a los propios,
porque tenés que decir las palabras de otra con tu propia voz

4\. Empodera a aquellas que no son tradicionalmente escuchadas, porque al
escuchar tu idea repitiéndose en unísono en el grupo es una evidencia
concreta que realmente te escuchan.

Notas
-----

En inglés \"Chequeo de Micrófono\" se abrevia \"Mic Check\", creo que
acá generalmente sería \"Hola Hola 1 2 3\"
