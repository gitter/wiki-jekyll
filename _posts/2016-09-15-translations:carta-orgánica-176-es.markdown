---
title: Translations:Carta Orgánica/176/es
layout: post
categories: []
---

Serán causas para la revocación de una responsabilidad delegada o
auto-asumida el no cumplimiento de las tareas acordadas, el no respeto
de los consensos alcanzados y la exclusión de otros piratas de las
responsabilidades y tareas.
