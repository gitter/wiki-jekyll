---
title: Translations:Carta Orgánica/60/es
layout: post
categories: []
---

El lugar en que sucederán las Asambleas puede cambiar con el correr del
tiempo, pudiendo darse estas tanto en espacios físicos como en espacios
digitales.
