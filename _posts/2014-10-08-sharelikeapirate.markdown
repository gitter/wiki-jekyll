---
title: ShareLikeAPirate
layout: post
categories:
- '2014'
- Minitrue
- Propuestas_en_curso
- Propuestas_por_minitrue
- '2014'
- Minitrue
- Propuestas_en_curso
- Propuestas_por_minitrue
---

Propuesta
---------

ShareLikeAPirate, tiene como fin aumentar la difusion del Amparo
presentado por el partido pirata utilizando la divulgacion de contenido
libre en la bahia pirata. La campaña es una herramienta para sumar
damnificados al amparo, partiendo del principio que esta medida es una
forma de censura que atenta directamente contra la libertad de expresión
de todo el mundo, consagrada en normativa interna (Constitución
Nacional, Constitución de la Ciudad de Buenos Aires) y en normativa
internacional como son los Tratados con Jerarquía constitucional. Con
esta campaña buscamos mostrar como el derecho a la libre expresion de
todo el mundo se ve violentado con esta medida.

ShareLikeAPirate, busca aumentar la exposicion a travez de tres
herramientas distintas, la creacion con junta y colectiva de contenido
libre, la indexacion del contenido libre en TPB y subir contenido libre
a TPB con el fin de difundirlo

**\[Crear\] Creacion Conjunta y Colectiva**

Se busca crear eventos donde artistas, programadores, hackers, makers y
quien quiera, generen contenido (musica, pintura, fotografia, software,
literatura,etc) licenciado bajo creative commons, a fin de subirlo a la
bahia pirata con el tag \[ShareLikeAPirate\] para compartirlo y sumarlo
al amparo.

**\[Bajar\] Indexacion del contenido libre en TPB**

La censura a TPB no solo no nos permite compartir el contenido generado
por nosotros, si no que pretende impedirnos acceder a contenido creado
por otros, vamos a sumar al amparo el contenido al cual no se puede
tener acceso a traves de TPB.

**\[Subir\] Subir contenidoa TPB**

Otros colectivos ya crearon contenido pirata con licencia creative
commons, como netlabels, revistas y artistas independientes. Impulsamos
a todo colectivo o particular a subir su contenido a TPB con el tag
\[ShareLikeAPirate\] y sumarse al amparo.

Tareas
------

**Armar Flyers**

**Crear Guia para subir contenido a ThePirateBay**

**Organizar la indexacion de contenido**

**Conectarse con netlabels, colectivos y particulares relacionados con
la culturalibre en argentinos e internacionales**

**Organizar fecha y lugar para evento.**

\'\'\'Contactarse con la gente de TPB para que pongan el flyer en
promobay\"

Reunion
-------

\'\'\'Objetivo: \'\'\' Reunirnos para organizar el evento y cosas que no
se hayan podido definir online.

**Lugar**: A definir

**Fecha**: En Principio el miercoles 15 de Octubre 18:30

[Categoría:2014](Categoría:2014 "wikilink")
[Categoría:Minitrue](Categoría:Minitrue "wikilink")
[Categoría:Propuestas\_en\_curso](Categoría:Propuestas_en_curso "wikilink")
[Categoría:Propuestas\_por\_minitrue](Categoría:Propuestas_por_minitrue "wikilink")

[Categoría:2014](Categoría:2014 "wikilink")
[Categoría:Minitrue](Categoría:Minitrue "wikilink")
[Categoría:Propuestas\_en\_curso](Categoría:Propuestas_en_curso "wikilink")
[Categoría:Propuestas\_por\_minitrue](Categoría:Propuestas_por_minitrue "wikilink")
