---
title: Translations:Carta Orgánica/37/es
layout: post
categories: []
---

Con el ánimo de mantener la horizontalidad y el dinamismo participativo
y autogestivo en las actividades regulares del Partido, todo Pirata
tiene derecho a proponer y llevar a cabo actividades con el fin de
sostener, impulsar, difundir y extender los principios piratas en todos
los espacios.
