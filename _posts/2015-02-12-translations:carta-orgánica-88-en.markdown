---
title: Translations:Carta Orgánica/88/en
layout: post
categories: []
---

Bureaucratic Bodies have the obligation to follow the Pirate
Assemblies\' will by following the mechanisms defined in [\#Title
III](#Title_III "wikilink") of this document and by elaborating
Proceedings of their activities.
