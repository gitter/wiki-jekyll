---
title: Charla en Bibliotecología
layout: post
categories: []
---

19/5 ¿Dónde estamos parados? La Ley 11.723 de Propiedad Intelectual
-------------------------------------------------------------------

Para iniciar un debate de forma abierta y colectiva es necesario saber
cuál es la situación actual. Para esto, en la primera charla, Mónica
Boretto nos va a introducir en tema (Abogada y docente de Derechos
editoriales y del autor en la carrera de Edición).

26/5 ¿Qué es el copyleft? Elementos para reformar el derecho de autor
---------------------------------------------------------------------

En el segundo encuentro vendrán integrantes del Partido Pirata para
presentar la filosofía del copyleft, su origen y la vigencia de la lucha
por el acceso abierto frente a la legislación restrictiva de la
propiedad intelectual. Por su parte, Evelyn Heidel, de la Fundación Vía
Libre, va a hablar sobre una serie de 14 puntos para reformar los
derechos de autor y facilitar el acceso abierto en nuestro país. La
propuesta sobre la que hablaremos es parte de un proyecto de la
Fundación Vía Libre y toma conceptos de otros movimientos a nivel
global, como es la Quadrature du net.

09/6 Una propuesta concreta para la modificación de la Ley 11.723
-----------------------------------------------------------------

Lucía Pelaya hablará sobre una propuesta concreta de reforma de la ley
actual sobre la que ha trabajado junto a otrxs bibliotecarixs en la
Subcomisión de Propiedad Intelectual, Acceso a la Información y Libertad
de Expresión de ABGRA. Luego de haber visto, en las charlas anteriores,
un pantallazo sobre los distintos puntos de vista, podremos analizar de
forma reflexiva la propuesta de la Asociación de Bibliotecarios (ABGRA)
y aventurarnos a un debate de carácter crítico y constructivo sobre este
problema que nos interpela cada día con más urgencia.

16/6 Repositorios digitales y acceso abierto
--------------------------------------------

En el último encuentro hablaremos con Fernando Ariel López sobre el
avance sustancial de nuestro país en políticas de acceso abierto, la
importancia estratégica que significa esto para el desarrollo científico
nacional y el rol decisivo que cumple el estado como regulador del
mercado editorial. (A lxs representantes del claustro nos parece
interesante ofrecer un pantallazo sobre este tema porque a fines del año
pasado el Congreso Nacional aprobó una ley que restringe parcialmente la
explotación comercial de la información científica obtenida con fondos
públicos, mediante la creación de repositorios digitales de resultados y
de datos de investigación. El de las editoriales científicas es un
ejemplo claro de mercado monopólico. Es controlado por un puñado de
empresas que a falta de políticas públicas imponen sus exigencias
comerciales a la comunidad científica. La ley de repositorios digitales
quizás sea un buen ejemplo de cómo el estado, al promulgar leyes, puede
efectivamente ocupar un rol decisivo como mediador entre los intereses
de las empresas y los intereses de la sociedad.)
