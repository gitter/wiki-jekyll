---
title: Translations:Carta Orgánica/57/es
layout: post
categories: []
---

Las Asambleas del Partido Pirata son de carácter democrático,
organizativo, solidario, amistoso, horizontal, descentralizado, abierto
y participativo.
