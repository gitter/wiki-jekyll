---
title: Translations:Carta Orgánica/27/es
layout: post
categories: []
---

Si el titular de plataformas de compartición de obras culturales no
pudiera ser localizado, se considerará a ese sitio pirata "tácito" y a
su difusión y protección como parte de los objetivos del partido.
