---
title: Translations:Carta Orgánica/7/es
layout: post
categories: []
---

Esta estructura aspira a que de ella nazcan los más diversos movimientos
humanistas, no solo dándoles apoyo, sino que la estructura misma se
replantee continuamente en función de la mejor coexistencia de todos los
movimientos que la incluyen y definen contextualmente.
