---
title: Translations:Carta Orgánica/48/es
layout: post
categories: []
---

Habrá Acuerdo sobre una propuesta cuando esta cuente con la aprobación
de un porcentaje a definir por reglamento no menor al 50% de los Piratas
Afiliados Participantes de la Asamblea.
