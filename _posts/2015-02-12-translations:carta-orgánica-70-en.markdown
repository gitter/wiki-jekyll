---
title: Translations:Carta Orgánica/70/en
layout: post
categories: []
---

The Permanent Assembly will call for a General Assembly with a grace
period of no less than fifteen (15) days and by reliable communication
of its Agenda to all Affiliated Pirates.
