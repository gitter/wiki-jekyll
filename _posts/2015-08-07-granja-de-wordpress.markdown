---
title: Granja de Wordpress
layout: post
categories:
- Barco de infraestructura
---

[Categoría:Barco de
infraestructura](Categoría:Barco_de_infraestructura "wikilink")

El sitio [partidopirata.com.ar](http://partidopirata.com.ar) es una
granja de [WordPress](https://wordpress.org). Esto quiere decir que es
posible (y muy simple!) crear sitios del tipo
**sitio.**partidopirata.com.ar e inclusive sin ser parte del dominio
partidopirata.com.ar.

Crear subsitios
===============

-   Loguearse como administrador de la red
-   Ir a la sección sitios y agregar un sitio nuevo
-   Agregar el subdominio en el DNS (hasta que tengamos un DNS que
    soporte \*.partidopirata.com.ar)

Crear blogs en sus propios dominios
===================================

-   Seguir los pasos de [\#Crear subsitios](#Crear_subsitios "wikilink")
-   Tomar nota del ID del sitio, en la URL donde dice **id=X**
-   En Ajustes, abrir la sección Domains
-   Agregar un dominio y el ID. **No tildar** la opción \"primary\".
-   Configurar el dominio para que sea un CNAME de partidopirata.com.ar

Actualizar la granja
====================

-   Ingresar por SSH
-   Hacer un backup
    `rsync -av --delete-after /srv/http/partidopirata.com.ar{,~}/`
-   Correr
    `/usr/local/bin/auto-wordpress /srv/http/partidopirata.com.ar`
