---
title: Translations:Carta Orgánica/151/es
layout: post
categories: []
---

Se dará como concluída una Propuesta cuando los responsables emitan un
comunicado con la descripción del proceso y los resultados obtenidos.
