---
title: Translations:Carta Orgánica/17/es
layout: post
categories: []
---

Será responsabilidad de los Piratas Afiliados atender tanto a las
actividades regulares como a las Asambleas Generales y otros eventos que
se refirieran a la organización, participación y difusión del Partido.
