---
title: Observatorio de ISPs
layout: post
categories: []
---

¿Hay provedores filtrando nuestro trafico? ¿Dandonos menos de lo que nos
venden? Este es el lugar para investigar, comparar y denunciar estas
practicas.
