---
title: Translations:Carta Orgánica/19/es
layout: post
categories: []
---

Los Piratas Afiliados podrán ser elegidos como representantes según sea
necesario por exigencias de la ley electoral o por necesidad de nombrar
a un representante para casos puntuales según las disposiciones de esta
Carta Orgánica y la de los Órganos partidarios que ella organice.
