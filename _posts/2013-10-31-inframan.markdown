---
title: InfraMan
layout: post
categories: []
---

Arreglar permisos de carpetas y archivos
----------------------------------------

\"777 el numero de los bestias\" - fauno 20xx

`find -type d -exec chmod 755 {} \;`
`find -type f -exec chmod 644 {} \;`

Agregar dominio
---------------

-   Agregar carpeta en /srv/http
-   Agregarlo a la conf de nginx
-   \... \<- fauno magic

Crear nuevo blog
----------------

Loguearse al WP e ir a administración de red. Follow the white rabbit.

Crear mail pirata
-----------------
