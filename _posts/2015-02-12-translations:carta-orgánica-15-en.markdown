---
title: Translations:Carta Orgánica/15/en
layout: post
categories: []
---

Affiliation will extinguish by affiliation to another party, resignation
or expulsion.
