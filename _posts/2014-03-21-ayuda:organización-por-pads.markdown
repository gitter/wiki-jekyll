---
title: Ayuda:Organización por pads
layout: post
categories:
- Organización
- Eventos
---

[Categoría:Organización](Categoría:Organización "wikilink")
[Categoría:Eventos](Categoría:Eventos "wikilink")

Esqueleto para organizar un evento usando un
[pad](http://pad.partidopirata.com.ar). Ir a editar/ver fuente, copiar y
pegar en un pad nuevo. Viene [del wiki del
HackLab](http://wiki.hackcoop.com.ar/RFC:Organización_por_pads).

Evento
======

Escribir una descripción del evento, hora y lugar acá.

Difusión
--------

### Grupos a los que invitar

Hacer una lista de los grupos, personas, etc. a los que invitar
personalmente al evento. Un \"che, estamos haciendo esto, venís? podés
participar con tu proyecto\", mueve bastante.

### Gacetilla

Escribir un texto invitación al evento, qué se va a hacer, qué va a
pasar, por qué, etc.

### Poster

Diseñar un poster y otro material gráfico

Organización
------------

### Cosas para hacer

-   Listar tareas
-   Asignarse como responsables al lado de cada una entre paréntesis
    ([Hacker:Fauno](Hacker:Fauno "wikilink"))

` Los comentarios o el progreso de una tarea se pueden poner inmediatamente abajo`

### Cosas hechas

Copiar y pegar las [\#Cosas para hacer](#Cosas_para_hacer "wikilink")
acá una vez que estén terminadas. La idea es poder tener un espacio
limpio donde se anota lo que falta hacer y se puede visualizar
fácilmente sin tener que filtrar leyendo, además de tener un segundo
espacio donde se visualiza lo que ya se hizo. Es una especie de Kanban
en texto plano :)
