---
title: Translations:Carta Orgánica/117/es
layout: post
categories: []
---

Serán elegibles para conformar la lista de candidatos todos los Piratas
Afiliados que cumplan con los requerimientos dispuestos por Ley para los
cargos en que se postulen.
