---
title: Sex And The Town (Season 3) DVD Review
layout: post
categories: []
---

One of many most productive original wires series of The City, Sex And
the nineties chronicles the lives of four female friends who reside in
New-York City. Sarah Jessica Parker stars as Carrie Bradshaw, a
widely-read sex writer for a local paper. She\'s joined by her three
closet buddies Miranda Hobbs (Cynthia Nixon), Samantha Jones (Kim
Cattrall), and Charlotte McDougal (Kristin Davis). Together, they form a
clique of person reliant upon each other for suggestions about
relationship, job, and life choices\...\
\
The Sex And The City (Season 3) DVD includes a quantity of hilarious
episodes like the season premiere \"Where There\'s Smoke\" by which
Carrie, Miranda, Samantha, and Charlotte head to Staten Island to judge
the Brand New York City Fire Department\'s \"Calendar Contest\".
Provides becomes enthusiastic about an enterprising politician, and
Charlotte decides she has to get married by year\'s end. Meanwhile,
Samantha gets it down with a fireman who must depart whenever a call
will come in and Miranda involves terms with some personal thoughts
because of this of her visit with the firemen? Other notable episodes
from Season 3 include \"Attack of the Five Foot Ten Woman\" in which
Carrie runs across Mr. Big\'s wedding story and Samantha finds a spa
where in actuality the massage therapist is claimed to-do just a little
\'extra,\' and \"Are We Sluts?\" in which each of the women encounters a
problem associated with their revolving door relationships?\
\
Below is just a listing of episodes included on the Sex And The Town
(Season 3) DVD:\
\
Episode 31 (Where There\'s Smoke) Air Date: 06-04-2000\
Episode 32 (Politically Erect) Air Date: 06-11-2000\
Episode 33 (Attack of the Five Foot Ten Woman) Air Date: 06-18-2000\
Episode 34 (Boy, Girl, Boy, Girl) Air Date: 06-25-2000\
Episode 35 (No Ifs, Ands, or Butts) Air Date: 07-09-2000\
Episode 36 (Are We Sluts?) Air Date: 07-16-2000\
Episode 37 (Drama Queens) Air Date: 07-23-2000\
Episode 38 (The Big-Time) Air Date: 07-30-2000\
Episode 39 (Easy Come, Easy Go) Air Date: 08-06-2000\
Episode 40 (All-Or-Nothing) Air Date: 08-13-2000\
Episode 41 (Running with Scissors) Air Date: 08-20-2000\
Episode 42 (do not Inquire, do not Notify) Air Date: 08-27-2000\
Episode 43 (Escape from Nyc) Air Date: 09-10-2000\
Episode 44 (Sex and Another City) Air Date: 09-17-2000\
Episode 45 (Hot Child in the City) Air Date: 09-24-2000\
Episode 46 (Frenemies) Air Date: 10-01-2000\
Episode 47 (What Goes Around Comes Around) Air Date: 10-08-2000\
Occurrence 48 (Cock-A-Doodle-Do) Air Date: 10-15-2000\
\
\
