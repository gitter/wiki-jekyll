---
title: Plantilla:SoyResponsable
layout: post
categories: []
---

<noinclude>

Generar un listado con las propuestas de las que soy responsable

Listo para usar:

{{SoyResponsable \| responsable = Vos }}

<noinclude>

<includeonly>

Propuestas de las que soy responsable
-------------------------------------

<DPL> category = Propuestas por </DPL> </includeonly>
