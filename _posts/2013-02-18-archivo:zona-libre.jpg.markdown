---
title: Archivo:Zona libre.jpg
layout: post
categories: []
---

[Zona
libre](https://secure.flickr.com/photos/christopherdombres/4792941198/in/photostream)
\-- por [Christopher
Drombres](https://secure.flickr.com/photos/christopherdombres/),
[cc-by](http://creativecommons.org/licenses/by/2.0/deed.en)

[Category:Media](Category:Media "wikilink")
