---
title: Chili - Administrador de proyectos
layout: post
categories: []
---

ChiliProject es un administrador de proyectos basado en web. Apoya a tu
equipo a travez de toda la vida de un proyecto, desde el inicio, la
discusion, el planeamiento, el seguimiento y el reporte de progreso para
compartir colaborativamente el conocimiento.
<https://www.chiliproject.org/>

El Partido Pirata tiene instalado su administrador de proyectos en
<http://adhoc.partidopirata.com.ar>

**[Tutorial sobre como utilizar
Chili](http://partido-pirata.blogspot.com.ar/2012/08/tutorial-para-usar-el-programa-adhoc.html)**
