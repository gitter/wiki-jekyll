---
title: Translations:Carta Orgánica/24/en
layout: post
categories: []
---

Affiliated Pirates give themselves a reciprocal and indistinct power to
mutually defend themselves and other pirates before the courts in case
of any members being sued civilly or becoming defendant criminally for
practicing their constitutional right to inform and be inform, by
sharing cultural commons, in material or digital forms.
