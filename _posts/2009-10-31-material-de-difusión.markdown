---
title: Material de Difusión
layout: post
categories: []
---

Videos
======

Publicados
----------

1.  Primer video del partido pirata by Gabriel

<http://www.youtube.com/watch?v=uhVJlH2IBBQ>

En Producción
-------------

Videos de otros partidos pirata
-------------------------------

### Alemania

<http://www.youtube.com/watch?v=xeC_MKErHGY>

<http://www.youtube.com/watch?v=3vsR-WF0nLQ>

### Brasil

<http://www.youtube.com/watch?v=WnXOoxCw33Y>

Ideas para futuros Videos
-------------------------

1.  Crear un video de la misma manera en que se creo el spot de gmail
    organizado por google
    (http://mail.google.com/mail/help/intl/es/gmail\_video.html) donde
    en el video aparezca un nene leyendo y diga \"Me gusta leer y
    compartir. Yo soy Pirata\" y de la misma manera en que pasan el
    sobre de gmail pase un libro hacia un costado y que la gente vaya
    armando el video. Que lean el libro lo pase hacia un costado (dando
    la sensación de compartir ) y diga \"Yo soy Pirata\".

<!-- -->

1.  
