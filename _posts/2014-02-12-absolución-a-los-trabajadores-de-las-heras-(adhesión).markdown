---
title: Absolución a los Trabajadores de las Heras (Adhesión)
layout: post
categories:
- Adhesiones
---

[Categoría:Adhesiones](Categoría:Adhesiones "wikilink")

Publicada el [2014-02-04](2014-02-04 "wikilink") por
[CeProDH](http://ceprodh.org.ar/spip.php?article529).

Adhesión enviada por [Pirata:Aza](Pirata:Aza "wikilink") el
[2014-02-09](2014-02-09 "wikilink").

PETITORIO ABSOLUCION DE LOS TRABAJADORES DE LAS HERAS
-----------------------------------------------------

Los abajo firmantes repudiamos las injustas condenas dictadas por el
tribunal de Caleta Olivia, el pasado 12 de diciembre, a nueve
trabajadores petroleros de Las Heras, Santa Cruz, de los cuales 4 fueron
condenados a cadena perpetua, uno de ellos por ser menor de edad al
momento de los hechos derivado a un régimen especial, y cinco, a cinco
años de prisión con los cargos de coacción agravada. A los trabajadores
se los condena sin ninguna prueba y con gravísimas violaciones a los
derechos humanos, por la muerte del policía Sayago, ocurrida en la
pueblada de esa localidad en el año 2006 cuando salieron a luchar contra
el impuesto a las ganancias y por mejores condiciones de trabajo. Los
abajo firmantes exigimos la absolución de los trabajadores, dado que
este es fue un juicio absolutamente armado. Durante el trascurso del
mismo y en los alegatos, los abogados defensores de los petroleros
(amenazados constantemente por este tribunal y sancionados) demostraron
la inocencia de los trabajadores por no encontrase pruebas. Lo único que
se pudo demostrar fueron las torturas, los apremios ilegales, las
vejaciones y las amenazas que sufrieron los petroleros, por la policía
provincial durante los tres años que estuvieron detenidos, para obtener
"pruebas" Un accionar propio de la dictadura militar". Denunciamos al
Tribunal Oral de Caleta Olivia que llevó adelante el juicio y luego
condenó avalando los testimonios arrancados bajo torturas. El mismo
fiscal Candia se atrevió a decir con total impunidad como muestra de
esto, que una bolsa en la cabeza y un par de "cachetadas" el no lo
consideraba torturas. Las denuncias que se realizaron durante el mismo,
acompañadas por cientos de Organismos de Derechos Humanos,
Organizaciones sindicales, Sociales, Políticas, llevaron a que la
Secretaría de Derechos Humanos de la Nación enviara a Caleta Olivia un
pedido de informe sobre el juicio, que estuvo armado contra los
trabajadores, plagado violaciones a los derechos humanos, como las
brutales torturas de las que fueron víctimas durante su detención y las
amenazas constantes a sus familias. Estas durisimas condenas, son uno de
los más graves ataques contra los trabajadores, desde el 83 a esta
parte, por haber salido a pelear por sus derechos. Su único objetivo es
que esta condena sea ejemplificadora para que los trabajadores no se
enfrenten a las petroleras y a las grandes patronales. Sentando un
gravísimo antecedente para todos los luchadores del país, así como, la
impunidad de la justicia que avala condenar a trabajadores en base a
testimonios obtenidos bajo tortura. La condena a los trabajadores, no
implicó cárcel inmediata hasta que no quede firme la condena, tiempo que
utilizaremos lograr la verdadera justicia, que es la absolución de los
compañeros. Los primeros días de febrero se apelará en la instancia de
casación en el Tribunal Superior de Río Gallegos.

Por eso exigimos: Absolución inmediata e incondicional a los
trabajadores petroleros de Las Heras
