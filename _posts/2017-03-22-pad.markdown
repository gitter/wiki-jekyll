---
title: Pad
layout: post
categories:
- Herramientas
- Software Libre
---

[Categoría:Herramientas](Categoría:Herramientas "wikilink")
[Categoría:Software Libre](Categoría:Software_Libre "wikilink")

¿Para qué?
----------

Los pads son una herramienta genial para escribir textos
colaborativamente y consensuar rápido y en vivo. Podés ver en tiempo
real los cambios que está haciendo cada participante, y además
identifica a cada persona con un color distinto. También podés
exportarlo a diferentes formatos y ver un historial de cambios.

¿Y cómo son?
------------

Así empiezan:

![](Pad-en-blanco.png "Pad-en-blanco.png"){width="650"}

Y así quedan:

![](Pad.png "Pad.png"){width="650"}

Se ve que este es un pad saludable, por la cantidad de colores :)

¿Cómo se usan?
--------------

Primero y principal: no hay botón de **Guardar**, no hace falta porque
el servidor se ocupa. Digamos que guarda cada letra que apretás, para
mostrarsela al resto de las personas que están participando en tiempo
real! Si prestás atención, a veces se puede ver cómo va surgiendo la
idea de un texto mientras quien lo escribe borra y reescribe todo el
tiempo :P

Ahora sí, el menú de la izquierda:

![](Pad-barra-izquierda.png "Pad-barra-izquierda.png"){width="650"}

El botón de **borrar colores** borra todo lo del documento, a menos que
tengas texto seleccionado. En ese caso, sólo borra los colores de ahí.
Es muy útil para limpiar algunas partes y dejar el caos hermoso en
otras.

El menú de la derecha:

![](Pad-barra-derecha.png "Pad-barra-derecha.png"){width="650"}

Los **viajes en el tiempo** sirven para ver cómo se fue haciendo un pad
desde el principio.. probalo, es muy flashero :)

### Algunas buenas prácticas para colaborar

-   Ponete un nick
-   Si es posible, en vez de borrar tachá, así se sabe qué es lo que
    modificaste
-   No borres los colores de todo el documento sin consensuar
-   Elegí un color que armonice con los demás (que la vista pare de
    sufrir!)
-   Hacé referencias a [31
    minutos](http://www.youtube.com/watch?v=rBUNrdoY51g)

¡Quiero uno ya! ¿Cómo hago?
---------------------------

Para crear un pad podés ir a cualquiera de estas páginas y elegir un
nombre

-   [RiseUp](https://pad.riseup.net): se borra si pasan 30 días sin
    ediciones
-   [Pad Pirata](https://pad.partidopirata.com.ar): siguen ahí hasta que
    alguien los vandalice

O directamente desde la barra de direcciones, borrás el nombre de un pad
y te inventás otro (si estás en <http://pad.partidopirata.com.ar/p/bla>
podés ir a <http://pad.partidopirata.com.ar/p/nuevo> y si no había un
pad con ese nombre, ya lo creaste).

Y si quisieras tener tu propio programa de pads, en tu propia página,
para algo es software libre! [Acá](http://etherpad.org/) está la página
y [acá](https://github.com/ether/etherpad-lite) el código.
