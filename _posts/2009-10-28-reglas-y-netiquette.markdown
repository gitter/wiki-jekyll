---
title: Reglas y netiquette
layout: post
categories: []
---

Reglamento del Partido Pirata Argentino
=======================================

Las siguientes reglas están para ayudarte a comprender qué significa ser
parte de el Partido Pirata Argentino, es decir, qué está permitido
publicar y qué no.

Recuerda que lo que hagas en Partido Pirata Argentino está sujeto a las
Normas de Convivencia y a los Términos Legales, que aceptaste al
registrarte.

Sé amable y respetuoso cuando interactúes con otros miembros de el
Partido Pirata Argentino. Si estás buscando un lugar para molestar,
agredir, acosar o intimidar a otros, Partido Pirata Argentino no es el
sitio apropiado.

Sé responsable del contenido que subas. Antes de hacer el último click,
modera el mismo de forma sensata. Es simple: si al momento de publicar
algo dudas de que pueda ser visto por un niño o por una persona mayor,
implica que debes revisarlo.

No brindes información que no sea veraz.

Cuida tu vocabulario. Adécuate a la lista de correo, foro, IRC y wiki en
los que participes y a los temas tratados.

Respeta los derechos de autor. No incluyas como propio contenido ajeno
que hayas recopilado por ahí. Cuando publiques material de otro medio,
cita la fuente. De existir, siempre incluye el link. No publiques
información que pudiese asociarse con actos de piratería informática.

Cuando en una lista de correo, foro, Irc y wiki contestes un tema, no
desvirtúes la conversación ni incluyas contenido sin sentido a
propósito. A nadie le gusta perder el tiempo.

No divulgues datos privados personales ni de terceros. Tampoco opines
sobre personas u empresas sólo para perjudicarlas.

No publiques ni solicites libros, música, películas ni cualquier otro
bien cultural para descargar (o vender) si no está autorizado para ser
distribuido de esa forma. Lo mismo para los sitios que permiten bajarlo.

No utilices listas de correo, foros, wiki y temas como canales de chat.
Ya existen webs y programas específicos para eso como el IRC.No
publiques más de un mensaje o tema con el mismo contenido. Una vez es
suficiente.

Algunas cosas pueden resultar impactantes para ciertas personas. Si vas
a publicar algo que tal vez afecte la sensibilidad, adjúntalo en un
archivo y acláralo.

Pautas de Convivencia
=====================

Las siguientes reglas están para garantizar una buena convivencia entre
todos los miembros de el Partido Pirata Argentino:

No publiques datos privados de terceros.

No publiques más de un mensaje o tema con el mismo contenido.

No utilices los foros y temas como canales de chat.

Si publicas material de otro medio, cita la fuente.

No pidas ni publiques material que viole derechos de propiedad
intelectual ni tampoco información que pudiese asociarse con actos de
piratería informática.

No publiques material sin sentido en forma intencional, ni hagas uso de
la publicidad en forma abusiva. A nadie le gusta el spam.

No están permitidas las faltas de respeto, la discriminación y el
racismo en cualquiera de sus formas.

No publiques material que pudiese atentar contra la moral y las buenas
costumbres, contra la protección de la minoridad y la infancia, ni
contra los derechos de los animales.

No emitas opiniones sobre personas u empresas con el único objetivo de
perjudicarlas. No divulgues mensajes cuyo contenido pudiese dañar a un
tercero, o atentar contra su dignidad e integridad.

Problemas Personales
====================

Al comunicarnos por escrito con personas que no conocemos personalmente
pueden surgir malentendidos, desacuerdos y mas. Es una buena practica
releer cualquier parrafo que encontremos ofensivo y considerar que puede
tener algun significado alternativo, asi mismo se recomienda releer
nuestros mensajes antes de enviarlos para asegurarnos que no haya algun
parrafo/oracion que pueda ser malinterpretado.

En caso de que surga algun malentendido, desacuerdo, etc. se recomienda
tratarlo por privado con esa persona y asi evitar mails innecesarios y
la inclusion de mas personas al conflicto.

Uso de la lista del Foro y la lista de correo
=============================================

-   Intentemos mantener más o menos ordenados los hilos y los temas y no

tratar todo en todos los hilos. Si hay un hilo sobre una acción
concreta, discutamos la acción concreta. Si hay un hilo sobre el
manifiesto, discutamos sobre el manifiesto. Si un hilo dispara una nueva
idea, iniciemos un nuevo hilo, y si hace falta diciendo: \"A partir de
lo charlado en el hilo Tal y Tal, se me ocurre esta otra
idea/iniciativa/temática \...\"

-   Ussemos las buenas costumbres de la

netiqueta, y en vez de pegar el texto completo en el cuerpo del mail, se
cite su fuente con un link, y se agregue una breve descripción de su
contenido y por qué hay que leerlo, y de ser muy necesario, extraer una
parte y pegarla en el cuerpo del mail.

A cerca de los acortadores de direcciones web
=============================================

Intenten en lo posible evitar usar acordadores de direcciones por
motivos de seguridad ya sea en la Wiki, IRC, Foro y Lista de Correos.

Upload de Archivos
==================

Por cuestiones legales y posibles aprietes en la lista se resolvio que
la mejor forma de agregar archivos a la wiki seria subirlos en un sitio
publico y enlazarlos desde aqui.

-   <http://www.megaupload.com/>
-   <http://www.rapidshare.com/>
-   <http://www.2shared.com/>
-   <http://www.4shared.com/dir/22009525/6c531da9/PPArg.html>

Más información sobre reglas y netiquette
=========================================

<http://es.wikipedia.org/wiki/Netiquette>
