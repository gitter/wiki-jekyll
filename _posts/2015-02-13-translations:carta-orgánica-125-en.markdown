---
title: Translations:Carta Orgánica/125/en
layout: post
categories: []
---

-   Contributions granted by the Permanent Parties Fund in charge of the
    State, assigned to and administered by the Pirate Assembly;
-   Contributions from affiliates and supporters, in ammount and
    conditions decided by the Assembly;
-   Contributions from employees, officials or candidates, selected or
    elected, from their appointments salary;
-   Contributions and donations without charge, only and when they
    aren\'t forbidden by law, or the ethical and/or ideological
    Principles of the Party;
-   Real State Properties acquired with Party\'s funds or donations to
    this end, that are to be legally registered to the Party;
-   Other contributions allowed by law that don\'t contradict the
    ethical and/or ideological Principles of the Party.
