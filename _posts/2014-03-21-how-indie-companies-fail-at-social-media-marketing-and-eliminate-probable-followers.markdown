---
title: How Indie Companies Fail At Social-Media Marketing And Eliminate Probable Followers
layout: post
categories: []
---

In a Indie Group? Odds are, if you are, and a lot of people are (which
has saturated industry for the top), you\'re marketing your band to
prospective enthusiasts on Facebook, MySpace, Reverbnation, Twitter, and
a host of different websites.Chances additionally are that you\'re
unknowingly spamming 1000s of people and turning people from your band
in the place of turning them on for your band. It isn\'t your fault.
Most People Are carrying it out. However, if we desire to switch this
around we have to get classy instead of supply this demanding decline of
unbiased music.First down, it ought to be clear by now that spamming is
not the way to obtain new followers. You all know it to become correct
on some level. Itis exactly that we don\'t need it to be true \]. You
want to think that it\'s possible fit the time in, to take a seat in
front of our computers, and generate our band the value it deserves.
This Can Be A half-truth. We can definitely create our band\'s report
and gain new supporters by investing in time marketing online,
nonetheless it must be done the smart way.I recently put in place a
Facebook site, and I was overwhelmed by the large number of spam
delivered to me by independent artists. Right-now I\'ve more than 500
messages in my inbox, and I could assure you that do not require are
precise legitimate, customized messages. I never expected to be on any
of these E-mail lists. Any performer who promotes this way is very
clearly indicating which they would rather send the media a message such
as for instance \"Hey! Check us out!\" than really ask pleasantly and
provide the necessary information with professionalism and a few class.
That is not to express you-can\'t use themes. By all means, utilize a
format. You never want to have to sort every e-mail from-scratch. But at
the very least create the e-mails individual. If you\'re calling an
audio blog, make sure you\'re texting them in ways that\'s sincere and
tailored. Incorporate all of the necessary information on your group
including cd download links, bio, and website information.What many
companies don\'t realize is that the general public loses respect for
them once they junk. Into effectively marketing your group if you set
the exact same timeframe you\'ll get a great deal more success.
Individuals are on information overload. First, printing advertising
turned devalued due to the increase of social media advertising,
advertising through person to person electronically and so on. Now the
printing market as a whole is suffering. Subsequently social media was
shared as the holy grail, but because it ends up (and this is actually a
position indie groups need to keep yourself updated of), OTHER-PEOPLE
need todo the speaking for this to function. Which means whenever you
speak yourself up or send mass emails about yourself (unless it is on a
proper mailing list), it has a reverse influence. OTHERS have to talk
about your band.What type of other people? Anybody. But ultimately not
your Mom.Ideally, the audio websites must certanly be speaking about
your band. Podcasts must certanly be discussing your group. Magazines
and audio sites should really be speaking about your group. Online-music
neighborhoods ought to be placing your mp3is. The sad thing about our
problem today is that most indie bands do not market themselves to audio
sites or podcasts or these areas. They just toss their audio at the
normal social media wall to see if it will stick anywhere. This only
doesn\'t work, folks.Yes, you should have a Facebook site, and there are
certainly a ton of approaches to promote it. You can sign in while the
page and answer comments, provide advantages to Facebook page members,
offer contests and drawings via applications such as for instance
Wildfire, put in place a specific Facebook marketing campaign, join and
discuss additional Facebook websites, and post your music and films when
appropriate. Use movie and widgets to advertise yourself and inspire
others to do the exact same. Your page must grow organically. Generally
incorporate it within your mail signature.Should you\'ve a MySpace page?
Yes, nonetheless it\'s controversial at this point. Set it up expertly
but don\'t spend enough time on it.You market yourself for your love of
the music. You think that it should be heard by individuals. Then show
the proper respect to it. When was the final time you heard someone
describing how they discovered a fresh group, and they said \"Yeah, I
acquired this email from their store saying \'Please! Get our recording
for free!\' and I was hooked!\" It does not happen this way.
Consequently present your music the appropriate regard and push your
music behindthescenes. Find other-people discussing you.
