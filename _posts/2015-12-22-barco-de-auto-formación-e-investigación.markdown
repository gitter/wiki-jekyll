---
title: Barco de Auto-Formación e Investigación
layout: post
categories: []
---

Objetivos generales
===================

-   Generar un espacio horizontal para la capacitación y autoformación
    de piratas en temas que consideren necesarios.
-   Generar un espacio para la investigación en temas piratas.
-   Aumentar el capital cognitivo de las piratas.
-   Generar conocimiento y permitir su divulgación, dejando en
    manifiesto la efectividad de la horizontalidad.
-   Fomentar la construcción colectiva de conocimiento orientado a la
    acción pirata
-   Que piense y delinee nuestras utopías piratas
-   Fomentar el uso de licencias libres en la producción de materiales.

Autoformación.
==============

Actividades
-----------

-   Encuentros físicos entre piratas para capacitarse en temáticas
    puntuales.
-   Divulgación de contenido educativo.
-   Permitir un intercambio fluido de material/conocimiento entre
    piratas con distintos niveles de experiencia en temas concretos.
-   Generar talleres y reuniones abiertos a no piratas ej: Grog&Tor
-   En caso de ser considerado necesario, se podría pedir la
    participación de gente ajena al ppar que permita ayudar en el
    proceso de formación.(temporary sudoers)

Metodología
-----------

-   No hay cantidad minima de piratas para iniciar el proceso de
    autoformación.
-   Puede seleccionarse cualquier tema que considere necesario.
-   Los procesos de autoformación deben tener : Tema, Tiempo estipulado,
    en caso de haber reuniones indicar lugar cantidad fecha y horario de
    reunión/es, objetivos.
-   Se envía un mail a la lista de autoformación para notificar sobre el
    inicio con Tema, tiempo estipulado, datos de reuniones y objetivos.
-   Al finalizar una instancia se requiere presentar material, ya sea un
    resumen de la actividad, material de difusion , papers, etc.

Investigación
=============

Actividades
-----------

-   Generar una biblioteca de contenido que pueda ser utilizado por
    investigadoras y piratas en formación.
-   Generar un equipo de peer review que pueda evaluar los trabajos.
-   Generar un espacio que fomente la investigación independiente y
    horizontal.
-   Palabras relacionadas con temáticas de investigación: informática,
    seguridad, privacidad, copyright copyleft, derechos de autor,
    telecomunismo, feminismo y tecnología, economía, filosofía,
    programación, organizaciones experimentales, redes de pares,
    hacktivismo, software libre, arte, cultura y tecnología, piratería
    file sharing, internet, latinoamerica, argentina
-   publicación de las investigaciones bajo licencias libres

Metodología
-----------

-   Cualquiera puede iniciar un proceso de investigación, solo es
    necesario avisar a las personas involucradas en el barco.
-   En caso de necesitar ayuda de otras piratas, es necesario enviar
    mail a la lista indicando, tema de investigacion, objetivos y
    plazos.
-   La estructura del ensayo/paper deberá ser acorde a los estándares de
    la disciplina dentro de la cual se podría englobar. Por ejemplo, un
    paper de investigación sobre hactivismo en argentina desde una
    perspectiva sociológica, deberá tener una estructura y formato
    acorde a publicaciones académicas en el ámbito de la sociología.
-   Previo a la publicación de un trabajo es necesario un proceso de
    peer review.
-   Se promueve la publicación de artículos con pseudonimos/nicks.
