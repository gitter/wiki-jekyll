---
title: Declaración de Principios 3.2 (Suecia)
layout: post
categories: []
---

Introducción

El Partido Pirata tiene como meta, cambiar la legislación global para
facilitar el desarrollo de la sociedad informática, que está
caracterizada por diversidad y libertad. Esto lo logramos al pedir un
nivel mayor de respeto por los ciudadanos y su derecho a la privacidad,
y al reformar el derecho de autor (copyright) y las leyes de patente.

Las tres principales creencias del Partido Pirata son la necesidad de
protección de los derechos de los ciudadanos, la voluntad de liberar
nuestra cultura, y el entendimiento de que las patentes y los monopolios
privados están dañando a la sociedad.

La nuestra es una sociedad controlada y bajo vigilancia donde
prácticamente todo el mundo está registrado y en observación. No está en
el mejor interés de un estado judicial moderno imponer la vigilancia a
todos sus ciudadanos, de ese modo tratándolos como sospechosos. La
Democracia presupone una protección fuerte de los derechos de los
ciudadanos.

El copyright fue creado para beneficiar a la sociedad fomentando actos
de creación, desarrollo y difusión de expresiones culturales. Para
conseguir esos objetivos, necesitamos un delicado equilibrio entre las
demandas comunes de disponibilidad y distribución por una parte, y el
reconocimiento y remuneración de las demandas del creador por la otra.
Nosotros reivindicamos que el sistema actual del copyright se ha salido
de este equilibrio. Una sociedad donde las expresiones culturales y el
conocimiento sean libres para todos en términos iguales beneficiaría al
conjunto de la sociedad. Nosotros reivindicamos que los abusos generales
y sistemáticos del copyright actual son activamente contraproducentes a
esos propósitos al limitar tanto la creación de, y el acceso a, nuestra
cultura.

Los monopolios privados son uno de nuestros peores enemigos. Ellos
llevan directamente al aumento de los precios y a un enorme coste
escondido a los ciudadanos. Las patentes son monopolios de ideas
oficialmente concedidos. Enormes corporaciones se pelean unas con otras
en una carrera para archivar más y más patentes que poder usar contra
competidores más pequeños para prevenirles de competir en igualdad de
condiciones. La meta de cualquier monopolista no es ajustar los precios
y términos en que el mercado vaya a adoptar, sino más bien usar sus
derechos obtenidos deshonestamente como una palanca para subir los
precios e imponer términos a su favor en cuanto a uso y autorización.
Queremos limitar las oportunidades de crear daños y situaciones de
monopolio innecesarias.

Las marcas registradas son ante todo útiles como medios de protección
del consumidor. Nosotros creemos que las marcas registradas en general
funcionan bien actualmente, y no sugerimos ningún cambio en este punto.

Democracia, Derechos de los Ciudadanos y Libertades Personales

El derecho de los ciudadanos a la privacidad está escrito en la
constitución sueca. De este derecho fundamental aparecen numerosos otros
derechos humanos básicos como los derechos a la libertad de expresión, a
la libertad de opinión, a obtener información así como el derecho a la
cultura y el desarrollo personal. Todos los intentos del estado de
reducir esos derechos deben ser cuestionados y afrontados con fuerte
oposición.

Todos los poderes, sistemas y métodos que el estado puede usar contra
sus ciudadanos deben estar bajo evaluación constante y escrutinio por
los oficiales electos. Cuando el gobierno vigila a ciudadanos normales
que no son sospechosos de ningún crimen, esto es fundamentalmente
inaceptable y una violación clara del derecho de los ciudadanos a la
privacidad. Se debe garantizar a todo ciudadano el derecho al anonimato,
que es inherente a nuestra constitución, y el derecho del individuo a
controlar todo el uso de sus datos personales debe ser fortalecido.

El Partido Pirata toma un lugar en contra de la legislación especial
para los crímenes terroristas. Las penalidades y códigos criminales que
existen para las acciones que dañan o amenazan a los ciudadanos suecos o
a sus propiedades, son suficientes. Las leyes anti-terroristas de hoy en
día anulan el debido proceso legal, y existe el riesgo de que sean
usadas como herramienta represiva en contra de inmigrantes y disidentes.

El gobierno debe respetar la constitución no sólo en palabras, sino en
la práctica. El respeto por los ciudadanos y su privacidad significa,
que los principios como la prohibición de la tortura, integridad de la
aplicación de la ley, el debido proceso legal, la inmunidad del
mensajero1 y el secreto postal NO son negociables. El Partido Pirata
debe y actuará exponiendo y derrocando la administración que el partido
considere que no respete los derechos humanos, tal como se espera en la
democracia occidental.

El acta de secretos postales debe ser elevada a un acta general de
comunicaciones. De la misma forma que está prohibido leer el correo de
otro, debe estar prohibido leer o acceder al email, SMS u otras formas
de mensajes, sea cual sea la tecnología subyacente, o el operador que
sea. Cualquier y toda excepción a esta norma debe estar bien motivada en
todos y cada uno de los casos. Los empleadores deberían sólo poder
acceder a los mensajes de sus empleados sólo si es absolutamente
necesario para asegurar la funcionalidad técnica o si está en conexión
directa con las tareas relacionadas con el trabajo del empleado. El
gobierno sólo debería poder acceder a los medios de comunicación de los
ciudadanos o poner bajo vigilancia a un ciudadano en caso de una firme
sospecha de que un crimen esté siendo realizado por tal ciudadano. En
todos los demás casos, el gobierno debería asumir que sus ciudadanos son
inocentes y dejarlos tranquilos. A este acta de secretos de comunicación
se le debe de dotar de una sólida protección legal, ya que el gobierno
ha mostrado repetidamente que no se le puede confiar información
sensible.

Queremos revocar la Directiva de Retención de Datos y fortalecer el
derecho de los ciudadanos a la privacidad.

El Partido Pirata no tiene opinión de si Suecia debe o no debe ser un
miembro de la Unión Europea, pero ahora que lo somos, tenemos derecho a
demandar que la unión sea gobernada por principios democráticos. El
déficit de democracia en la Unión, debe ser redirigido a largo plazo, y
el primer paso es prevenir que sea tallado en piedra a través de una
mala constitución. La constitución de la Unión Europea por la que
Francia y Holanda votaron en contra, no debe ser aceptada, ni en su
forma original, ni con "cambios cosméticos".

La toma de decisiones y la administración gubernamental tanto en Suecia
como en la Unión Europea, debe caracterizarse por la transparencia y
apertura. Los representantes de Suecia en la Unión Europea deben actuar,
para acercar la Unión al principio sueco del acceso público a
documentos2.

Los basamentos de la democracia deben ser protegidos, tanto en Suecia
como en la Unión Europea.

Liberar Nuestra Cultura

Cuando el copyright fue creado originariamente, sólo regularon el
derecho de un autor de ser reconocido como el creador. Ha sido después
cuando se ha expandido el copyright para cubrir el copiado comercial de
trabajos así como limitar el derecho natural de ciudadanos privados y
organizaciones sin ánimo de lucro. Nosotros decimos que esta tendencia
al desequilibrio en el copyright se ha acentuado hasta un nivel
inaceptable para toda la sociedad. Los progresos económicos y
tecnológicos han colocado a las leyes de copyright en una posición
totalmente desequilibrada y en cambio infieren ventajas injustas para
unos pocos pero grandes jugadores en el mercado a expensas de los
consumidores, creadores y a la sociedad en general. Millones de
canciones clásicas, películas, canciones y libros son mantenidos
secuestrados en las bóvedas de enormes corporaciones mediáticas, no
queriendo que sean re-publicadas por sus grupos centrales pero
potencialmente demasiado provechosos como para liberarlos. Nosotros
queremos liberar nuestra herencia cultural y hacerla accesible a todos,
antes de que el tiempo marchite al celuloide de los carretes de las
películas antiguas.

Las leyes inmateriales son una forma de legislar propiedades materiales
con valores inmateriales. Las ideas, el conocimiento, y la información
son por naturaleza no exclusivos y su valor común cae dentro de su
capacidad inherente para ser compartidas y difundidas.

Nosotros declaramos que el copyright debe ser devuelto a sus orígenes.
Las leyes se deben alterar para regular sólo un uso comercial y el
copiado de trabajos protegidos. Compartir copias o de otro modo difundir
o usar trabajos para usos sin ánimo de lucro, no debe ser nunca ilegal
puesto que tal uso justo beneficia a toda la sociedad.

Queremos reformar el copyright comercial. La noción básica del copyright
fue siempre encontrar un equilibrio justo entre los intereses
comerciales contradictorios. Actualmente ese equilibrio se ha perdido y
necesita ser readquirido.

Sugerimos una reducción de la protección del copyright comercial, por
ejemplo, limitar el monopolio de crear copias de un trabajo con fines
comerciales, a cinco años máxime improrrogable desde la publicación del
trabajo. El derecho a realizar trabajos derivados debería ser ajustado
de forma que la regla básica sea la libertad de todos para poder
realizarlos inmediatamente. Todas y cada una de las excepciones de esta
regla, como por ejemplo la traducción de los libros, o el uso de
partituras musicales protegidas en películas, deberían estar enumeradas
explícitamente en los estatutos.

Queremos crear un copyright justo y equilibrado.

Toda reunión, uso, procesado y distribución no comercial de cultura
debería ser explícitamente fomentado. Las tecnologías que limitan el
derecho legal de los consumidores a copiar y usar información o cultura,
el llamado DRM (Gestión Digital de Derechos), debería ser prohibido. En
los casos en que esto lleve a desventajas obvias para el consumidor,
todo producto conteniendo DRM debe mostrar advertencias claras para
informar a los consumidores de este hecho.

Los acuerdos contractuales implementados para prevenir tales
distribuciones legales de información deberán ser declaradas nulas e
inválidas. La distribución no comercial de cultura, información o
conocimiento ya publicados -con la clara excepción de los datos
personales- no debe ser limitada o castigada. Como una conclusión lógica
de esto, queremos abolir el impuesto o canon a los medios vírgenes.

Queremos una cultura que sea para todos.

Las Patentes y los Monopolios Privados Perjudican a la Sociedad

Las patentes conllevan muchos efectos perjudiciales. Las patentes
farmacéuticas son responsables de muertes humanas por enfermedades cuya
medicación se podrían haber permitido, llevan a la obstruyen las
investigaciones prioritarias, y el coste de las medicinas se hace
innecesariamente alto, y en crecimiento, también en sectores más ricos
del mundo.

Las patentes sobre la vida y genes, como cultivos patentados, llevan a
consecuencias irracionales y perjudiciales. Las patentes de software
retardan el desarrollo tecnológico y constituyen una grave amenaza para
Suecia así como para muchas PYMES del sector tecnológico.

Se ha dicho que las patentes fomentan la innovación protegiendo a los
inventores e inversores sus nuevas invenciones y métodos de
manufacturación. En la realidad, las patentes son usadas cada vez más
por las grandes corporaciones para dificultar la competición con las
pequeñas empresas en igualdad de condiciones. En vez de fomentar e
inducir a la innovación, las patentes están siendo usadas como "campos
de minas" haciendo la guerra contra los demás, y a menudo el propio
propietario no tiene planes de desarrollar más la patente que posee para
su provecho.

Nosotros creemos que las patentes se han vuelto obsoletas y que reprimen
activamente la innovación y la creación de nuevo conocimiento. Además,
sólo mirando a todas las áreas de negocio que no son patentables se ve
claro que las patentes simplemente no son necesarias -- las fuerzas de
mercado de ser el primero en lanzar al mercado el producto es más que
suficiente para fomentar la innovación. Los inventores deberían competir
justamente con ventajas naturales como los diseños innovadores, los
beneficios que reportan al cliente, la mejora de los precios y de la
calidad, en vez de con un monopolio del conocimiento garantizado por el
estado. Al no tener que pagar a pequeñas armadas de abogados de
patentes, se liberarán recursos que pueden ser usados para crear
verdadera innovación y se mejorarán los productos a un ritmo más rápido,
beneficiándonos a todos al final.

Queremos abolir las patentes.

Además de abusar de las patentes, las grandes corporaciones intentan
crear monopolios por otros medios. Guardando en secreto información
sobre cosas como el formato de los ficheros, intentan crear un cerrojo
para el vendedor en sus productos, de ese modo limitando la competición
con una descarada indiferencia a los valores del libre y justo mercado.
Esta práctica lleva directamente a precios mayores y a un ratio menor de
innovación. Siempre que el sector fundado públicamente produzca sistemas
de información o información en sí misma, debe activamente contrarrestar
la formación o continuación de esos monopolios privados de información,
conocimiento, ideas, o conceptos. Iniciativas como el Acceso Abierto,
con el propósito de hacer los resultados de investigación, libremente
disponibles, deben ser promovidas y apoyadas.

Los monopolios privados deben ser combatidos.

El sector público debe archivar documentos y hacerlos disponibles al
público en formatos abiertos. Debe ser posible la comunicación con el
gobierno sin estar atado a un cierto proveedor o software privado. El
uso del código abierto en el sector público, incluyendo a las escuelas,
deber ser estimulado.

El uso de formatos abiertos y código abierto debe ser promovido.

Palabras finales

Deseamos defender los derechos de los ciudadanos, su derecho a la
privacidad y los derechos humanos básicos. Cuando el gobierno pone
rutinariamente bajo vigilancia, lleva invariablemente al abuso del
poder, a la falta de libertad y a la injusticia. Nosotros demandamos una
corrección esas injusticias. Demandamos justicia, libertad y democracia
para todos los ciudadanos.

Las leyes actuales de copyright y patentes llevan a monopolios
perjudiciales, a la pérdida de valores democráticos importantes,
entorpece la creación de cultura y conocimiento, y previene que éstos
lleguen a los ciudadanos. Exigimos la abolición de las patentes así como
leyes equilibradas de copyright, arraigadas en la voluntad de la gente,
para enriquecer la vida de las personas, permitir un clima económico
saludable, crear una base común de conocimiento y cultura, y de ese modo
beneficiar al desarrollo de la sociedad en su totalidad.

Con esto, nuestro trabajo se centra en medios parlamentarios y de ese
modo buscamos un mandato de la gente para representarlos en estas
cuestiones.

El Partido Pirata no se esfuerza por ser parte de una administración.
Nuestro objetivo es tener una posición de desempate en el parlamento
como influencia, y apoyar una administración que dirija los aspectos
referentes a nuestra plataforma de manera satisfactoria. Cuando lo haga,
apoyaremos esa administración en otros aspectos donde decidamos no
sostener opiniones propias.

Para unificarse como un movimiento sólido y poderoso, hemos elegido no
tomar parte en ninguna otra conexión que no esté conectada con los
principios aquí declarados.

Nos posicionamos unidos alrededor de nuestra protección del derecho a la
privacidad, nuestra voluntad de reformar el copyright y la necesidad de
abolir las patentes.

1 La constitución sueca incluye una fuerte referencia a la Convención de
los Derechos Humanos Europea, de donde es incluida en la constitución. /
Algunos países tienen un nombre para la "Inmunidad del Mensajero",
prefiriendo el término "Principio del Servicio de Envíos". Se refiere al
hecho de que un mensajero (Servicio de Envíos) nunca es legalmente
responsable por el contenido de un mensaje entregado. 2 El principio
sueco del acceso público a documentos -- "offentlighetsprincipen" --
significa que cualquier persona tiene el derecho a pedir un documento de
cualquier administración, sin identificarse. Mientras existan documentos
que puedan ser clasificados explícitamente y exonerados de este
principio, tal sello secreto, debe estar justificado por un criterio
estricto y rara vez aplicable, que puede ser apelado por el público.
Para ilustrar la fuerza de este principio, menores de edad pueden ver
video con material censurado por el Panel de Administración de Cine,
siempre que lo presenten en las oficinas del panel. Se recuerda que el
panel tiene prohibido pedir identificación o una prueba de edad similar.
