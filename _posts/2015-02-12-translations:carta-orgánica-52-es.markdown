---
title: Translations:Carta Orgánica/52/es
layout: post
categories: []
---

Cualquier grupo que contenga al menos tres (3) Piratas Afiliados podrá
pedir su reconocimiento como Barco Pirata mediante Propuesta a la
Asamblea Permanente.
