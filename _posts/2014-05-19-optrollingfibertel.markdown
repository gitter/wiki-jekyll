---
title: OpTrollingFibertel
layout: post
categories: []
---

¿En qué consiste la operación?
------------------------------

Está comprobado que se puede llamar al servicio técnico y pedir que
enumeren todos los días en que hubo problemas en la zona. Luego se puede
reclamar el descuento que corresponde a todos esos días, aunque no se
haya hecho el reclamo en el día.

La estrategia para conseguir este descuento general (a veces hasta el
valor de una factura) es la siguiente:

1\. Llamar al servicio técnico y hablar con una persona.

2\. Decir que el servicio anda mal y pedir que enumeren los problemas en
la zona del último mes, día por día.

3\. Reclamar el descuento de esos días, que se hace efectivo
automáticamente en la próxima factura.

En la cuenta de twitter [OpFibertel](https://twitter.com/OpFibertel)
vamos a recibir denuncias y a difundir cuándo el servicio se encuentra
cortado en una zona. Al final de cada mes y en base a las denuncias
vamos a hacer un resumen de las zonas que estuvieron afectadas durante
este período para que todos los que residan en esa zona puedan llamar y
pedir el reintegro del dinero que corresponde.
