---
title: Translations:Carta Orgánica/105/es
layout: post
categories: []
---

-   Representar al actor, velando por sus derechos, intereses y
    mandatos.
-   Notificar a su representado sobre los hechos que se le imputen, así
    como cualquier otra información respecto del mismo, en todo momento.
