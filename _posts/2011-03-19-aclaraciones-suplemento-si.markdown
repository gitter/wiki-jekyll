---
title: Aclaraciones suplemento si
layout: post
categories: []
---

El suplemento Si! del diario Clarín publicó [una reseña sobre el
PirateFest](http://www.si.clarin.com/piratas_0_446355363.html), donde el
periodista Daniel Convertini escribe:

` `*`"La`` ``idea`` ``caló`` ``hondo`` ``en`` ``un`` ``grupo`` ``de`` ``entusiastas`` ``argentinos:`` ``formaron`` ``el`` ``Partido`` ``Pirata`` ``e`` ``importaron`` ``punto`` ``por`` ``punto`` ``esta`` ``problemática`` ``primermundista."`*

De los muchos prejuicios y apreciaciones superficiales que contiene la
nota, conviene aclarar dos cuestiones importantes, en primer lugar la
problemática *primermundista* no fue *importada* por el Partido Pirata,
sino mas bien, por nuestros funcionarios, jueces y legisladores, que
*\"primermundisticamente\"* han optado muchas veces por seguir la agenda
de intereses transnacionales *punto por punto*, tanto de empresas
multinacioanles como de estados extranjeros, en vez de contextualizar
los casos locales según la problemática local. Desde esta **perpectiva
local**, \"tercermundista\" es que surge la necesidad de construir la
opción politica del Partido Pirata Argentino. En segundo lugar, debe
quedar claro que la problemáticas relacionadas con la llamada propiedad
intelectual obedecen a un esquema geopolítico donde los intereses de los
paises en desarrollo no son los mismos que los de los países centrales.

Algunos ejemplos de problematicas, cuya respuesta, desde la esfera
judicial o legislativa, no tiene en cuenta o menoscaba el contexto
local:

-   El 6 de febrero de 2008 [la revista Rolling Stone informaba sobre
    los primeros juicios por compartir música en
    Argentina](http://www.rollingstone.com.ar/985034). Siguiendo la
    estrategia de amedrentamiento contra los usuarios de internet,
    adoptada principalmente en Estados Unidos, las empresas locales
    iniciaron una ola de demandas contra los usuarios argentinos. No
    hace falta señalar, que ni en la nota, ni en la estrategia adoptada
    por las empresas es tenido en cuenta el precio prohibitivo que
    tienen los CD para gran parte de la poblacion, asi comol absoluta
    ausencia de una oferta altenativa en internet, adecuada a los
    bolsillos locales, provista por las empresas discograficas.

<!-- -->

-   El 10 de febrero del mismo año [en Página 12 se publica un proyecto
    de Ley de
    Música](http://www.pagina12.com.ar/diario/suplementos/espectaculos/2-9168-2008-02-10.html)
    del entonces diputado Esteban Morgado que contenía una cláusula que
    implantaba el llamado *\"Canon Digital\"*. El [Canon
    Digital](http://www.noalcanon.com.ar/) es un porcentaje del precio
    de CDs y DVDs vírgenes así como pendrives y cualquier otro
    dispositivo que sirva para almacenar archivos que pudieran tener
    derechos de autor, que recaudan las gestoras colectivas y
    distribuyen sin control público, principalemnte con el objeto de
    beneficiar a las empresas discograficas por las supuestas pérdidas
    provocadas por el intercambio de archivos en internet. Más alla de
    lo arbitrario de esta recaudacion, que no distingue el uso que
    recibirá el dispositivo de almacenamiento (por ejemplo guardar fotos
    familiares, documentos de trabajo, backups de información etc.) el
    modelo sigue el establecido en los \"primermundistas\" países
    europeos (mecanismo [no libre de
    controversia](http://www.elmundo.es/elmundo/2011/01/14/navegante/1295035251.html)),
    es decir, otro intento de \"importacion\" de regulaciones
    tecnológicas por parte de nuestros legisladores.

<!-- -->

-   En mayo de 2008 [la senadora por Formosa Adriana Bortolozzi de
    Bogado presentó otro proyecto de canon
    digital](http://fabio.com.ar/verpost.php?id_noticia=3419#comments).
    Entre sus
    [considerandos](http://www.senado.gov.ar/web/proyectos/ver_adjunto_pdf.php?clave=F743003409/S1298.pdf)
    admite sin disimulo, nada menos que declaraciones del Embajador de
    EE.UU. y datos aportados por la Secretaría de Comercio Exterior
    norteamericana.

<!-- -->

-   En febrero del año 2009 es [el Senador Filmus quien intenta reflotar
    la idea del Canon
    Digital](http://partido-pirata.blogspot.com/2009/02/el-canon-siempre-en-verano.html).

<!-- -->

-   El 14 de mayo de 2009 nos enteramos que la [Universidad de Buenos
    Aires y otras universidades públicas pagarán un canon por el uso de
    fotocopiadoras](http://www.lanacion.com.ar/nota.asp?nota_id=1127847).
    En el caso de la Universidad de Buenos Aires se establece que se
    paguen \$12,72 por alumno lo que daría la suma de \$3.816.000 para
    poder realizar fotocopias (parciales) de los libros editados por los
    asociados a CADRA (una entidad que gestiona derechos reprográficos
    en la Argentina). Los \"costos\" de la gestión de CADRA solamente,
    demandan más de la mitad de lo recaudado. La universidad tampoco
    queda a salvo de las demandas, ya que CADRA no tiene asociada a la
    totalidad del rubro editor. El acuerdo tampoco toma en cuenta que
    parte del material es producido por la misma Universidad, es decir
    con fondos públicos, por lo cual la Universidad termina tranfiriendo
    recursos a entidades privadas por libros que la misma Universidad
    financió su autoria y de paso promociona al incluirlo en la
    bibliografía de una cátedra. Nuevamente, se adoptan politicas
    relacionadas con la llamada propiedad intelectual siguiendo
    lineamientos \"primermundistas\", sin tener en cuenta las
    problemáticas locales: el presupuesto educativo limitado, la
    dificultad de acceder al material de estudio etc.

<!-- -->

-   En [setiembre de 2009 el profesor Horacio Potel es procesado
    penalmente](http://partido-pirata.blogspot.com/2009/09/cuando-la-filosofia-no-es-para-todos-el.html).
    ¿Su crimen?, mantener una biblioteca digital gratuita y sin ningun
    fin de lucro, con textos de Jacques Derrida y Heidegger. La fiscalía
    actúa diligentemente frente al pedido realizado por la editorial
    Minuit de Francia. Pedido que llega a través del agregado cultural
    de la Embajada de Francia, y canalizado por la Cámara Argentina del
    Libro por medio de una denuncia a la justicia. El profesor Potel es
    sometido a un proceso criminal que incluye allanamientos y embargos.
    Muchos de los textos de las bibliotecas digitales de Potel son obras
    inhallables, o a costos prohibitivos o directamente descatalogados,
    a los cuales muchos lectores, especialmente estudiantes de toda
    america latina, jamás hubieran podido tener acceso. La problemática
    local del acceso a los libros, cuyos costos son prohibitivos para el
    estudiante promedio es ignorado.

<!-- -->

-   En Setiembre de 2009, un sitio en donde se digitalizaban textos y
    apuntes que se usaban en las carreras de la Facultad de Filosofía y
    Letras de la Universidad de Buenos Aires, [Bibliofyl, es cerrado por
    presiones de la
    CADRA](http://partido-pirata.blogspot.com/2009/09/bibliofyl-una-nueva-biblioteca.html).
    En este caso se los acusaba de compartir enlaces para descargar
    libros y apuntes. La problemática local del acceso a los libros,
    nuevamente ignorada.

<!-- -->

-   En octubre de 2009 CAPIF, la cámara de empresas de la industria
    discográfica argentina, [pidió apoyo a la Ministra Débora Giorgi
    para impulsar una ley HADOPI, o ley de los tres avisos,
    criolla](http://partido-pirata.blogspot.com/2009/10/una-brigada-hadopi-criolla.html).
    La ley HADOPI es una ley fuertemente represiva aplicada en Francia,
    para evitar que los usuarios de internet compartan archivos.
    Nuevamente soluciones para la industria, importadas de contextos
    económicos \"primermundistas\".

<!-- -->

-   En noviembre de 2009 aprobaron la extensión en 20 años (de 50 a 70
    años) del monopolio privado sobre los fonogramas. El gran
    beneficiario inmediato de esta ley (la version local de la [\"Mickey
    Mouse Protection
    Act\"](http://en.wikipedia.org/wiki/Copyright_Term_Extension_Act),
    que podría denominarse localmente \"Mercedes Sosa Act\") fue la
    empresa Sony BMG Music Entertainment, que relanzó los primeros
    discos de Mercedes Sosa, aprovechando el impacto de su
    fallecimiento. Estas grabaciones hubieran entrado en dominio público
    de no aprobarse la ley. El dominio público, al igual que el espacio
    público, la educación pública, o la salud pública cumple un rol
    escencial en nuestra sociedad: instituir ámbitos destinados
    específicamente a proteger el bien común. Sin tener en cuenta ningun
    contexto local de acceso a la cultura (los CD\'s tienen precios
    prohibitivos para la mayor parte de la población) los diputados
    aprobaron la ley. Entre [los
    fundamentos](http://www.senado.gov.ar/web/proyectos/verExpe.php?origen=S&tipo=PL&numexp=3030/09&nro_comision=&tConsulta=3)
    se menciona la necesidad de que el país adhiera al \"movimiento
    universal de ampliación de plazos de protección\".

<!-- -->

-   En abril de 2010 se anuncia el plan \"Conectar con Igualdad\". El
    gobierno finalmente se pliega al modelo privativo de vigilancia,
    dependencia y monopolio de Microsoft, y no al modelo público y
    abierto del software libre, que promueve la independencia
    tecnológica y se basa en el paradigma de libre acceso al
    conocimiento. A diferencia de \[Venezuela
    <http://www.youtube.com/watch?v=OezGrpb2yNU>\], \[Ecuador
    <http://www.youtube.com/watch?v=Hy5yAk4dYOk>\] o \[Brasil
    <http://www.youtube.com/watch?v=ZENh06QsB1c>\], el gobierno decir
    seguir una politica de dependencia tecnológica.

<!-- -->

-   Con el título \"Media Piracy in Emerging Economies\" (Piratería de
    Archivos en Mercados Emergentes), un riguroso estudio analizó
    comparativamente la piratería en países emergentes considerando no
    sólo las políticas de represión y los precios de los productos, sino
    también el poder de compra de la población. La investigación apunta
    que en el caso brasileño, una compra de un producto \"pirata\",
    representa para el bolsillo del brasileño medio, casi lo mismo que
    los habitantes de los Estados Unidos sienten cuando adquieren el
    producto original. Batman salía por casi de US\$ 3,50 en el mercado
    ilegal de Brasil en 2008 (año en que se hizo el estudios), sólo que
    ese valor, para la renta de un habitante de Brasil, corresponde a lo
    que seria un gasto de US\$ 20 para un estadounidense.

Una de las razones de la existencia del Partido Pirata Argentino, es
generar una respuesta politica a la aplicación de leyes, medidas
judiciales, o politicas públicas, que ponga el foco precisamente en las
**problemáticas locales** y no en los dictados de los *lobbies* que
llegan a través de embajadas o ejecutivos de empresas multinacionales.
En todos los casos citados más arriba, lo que puede notarse es un
seguimiento ciego de políticas o estrategas \"importadas\" de contextos
económicos y sociales muy diferentes al nuestro. Por eso la alusión que
realiza el periodista Daniel Convertini de Clarin, suponiendo una
supuesta importación de \"una problemática primermundista\" que no
existiría o fuera prioritaria en nuestro país, **nos resulta claramente
desinformativa, induciendo perversamente una idea equivocada en los
lectores: la problemática es fundamentalmente local**, y en cambio las
respuestas del estado, los legisladores, o los jueces son \"importadas
punto por punto\".

El otro aspecto que conviene ser mencionado, es que las regulaciones,
políticas y leyes relacionadas con la propiedad intelectual, responden a
un ordenamiento geopolitico claramente determinado. No se limitan al
problema del \"intercambio de archivos\" y los intereses de la industria
del entretenimiento, sino que involucran cuestiones como patentes sobre
alimentos, genes o medicamentos, y que desde una perspectiva norte/sur
condenan a los paises del Sur a la agricultura y a la industria,
mientras que los del Norte conservan el control sobre la creatividad y
el valor agregado. Como meciona este artículo de [Florent
Latrive](http://www.rebelion.org/noticia.php?id=102627): *\"accesorios
de moda diseñados en París y producidos en Túnez; computadoras
concebidas en Silicon Valley y fabricadas en Asia. Y todo ello con
estrictos controles fronterizos y en Internet\"*.

Desde este lugar \"tercermundista\" es que en Partido Pirata Argentino
pensamos leyes y políticas públicas relacionadas con la llamada
Propiedad Intelectual, con la privacidad, o con la libertad de
expresión, y una parte importante de las charlas que tuvieron lugar en
PirateFest tenian como objeto reflexionar, desde diferentres abordajes,
con una mirada **desde el sur**.
