---
title: Translations:Carta Orgánica/38/es
layout: post
categories: []
---

Para ello, toda propuesta deberá informarse debida y detalladamente a la
Asamblea Permanente y deberá contar con el Consenso de éste.
