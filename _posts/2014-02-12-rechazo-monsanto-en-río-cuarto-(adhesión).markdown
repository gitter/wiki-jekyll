---
title: Rechazo Monsanto en Río Cuarto (Adhesión)
layout: post
categories:
- Adhesiones
- Diciembre 2013
- GMO
---

[Categoría:Adhesiones](Categoría:Adhesiones "wikilink")
[Categoría:Diciembre 2013](Categoría:Diciembre_2013 "wikilink")
[Categoría:GMO](Categoría:GMO "wikilink")

Enviada por [Pirata:Fauno](Pirata:Fauno "wikilink") el 2013-12-02.
[Archivo](http://asambleas.partidopirata.com.ar/archivos/general/2013-December/006099.html)

Documento en rechazo a la instalación de Monsanto en Río Cuarto
---------------------------------------------------------------

La pretensión de Monsanto de poner en funcionamiento una planta
experimental de semillas en Río Cuarto representaba un paso más en la
consolidación del modelo de los agronegocios en nuestra región y en el
país. Las implicancias que los cultivos transgénicos y la aplicación de
agrotóxicos tienen a nivel social, político, económico, ambiental y
sanitario se han vuelto cada vez más evidentes: ya no quedan dudas de
que la agricultura promovida por las grandes corporaciones sólo trae
perjuicios para el pueblo y beneficios para unos pocos. El modelo de
agro-negocios forma parte de una nueva dinámica extractiva requiere de
subordinaciones y explotaciones, cuando no de la directa destrucción de
los tejidos sociales.

Las comunidades afectadas llevamos años sufriendo las consecuencias de
este modelo: hay severos indicios de que los casos de cáncer,
malformaciones, abortos espontáneos y los daños genéticos están
vinculados a la aplicación de millones de litros de agrotóxicos se suman
los efectos del extractivismo a nivel socio económico. El despoblamiento
del campo, la concentración de la tierra, la destrucción de los bienes
comunes ha acentuado las asimetrías entre los que más tienen y los que
menos tienen. A la vez, la contaminación, la pérdida de biodiversidad y
los crecientes desmontes que acarrean los agronegocios imposibilita el
desarrollo y el buen vivir de las generaciones futuras.

La transnacional Monsanto encabeza este modelo de acumulación por
desposesión. A la vez, es responsable de la producción y aplicación
sistemática de algunos de los elementos más contaminantes y letales, que
actualmente se utilizan como parte de la industria agroalimentaria.

A los mismos fines busca apropiarse de la producción de alimentos,
monopolizando la actividad agroalimentaria a través del registro de
propiedad intelectual sobre las variedades de semillas, usurpando y
concentrando el patrimonio vegetal desarrollado por las comunidades
campesinas y pueblos originarios del mundo.

Todo esto se buscaba ocultar detrás de la planta que Monsanto intentó
imponer a la ciudadanía de Río Cuarto, además de los impactos directos
que su actividad hubiera generado en lxs vecinxs de la ciudad y la
región.

Desde la llegada de Monsanto a la ciudad presenciamos irregularidades en
la construcción de la planta, en el avance de obra y el traspaso de
firmas. Durante todo el proceso de instalación la empresa pretendió
desestimar la licencia social e ignorar y violar las leyes nacionales,
provinciales y locales.

Por todo lo expuesto ratificamos el NO a Monsanto en Río Cuarto y
celebramos la decisión inapelable del intendente Juan Jure de rechazar
la instalación de la planta experimental, cumpliendo así con la ley y
acatando la voluntad popular.

**Asamblea Río Cuarto Sin Agrotóxicos en la Coordinadora Provincial para
la Soberanía Popular por el Agua y la Tierra**
