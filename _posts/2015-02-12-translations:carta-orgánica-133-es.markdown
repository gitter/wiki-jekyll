---
title: Translations:Carta Orgánica/133/es
layout: post
categories: []
---

El Órgano Fiduciario dispondrá cada mes, para la Asamblea, un monto de
dinero a determinar al inicio del año fiscal, denominado 'caja chica',
sobre el cual no ejercerá mayor control, sino que se dejará en manos de
la Asamblea el fin último de ese monto. Entre meses, se repondrá el
dinero que restase, de lo que hubiese quedado del mes anterior, para
alcanzar el valor determinado.
