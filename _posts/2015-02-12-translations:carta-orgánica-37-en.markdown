---
title: Translations:Carta Orgánica/37/en
layout: post
categories: []
---

In the spirit of maintaining the horizontalism and participatory and
autogestive dynamism during regular Party\'s activities, every Pirate
has the right to proppose and carry out activities with the goals of
supporting, boosting, spreading and extending the Pirate Principles in
any space.
