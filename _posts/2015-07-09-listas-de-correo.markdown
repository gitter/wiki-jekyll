---
title: Listas de correo
layout: post
categories: []
---

Una lista de correos es un grupo donde al enviar un email le llega a
todos los participantes. Tenemos varias listas de correos para
comunicarnos y te podes subscribir a las que quieras. [Ver
todas](http://asambleas.partidopirata.com.ar/listinfo)

### ¿Como usar una lista de correos?

Una vez que te subscribas a la lista vas a recibir mails de los demas,
el asunto del mail suele decir de que se trata. Si queres opinar
responde al mail sin cambiar el asunto y citando el mail que respondes.

Al principio puede ser agobiante la cantidad de mails que se reciben y
de los temas que se tratan, pero una vez que le agarras la mano es
fácil. No desespereis! Lo único que necesitas es ganas de contribuir y
opinar!

Empezando a participar, compartir, debatir ya estas colaborando! Podes
proponer ideas que estés dispuesta a llevar a cabo, y cuando otras
proponen podes adherir con un +1 y participando en la concreción.

Nos manejamos bajo Consenso general, mientras nadie se oponga se hace! Y
si propones una idea sos la que estaría encargada de llevarla a cabo, no
te asustes que no vas a estar solo, siempre hay alguien que se suma ø/

Cualquier duda mandas un mail a la lista y vas a tener un montón de
piratas que van a estar dispuestos a explicarte todo! o/

**`Si`` ``te`` ``llegan`` ``muchos`` ``mails`` ``acá`` ``tenés`` ``una`` ``guía`` ``sobre`` `[`Como`` ``organizar`` ``los`` ``mails`](Como_organizar_los_mails "wikilink")**

### Lista general

**Mucha actividad** La [lista
general](http://asambleas.partidopirata.com.ar/listinfo/general)
funciona de alguna manera como una asamblea permanente y es donde vamos
viendo que necesitamos y que queremos.

**Si te llegan muchos mails aquí tenes una guía sobre [Como organizar
los mails](Como_organizar_los_mails "wikilink")**

[Subscribirme a la lista
General](http://asambleas.partidopirata.com.ar/listinfo/general)

### Lista Difusion

**Poca actividad** esta es una lista solo de difusion, si queres
enterarte de las asambleas y eventos metete. [Subscribirme a la lista de
Difusión](http://asambleas.partidopirata.com.ar/listinfo/difusion)

### Lista Iberoamericana

**Poca actividad**, para armar una red de comunicación entre piratas de
diferentes paises en español/portugues. [Subscribirme a la lista
Iberoamericana](http://asambleas.partidopirata.com.ar/listinfo/iberoamerica)

[Netiqueta](Netiqueta "wikilink")
---------------------------------

Sugerencias sobre como responder emails para que sean claros y como
manejarse en listas de correos.
