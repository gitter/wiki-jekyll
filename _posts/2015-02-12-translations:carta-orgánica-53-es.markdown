---
title: Translations:Carta Orgánica/53/es
layout: post
categories: []
---

Los Barcos Piratas se autodeterminan, pudiendo realizar actividades y
sacar comunicados en su propio nombre.
