---
title: Translations:Carta Orgánica/75/es
layout: post
categories: []
---

Cada Asamblea General deberá contar con al menos un moderador a elegirse
al azar de entre todos los presentes al comienzo.
