---
title: Translations:Carta Orgánica/25/es
layout: post
categories: []
---

Se considera que el derecho a informar y ser informado libremente no
sólo es un derecho de raigambre constitucional, sino un derecho humano
fundamental según pactos internacionales, un derecho humano "difuso", y
cualquier ataque a quien lo ejerza, por causar precedente, tiene una
incidencia colectiva que obliga a todos los piratas a intervenir en su
defensa activa y pasiva.
