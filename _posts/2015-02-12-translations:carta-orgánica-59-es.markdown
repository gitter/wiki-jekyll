---
title: Translations:Carta Orgánica/59/es
layout: post
categories: []
---

Este mandato concluirá una vez cumplido el objetivo para el que fueron
creados, por renuncia de los responsables o revocación asamblearia.
