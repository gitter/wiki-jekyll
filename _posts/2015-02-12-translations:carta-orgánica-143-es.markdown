---
title: Translations:Carta Orgánica/143/es
layout: post
categories: []
---

La Carta Orgánica podrá ser cambiada mediante Acuerdo por la Asamblea
General. Esta Carta Orgánica comenzará a regir a partir del día
posterior a su aprobación por la Justicia Electoral.
