---
title: Como configurar el mail pirata
layout: post
categories:
- Guías
---

Datos de configuración
----------------------

### Entrante

`imap.partidopirata.com.ar`\
`993`\
`SSL/TLS`\
`normal password`

### Saliente

`smtp.partidopirata.com.ar`\
`587`\
`STARTLS`\
`normal password`

Thunderbird
-----------

1.  Pone tu nick, dirección de correo y contraseña
2.  Clickea configuración manual
3.  Ingresa todos los datos de configuracion (no dejes nada vacio ni en
    \"autodetectar\")

No le des al boton de testear la configuración por que al no poder
testearla te deshabilita el boton de guardar. Si lo haces por error
cambia alguno de los campos y volvelo a cambiar por la opcion correcta,
el boton deberia habilitarse.

[Categoría:Guías](Categoría:Guías "wikilink")
