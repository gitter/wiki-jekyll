---
title: Como crear un partido político
layout: post
categories: []
---

Nacional
--------

En una primera etapa, al partido político en formación se le otorga una
"personería provisoria". Esta figura consolida derechos vinculados con
el nombre y símbolos de un partido político, más no lo habilita a
participar electoralmente ni a obtener financiamiento público. Para
esto, debe presentar:

-   Acta de fundación y constitución.

<!-- -->

-   La "adhesión" de un número de electores no inferior al cuatro por
    mil

(4‰) del total de los inscriptos en el padrón del distrito
correspondiente, hasta el máximo de 4000 adherentes.

-   Nombre, declaración de principios y programa o bases de acción

política, carta orgánica, acta de designación de las autoridades
promotoras, domicilio partidario y acta de designación de los
apoderados.

Fuente: <http://www.elecciones.gob.ar/preguntas/preg_partidos.htm#2>

Municipal
---------

El reconocimiento de una agrupación municipal para actuar con ese
carácter deberá ser solicitado ante el Organo de Aplicación cumpliendo
los requisitos que seguidamente se indican:

-   Acta de fundación y constitución conteniendo lo siguiente:
    -   Nombre y domicilio de la agrupación.
    -   Declaración de principios y bases de acción política.
    -   Carta orgánica.
    -   Designación de autoridades promotoras y apoderados.
-   La adhesión inicial de la cuarta parte de los electores

inscriptos exigida para obtener el reconocimiento definitivo o de cien
(100) electores inscriptos si aquélla cifra resultara mayor.

Fuente: <http://www.juntaelectoral.gba.gov.ar/html/texto_ley_9889.htm>
