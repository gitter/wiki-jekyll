---
title: The day we fight back
layout: post
categories:
- Adhesiones
- Barco Callejero
- Privacidad
- Febrero 2014
---

[Categoría:Adhesiones](Categoría:Adhesiones "wikilink") [Categoría:Barco
Callejero](Categoría:Barco_Callejero "wikilink")
[Categoría:Privacidad](Categoría:Privacidad "wikilink")
[Categoría:Febrero 2014](Categoría:Febrero_2014 "wikilink")

![](Piratas-obelisco9.jpg "Piratas-obelisco9.jpg")

El martes 11 de febrero nos vamos a estar sumando a [El Día Que Nos
Defendemos](http://thedaywefightback.org)\], lanzada un mes antes en el
aniversario del Apagón SOPA y la muerte de Aaron Swartz.

Es un día de acción global contra la vigilancia y el espionaje, acá con
Proyecto X, SIBIOS, la SUBE, los infiltrados de la federal, las cámaras
y los 1300 millones extra que le dieron a inteligencia del ejercito
tenemos bastante que difundir!

Proyecto X inicio en el 2002, se descubrió en el 2012 y hoy sigue
haciendo inteligencia en agrupaciones. La federal sigue teniendo 1000
agentes de inteligencia uno de los cuales infiltro la Agencia Walsh y
varias organizaciones por 10 años!! De Inteligencia del Ejercito se
sospecha que espía a políticos opositores y su director general ahora es
general del ejercito!!

Los invitamos a sumarse a la volanteada y al pequeño taller de seguridad
que se hará allí.
