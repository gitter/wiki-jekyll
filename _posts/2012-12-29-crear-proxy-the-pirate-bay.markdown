---
title: Crear proxy The Pirate Bay
layout: post
categories: []
---

En esta guía vamos a explicar cómo hacer tu propio proxy a ThePirateBay.
Un proxy va a ayudar a los usuarios de los países en donde The Pirate
Bay ha sido bloqueado. Muchos ISPs de distintos países como el Reino
Unido y Holanda han sido forzados a bloquear The Pirate Bay.

Para la mayor parte del artículo vamos a usar un servidor web llamado
Nginx. Fue probado en distribuciones basadas en Red Hat y en Debian pero
funciona perfectamente en casi todos los sistemas basados en UNIX. Tu
servidos no debería tener alguna otra cosa corriendo en el puerto 80 y
obviamente necesitamos ancho de banda suficiente.

También existe un script en PHP que actúa como proxy a The Pirate Bay
diseñado específicamente para esto, y puede ser utilizado en planes de
hosting (no requiere un servidor dedicado para este). Si vas a hacer
esto, asegúrate de que no estás violando las condiciones de servicio de
tu proveedor.

La guía original se encuentra [aquí](http://proxybay.info/setup.html).

Script PHP
----------

Este script PHP es un proxy diseñado para proveer acceso a The Pirate
Bay. Es especialmente útil cuando no tenés un servidor dedicado al
proxy. Fue creado por ikwilthepiratebay.nl y requiere por lo menos PHP5
y cURL.

Para instalarlo, hay que extraerlo y subirlo a nuestro servidor web y
cambiar del nombre de htaccess.txt a .htaccess.

[PHP proxy](http://proxybay.info/assets/proxy/php_proxy.zip)

Wordpress Repress Plugin
------------------------

Si tenés un sitio Wordpress, podés instalar este plugin para permitir el
acceso a The Pirate Bay y otros sitios.

Cualquier sitio con Wordpress con este plugin va a empezar a funcionar
como proxy para los sitios censurados.

[Repress plugin for
Wordpress](http://wordpress.org/extend/plugins/repress/)

Windows
-------

Hemos preconfigurado nginx para Windows en un proxy para The Pirate Bay.
Solo hay que descargarlo, ejecutar start.bat, configurar tu router y ya
vas a tener un proxy completamente funcional.

[Descargar v1.0 basada en
nginx/1.2.0](http://proxybay.info/assets/proxy/nginx-proxy.zip)

Esto solo va a funcionar si tu ISP proxy no bloqueó The Pirate Bay. En
este caso, acá tenés una [lista de proxies para The Pirate
Bay](http://proxybay.info/)

Una vez que el programa se está ejecutando, vas a necesitar hacer un
forward al puerto 80 en tu router o el firewall a la IP de la
computadora con nginx.

Linux
-----

1\. Una vez que tengamos el servidor configurado y online, tenemos que
instalar nginx.

a\. Primero hay que instalar las dependencias, esto es para un sistema
basado en Red Hat.

` yum install pcre-devel zlib-devel openssl-devel gcc make subversion`

b\. Si estás usando un sistema basado en Debian, deberías usar esto.

` apt-get install libpcre3 libpcre3-dev zlib1g zlib1g-dev openssl gcc`\
` make subversion`

2\. Descargar la fuente. La última versión es la 1.2.0 del 04/05/2012.

` wget `[`http://nginx.org/download/nginx-1.2.0.tar.gz`](http://nginx.org/download/nginx-1.2.0.tar.gz)

3\. Descargar la fuente de substitutions4nginx usando subversion.

` svn checkout `[`http://substitutions4nginx.googlecode.com/svn/trunk/substitutions4nginx-read-only`](http://substitutions4nginx.googlecode.com/svn/trunk/substitutions4nginx-read-only)

4\. Extraer la fuente.

` tar xzvf nginx-1.2.0.tar.gzcd nginx-1.2.0`

5\. Prepararse para compilar configurando. No usar
\--with-http\_ssl\_module si no vas a usar SSL. Cambiar la ruta del
directorio de substitutions4ningx-read-only a donde lo hayas descargado.

`  ./configure --with-http_ssl_module --add-module=/path/to/substitutions4nginx-read-only`

6\. Compilalo. Será instalado en /usr/local/nignx por defecto.

` make`\
` make install`

7.Probar nginx iniciándolo y tipeando la IP de tu servidor en tu
navegador. Deberías ver un mensaje que dice \"Welcome to nginx!\".

` cd /usr/local/nginx/`\
` ./sbin/nginx`

8\. Si se ejecuta, lo vamos a parar para poder configurarlo. Si no te
podés conectar al servidor, asegurate de que no lo bloquée ningún
firewall. Para probarlo usá este
[script](http://bash.cyberciti.biz/security/shell-script-to-stop-linux-firewall).
Luego debés configurar iptables correctamente, pero eso es visto en este
[artículo](http://www.thegeekstuff.com/2011/06/iptables-rules-examples).

` ./sbin/nginx -s stop`

9\. Ahora debemos darle otro nombre al archivo de configuración original
para tener una copia ante cualquier eventualidad.

` cd conf`\
` mv nginx.conf nginx.conf-backup`

10\. Descarga esta configuración sin SSL y abrirla.

` wget `[`http://proxybay.info/assets/proxy/nginx.conf`](http://proxybay.info/assets/proxy/nginx.conf)\
` vi nginx.conf`

Si estás usando SSL, descargá este archivo.

` wget `[`http://proxybay.info/assets/proxy/nginx-ssl.conf`](http://proxybay.info/assets/proxy/nginx-ssl.conf)\
` mv nginx-ssl.conf nginx.conf`\
` vi nginx.conf`

11\. Si no estás usando SSL deberías considerar cambiar subs\_filters o
eliminarlos. Si usás SSL, tenés que cambiar la ruta de los certificados
SSL y editar server\_name.

12\. Crear el directorio del caché. Esto nos sirve para aliviar la carga
a los servidores de The Pirate Bay y acelerar nuestro sitio. Si
cambiaste la ruta del caché arriba, asegurate de que sea el mismo que
este.

` mkdir /usr/local/nginx/cache`

13\. Probar la configuración iniciando nginx. Si no tenemos ningún
mensaje, el servidor fue inicado. Si obtenemos errores, necesitaremos
cambiar algo para solucionarlos.

` ./usr/local/nginx/sbin/nginx`

14\. Si funciona, agregarlo a la lista de proxies
[aquí](http://proxybay.info/submit.html).
