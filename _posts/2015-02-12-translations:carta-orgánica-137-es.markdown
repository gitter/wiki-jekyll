---
title: Translations:Carta Orgánica/137/es
layout: post
categories: []
---

En caso de necesitar mas fondos de los disponibles el Órgano Fiduciario
podrá, por medio de una propuesta a las Asambleas, hacer un llamado a
donaciones o contraer deuda.
