---
title: Temario tentativo para la reunión del 30 de Septiembre
layout: post
categories: []
---

Temario tentativo
-----------------

Está a sujeto a modificación inclusive una vez comenzada la reunión. Se
sugiere que no haya más de 3 temas, así no se hace tan larga. Sugerimos
que si desea profundizar en alguna propuesta, puede dejar un link en la
misma wiki explayandosé todo lo que crea necesario, pero sea
considerado, no tenemos todo el día para leer, por lo tanto sea breve y
conciso.

-   Hoja de ruta (próximas acciones a seguir)
-   Contenido de la página
