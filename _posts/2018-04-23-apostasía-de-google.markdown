---
title: Apostasía de Google
layout: post
categories:
- Apostasía de Redes Sociales
---

[Categoría:Apostasía de Redes
Sociales](Categoría:Apostasía_de_Redes_Sociales "wikilink")

Apostasía de Google
===================

Reemplazar el buscador
----------------------

Hay un montón de buscadores que dicen que respetan nuestra privacidad.
Uno de los más conocidos es [DuckDuckGo](https://duckduckgo.com/). Sin
embargo, algunas personas desconfían de DDG, porque no es software
libre, aunque donan 10% de sus ingresos por publicidad a proyectos de
software libre.

Existe un proyecto libre llamado [SearX](https://searx.me/), que como
DDG, hace búsquedas en nuestros nombre en Google y otros buscadores,
pero sin entregar nuestra identidad. Se les llama meta-buscadores.

Al ser libre, puede haber muchas instancias de SearX funcionando y
podemos elegir la que más nos guste (¡o tener una nosotras!).

[Listado de instancias de SearX](https://stats.searx.xyz/)

¡Las piratas hemos estado usando SearX desde hace varios años sin
extrañar los resultados de Google!

Reemplazar el correo
--------------------

Hay muchos proveedores de correo electrónico, pagos y gratuitos, para
elegir. El factor decisivo es la confianza que genere el proyecto detrás
de ese servicio.

Para usos activistas, recomendamos [RiseUp](https://riseup.net/), un
colectivo norteamericano y [Autistici](https://autistici.org/), de
Italia, que ya tienen un recorrido de varios años proveyendo servicios
de correo, entre otros.

TODO: Agregar un listado de proveedoras menos hegemónicas
