---
title: Translations:Carta Orgánica/96/es
layout: post
categories: []
---

-   *Pase a bodega*, la suspensión temporal en las tareas asignadas.
-   *Vaya al carajo*, la suspensión temporal del derecho a voto en
    Asambleas.
-   *Caminar por la plancha*, la suspensión temporal del derecho a voz,
    voto y participación en actividades del Partido.
-   *Arrojarlo a los tiburones del mercado*, la expulsión del Partido.
    Esta sanción será apelable y resuelta definitivamente por la
    Asamblea General inmediata posterior.
