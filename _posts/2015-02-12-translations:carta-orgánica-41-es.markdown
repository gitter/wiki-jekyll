---
title: Translations:Carta Orgánica/41/es
layout: post
categories: []
---

En caso de no llegarse a Acuerdo, la propuesta será guardada en un
archivo histórico para futuras consultas y referencias en caso de que
nuevas propuestas guarden un parecido con alguna anterior.
