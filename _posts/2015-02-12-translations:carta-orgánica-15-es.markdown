---
title: Translations:Carta Orgánica/15/es
layout: post
categories: []
---

La afiliación se extinguirá por la afiliación a otro partido, por
renuncia o por expulsión.
