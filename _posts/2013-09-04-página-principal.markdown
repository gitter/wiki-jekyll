---
title: Página principal
layout: post
categories: []
---

+-----------------------------------------------------------------------+
| +--------------------------------+--------------------------------+   |
| | +-------------+-------------+  | -- ,\                          |   |
| | | <div style= | </div>      |  |  de de .\                      |   |
| | | "font-size: | <div style= |  | **** articulos.                |   |
| | | 162%; borde | "font-size: |  |                                |   |
| | | r:none; mar | 75%; border |  |                                |   |
| | | gin:0; padd | :none; marg |  |                                |   |
| | | ing:.1em; c | in:5; paddi |  |                                |   |
| | | olor:#6E98C | ng:.1em; co |  |                                |   |
| | | 2">         | lor:#444">  |  |                                |   |
| | | Bienvenidos | [Como usar  |  |                                |   |
| | | a la Wiki   | un          |  |                                |   |
| | | del Partido | MediaWiki]( |  |                                |   |
| | | Pirata      | http://camp |  |                                |   |
| | |             | usvirtual.u |  |                                |   |
| | |             | nex.es/cala |  |                                |   |
| | |             | /epistemowi |  |                                |   |
| | |             | kia/index.p |  |                                |   |
| | |             | hp?title=Co |  |                                |   |
| | |             | menzar_a_us |  |                                |   |
| | |             | ar_Mediawik |  |                                |   |
| | |             | i)          |  |                                |   |
| | |             |             |  |                                |   |
| | |             | </div>      |  |                                |   |
| | +-------------+-------------+  |                                |   |
| +--------------------------------+--------------------------------+   |
+-----------------------------------------------------------------------+

\_\_NOTOC\_\_

+-----------------------+-----------------------+-----------------------+
| <div class="mainpage_ | <div class="mainpage_ | <div class="mainpage_ |
| hubtitle">            | hubtitle">            | hubtitle">            |
| Partido Pirata {#part | Cultura Pirata {#cult | [Proyectos y campañas |
| ido-pirata}           | ura-pirata}           | ](Proyectos "wikilink |
| ==============        | --------------        | ") {#proyectos-y-camp |
|                       |                       | añas}                 |
| </div>                | </div>                | --------------------- |
| <div class="mainpage_ | <div class="mainpage_ | --------------------- |
| boxcontents">         | boxcontents">         | --                    |
| [Como Participar](Com | ### [Bandas Piratas]( |                       |
| o_Participar "wikilin | Bandas_Piratas "wikil | </div>                |
| k") {#como-participar | ink") {#bandas-pirata | <div class="mainpage_ |
| }                     | s}                    | boxcontents">         |
| --------------------- |                       | Ideas, proyectos,     |
| --------------------- | Tenemos que mostrar   | campañas, etcéteras.  |
| ---                   | que la alternativa    |                       |
|                       | copyleft es posible y | ### [Radio del Partid |
| Listas de correo,     | para eso hay que      | o Pirata](Radio_del_P |
| chat, comunicación y  | difundir a las bandas | artido_Pirata "wikili |
| organización.         | piratas.              | nk") {#radio-del-part |
|                       |                       | ido-pirata}           |
| Textos fundacionales  | ### [Libros Piratas]( |                       |
| del Partido Pirata {# | Libros_Piratas "wikil | Nos gustaría crear    |
| textos-fundacionales- | ink") {#libros-pirata | nuestro podcast, si   |
| del-partido-pirata}   | s}                    | desea participar de   |
| --------------------- |                       | él, envíe un e-mail   |
| ------------------    | ¿Libros, revistas y   | a:                    |
|                       | comics con licencia   | **ppirataargentino\#g |
| ### [Manifiesto](Mani | Creative Commons?     | mail.com**            |
| fiesto_del_Partido_Pi | También.              | (reemplace \# por @). |
| rata "wikilink") {#ma |                       |                       |
| nifiesto}             | ### [Películas Pirata | ### [Proyectos social |
|                       | s](Películas_Piratas  | es colaborativos](Pro |
| El manifiesto pirata  | "wikilink") {#películ | yectos_sociales_colab |
| poético votado en     | as-piratas}           | orativos "wikilink")  |
| asamblea 12/11/2009.  |                       | {#proyectos-sociales- |
|                       | Películas,            | colaborativos}        |
| ### [Declaración de P | documentales y videos |                       |
| rincipios Partido Pir | copyleft.             | La idea es crear      |
| ata Argentino](Declar |                       | proyectos sociales    |
| ación_de_Principios_P | ### [Programas Pirata | colaborativos que     |
| artido_Pirata_Argenti | s](Programas_Piratas  | tomen propia vida,    |
| no "wikilink") {#decl | "wikilink") {#program | para atacar problemas |
| aración-de-principios | as-piratas}           | sociales (como la     |
| -partido-pirata-argen |                       | crisis                |
| tino}                 | Software copyleft.    | habilitacional,       |
|                       |                       | inserción laboral,    |
| Redactada             | ### [Programas de Rad | etc) desde las bases. |
| conjuntamente. Basada | io Pirata](Programas_ |                       |
| en el manifiesto      | de_Radio_Pirata "wiki | ### [Observatorio Pen |
| poético.              | link") {#programas-de | al](Observatorio_Pena |
|                       | -radio-pirata}        | l "wikilink") {#obser |
| ### [Declaración de P |                       | vatorio-penal}        |
| rincipios Partido Pir | Programas de Radio    |                       |
| ata Internacional](De | con Temática Afín.    | Causas judiciales,    |
| claración_de_Principi |                       | aprietes legales,     |
| os_Partido_Pirata_Int | ### [Maquinaria Pirat | proyectos de ley,     |
| ernacional "wikilink" | a](Maquinaria_Pirata  | notas de interés y    |
| ) {#declaración-de-pr | "wikilink") {#maquina | mas.                  |
| incipios-partido-pira | ria-pirata}           |                       |
| ta-internacional}     |                       | ### [Nic-ar Censura?] |
|                       | Hardware,             | (Nic-ar_Censura? "wik |
| <http://www2.piratpar | herramientas y        | ilink") {#nic-ar-cens |
| tiet.se/international | maquinaria con        | ura}                  |
| /espanol>             | licencias libres para |                       |
|                       | construir vos mismo.  | Listado de dominios   |
| ### Carta Orgánica {# |                       | .com.ar que la        |
| carta-orgánica}       | </div>                | entidad               |
|                       |                       | administradora en     |
| Redactada             |                       | Argentina bloquea.    |
| conjuntamente, en     |                       |                       |
| revisión constante.   |                       | ### [Observatorio de  |
| <http://partidopirata |                       | ISPs](Observatorio_de |
| .com.ar/documentos/Ca |                       | _ISPs "wikilink") {#o |
| rtaOrganica.html>     |                       | bservatorio-de-isps}  |
|                       |                       |                       |
| [Preguntas frecuentes |                       | ¿Hay proveedores      |
| ](Preguntas_mas_frecu |                       | filtrando nuestro     |
| entes "wikilink") {#p |                       | trafico? ¿Dándonos    |
| reguntas-frecuentes}  |                       | menos de lo que nos   |
| --------------------- |                       | venden? Este es el    |
| --------------------- |                       | lugar para            |
| -----------------     |                       | investigar, comparar  |
|                       |                       | y denunciar estas     |
| Para sacarte esas     |                       | practicas.            |
| dudas que andan dando |                       |                       |
| vueltas.              |                       | ### [Observatorio Tel |
|                       |                       | efonicas](Observatori |
| [Multimedia del Parti |                       | o_Telefonicas "wikili |
| do](Multimedia_del_Pa |                       | nk") {#observatorio-t |
| rtido "wikilink") {#m |                       | elefonicas}           |
| ultimedia-del-partido |                       |                       |
| }                     |                       | ¿Tenes problemas con  |
| --------------------- |                       | proveedores de acceso |
| --------------------- |                       | telefonico o          |
| -----------------     |                       | fabricantes de        |
|                       |                       | telefonos?            |
| Panfletos, imagenes,  |                       | Compartilos.          |
| videos, logos,        |                       |                       |
| emoticons,            |                       | ### [Traducciones](Tr |
| wallpapers, lo que    |                       | aducciones "wikilink" |
| haya.                 |                       | ) {#traducciones}     |
|                       |                       |                       |
| [PirateFest](PirateFe |                       | Traducciones de       |
| st_2011 "wikilink") { |                       | artículos             |
| #piratefest}          |                       | interesantes que      |
| --------------------- |                       | distintos piratas     |
| -------------------   |                       | fueron llevando a     |
|                       |                       | cabo.                 |
| Festival anual de     |                       |                       |
| filosofía pirata. El  |                       | </div>                |
| primer festival       |                       |                       |
| pirata se llevó a     |                       |                       |
| cabo en paralelo a la |                       |                       |
| primera Asamblea      |                       |                       |
| General del Partido   |                       |                       |
| Pirata Internacional. |                       |                       |
| Luego se repitió en   |                       |                       |
| 2012.                 |                       |                       |
|                       |                       |                       |
| [Artículos de Interés |                       |                       |
| ](Artículos_de_Interé |                       |                       |
| s "wikilink") {#artíc |                       |                       |
| ulos-de-interés}      |                       |                       |
| --------------------- |                       |                       |
| --------------------- |                       |                       |
| -------------         |                       |                       |
|                       |                       |                       |
| Esta página contiene  |                       |                       |
| artículos de          |                       |                       |
| temáticas             |                       |                       |
| relacionadas con el   |                       |                       |
| proyecto como Cultura |                       |                       |
| Libre, Derechos de    |                       |                       |
| Autor, Patentes.      |                       |                       |
|                       |                       |                       |
| </div>                |                       |                       |
+-----------------------+-----------------------+-----------------------+

+-----------------------+-----------------------+-----------------------+
| <div class="mainpage_ | <div class="mainpage_ | <div class="mainpage_ |
| hubtitle">            | hubtitle">            | hubtitle">            |
| Guias y tutoriales {# | Información {#informa | Ideas {#ideas}        |
| guias-y-tutoriales}   | ción}                 | -----                 |
| ------------------    | -----------           |                       |
|                       |                       | </div>                |
| </div>                | </div>                | <div class="mainpage_ |
| <div class="mainpage_ | <div class="mainpage_ | boxcontents">         |
| boxcontents">         | boxcontents">         | ### [Compartir es bue |
| ### [Guia P2P](Guia_P | ### [Cannabis](Cannab | no](Compartir_es_buen |
| 2P "wikilink") {#guia | is "wikilink") {#cann | o "wikilink") {#compa |
| -p2p}                 | abis}                 | rtir-es-bueno}        |
|                       |                       |                       |
| Guia colaborativa     | Información sobre los | Campaña enfocada a    |
| sobre como funciona   | diferentes usos del   | instalar la idea de   |
| internet, como        | cannabis, estado      | que compartir es      |
| funciona la censura   | legal, agrupaciones,  | bueno y no tiene por  |
| en internet y como    | etc.                  | que ser ilegal.       |
| esquivarla.           |                       |                       |
|                       | ### [Derecho de Autor | ### [Política Digital |
| ### [Torrents/Magnets | ](Derecho_de_Autor "w | ](Política_Digital "w |
| ](Torrents/Magnets "w | ikilink") {#derecho-d | ikilink") {#política- |
| ikilink") {#torrentsm | e-autor}              | digital}              |
| agnets}               |                       |                       |
|                       | En esta página podrás | ¿Qué es una políica   |
| No se puede prohibir  | encontrar las leyes   | digital? ¿a qué está  |
| un standard, o se     | vigentes relacionadas | enfocada?             |
| puede? Qué son y qué  | con el derecho de     |                       |
| formas creativas      | autor y proyectos     | ### [Adopta un políti |
| podemos encontrar     | presentados en        | co](Adopta_un_polític |
| para usarlos?         | organismos            | o "wikilink") {#adopt |
|                       | legislativos.         | a-un-político}        |
| ### [Subtítulos](Subt |                       |                       |
| ítulos "wikilink") {# | </div>                | Programa enfocado a   |
| subtítulos}           |                       | establecer un dialogo |
|                       |                       | con diputados,        |
| Como hacerlos, de     |                       | senadores y demás     |
| donde bajarlos y como |                       | actores políticos.    |
| combinarlos con los   |                       | Muchos de los cuales  |
| videos.               |                       | desconocen los        |
|                       |                       | objetivos del         |
| ### [Mumble](Mumble " |                       | movimiento pirata.    |
| wikilink") {#mumble}  |                       |                       |
|                       |                       | ### [A quien votar](A |
| Charlá y chateá por   |                       | _quien_votar "wikilin |
| internet sin depender |                       | k") {#a-quien-votar}  |
| de un servidor        |                       |                       |
| central ni software   |                       | Cuestionario para     |
| privativo.            |                       | hacerle a candidatos  |
|                       |                       | a senadores y         |
| ### [Chili - Administ |                       | diputados, para saber |
| rador de proyectos](C |                       | a quien votar.        |
| hili_-_Administrador_ |                       |                       |
| de_proyectos "wikilin |                       | ### [Cooperativas soc |
| k") {#chili---adminis |                       | iales y estatales](Co |
| trador-de-proyectos}  |                       | operativas_sociales_y |
|                       |                       | _estatales "wikilink" |
| Para la organización  |                       | ) {#cooperativas-soci |
| interna y la división |                       | ales-y-estatales}     |
| de tareas utilizamos  |                       |                       |
| un administrados de   |                       | ¿Que seria una        |
| proyectos libre       |                       | cooperativa social y  |
| llamado Chilli.       |                       | una cooperativa       |
|                       |                       | estatal? ¿Que         |
| ### [Operación Leviat |                       | beneficios traerian?  |
| án Electrónico](Opera |                       | ¿Como se podrian      |
| ción_Leviatán_Electró |                       | promover y priorizar  |
| nico "wikilink") {#op |                       | desde el estado?      |
| eración-leviatán-elec |                       |                       |
| trónico}              |                       | </div>                |
|                       |                       |                       |
| Privacidad, cifrado,  |                       |                       |
| comunicación anónima  |                       |                       |
| y compartir archivos  |                       |                       |
| de forma segura.      |                       |                       |
|                       |                       |                       |
| ### [Crear proxy The  |                       |                       |
| Pirate Bay](Crear_pro |                       |                       |
| xy_The_Pirate_Bay "wi |                       |                       |
| kilink") {#crear-prox |                       |                       |
| y-the-pirate-bay}     |                       |                       |
|                       |                       |                       |
| Cómo crear tu propio  |                       |                       |
| proxy a The Pirate    |                       |                       |
| Bay.                  |                       |                       |
|                       |                       |                       |
| ### [Guía para regist |                       |                       |
| rar una obra con Crea |                       |                       |
| tive Commons](http:// |                       |                       |
| virtual.flacso.org.ar |                       |                       |
| /pluginfile.php/389/m |                       |                       |
| od_page/content/16/Gu |                       |                       |
| ia_de_licencias_Creat |                       |                       |
| ive_Commons_v2.pdf) { |                       |                       |
| #guía-para-registrar- |                       |                       |
| una-obra-con-creative |                       |                       |
| -commons}             |                       |                       |
|                       |                       |                       |
| ¿Qué son las          |                       |                       |
| licencias Creative    |                       |                       |
| Commons?, ¿Cómo       |                       |                       |
| registrar una obra    |                       |                       |
| con Creative Commons? |                       |                       |
|                       |                       |                       |
| ### OTR {#otr}        |                       |                       |
|                       |                       |                       |
| Prueba [Pidgin con    |                       |                       |
| OTR](Pidgin_con_OTR " |                       |                       |
| wikilink")            |                       |                       |
| o [Gibberbot con      |                       |                       |
| OTR](Gibberbot_con_OT |                       |                       |
| R "wikilink")         |                       |                       |
| para Android.         |                       |                       |
|                       |                       |                       |
| </div>                |                       |                       |
+-----------------------+-----------------------+-----------------------+
