---
title: Translations:Carta Orgánica/73/en
layout: post
categories: []
---

The General Assembly will inform and publish for historic purposes, and
in a period of no more than seven (7) days written Proceedings of the
happenings, participants, decisions made and their approval proportion,
and a digital record of the activities.
