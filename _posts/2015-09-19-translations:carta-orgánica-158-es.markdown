---
title: Translations:Carta Orgánica/158/es
layout: post
categories: []
---

Los Barcos Piratas pueden constituir un Cofre para financiar sus
actividades. La administración del Cofre debe respetar lo establecido
por la Administración de Fondos y el Órgano Fiducidario del Partido.
