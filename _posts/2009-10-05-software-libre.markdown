---
title: Software Libre
layout: post
categories: []
---

**Sistemas BSD y GNU/Linux**
----------------------------

*\' Introducción*\'

Algunos usuarios se encontrarán con otro tipo de sistemas operativos,
aún más desconocidos que el mismo Linux, estos son los BSD, como NetBSD,
FreeBSD, OpenBSD, y sus derivados. En este artículo intentaremos
explicar las diferencias y similitudes de estos sistemas operativos.

Los tres, FreeBSD, OpenBSD y NetBSD estan basados en BSD pero en
realidad son diferentes sistemas operativos con diferente núcleo, cada
sistema tiene un equipo que se encarga de él.

Las diferentes distribuciones Linux son diferentes sistemas pero con un
mismo núcleo.

Ninguna persona o empresa posée BSD. Su creación y distribución es obra
de una comunidad de voluntarios altamente cualificados y comprometidos a
lo largo y ancho del mundo. Algunos de los componentes de BSD son
proyectos de Código Abierto que cuentan con responsables ajenos al
proyecto BSD. Versiones de BSD

Cada proyecto BSD ponen a disposición pública tres "releases"
(versiones) distintas. Igual que en Linux, las "releases" tienen
asignado un número como por ejemplo 1.4.1 ó 3.5. Además el número de
versión tiene un sufijo que indica su propósito:

-   La versión de desarrollo del sistema recibe el nombre de CURRENT.
    FreeBSD asigna un número a CURRENT, por ejemplo FreeBSD 5.0-CURRENT.
    NetBSD utiliza un sistema ligeramente diferente y añade un sufijo
    compuesto por una única letra que indica cambios en las interfaces
    internas, por ejemplo NetBSD 1.4.3G. OpenBSD no asigna ningún número
    ("OpenBSD-current"). ésta rama es la que incluye todo el desarrollo.
-   A intervalos regulares, entre dos y cuatro veces al año, los
    proyectos liberan una versión RELEASE del sistema, que está
    disponible en CD-ROM y mediante FTP para su descarga gratuíta, por
    ejemplo OpenBSD 2.6-RELEASE o NetBSD 1.4-RELEASE. La versión RELEASE
    está dirigida al usuario final y es la versión "estándar" del
    sistema. NetBSD también dispone de patch releases que incluyen un
    tercer dígito, como por ejemplo NetBSD 1.4.2.
-   A medida que se van encontrando errores en la versión RELEASE son
    corregidos y las soluciones son incluídas en el árbol CVS
    (Concurrency Versions System, Sistema Concurrente de Versiones). En
    FreeBSD la versión resultante se denomina versión STABLE, mientras
    que en NetBSD y OpenBSD continúa siendo la versión RELEASE. Nuevas
    características más pequeñas pueden ser añadidas en ésta rama tras
    un período de pruebas en la rama CURRENT.
-   Linux, en cambio, mantiene dos árboles de código separados: la
    versión estable y la versión de desarrollo. Las versiones estables
    añaden un número par de versión, como 2.0, 2.2 ó 2.4. Las versiones
    de desarrollo añaden un número impar, como en 2.1, 2.3 ó 2.5. En
    ambos casos a ese número se le añade otro más que indica la versión
    exacta. Por si fuera poco cada distribuidor añade sus propios
    programas y aplicaciones de entorno de usuario, así que el número de
    versión es importante. Cada distribuidor además asigna números de
    versión a la distribución, así pues la descripción completa podría
    ser algo como "SUSE Linux 10.0 OSS con kernel 2.6.14″

**Cuántas versiones de BSD existen**

A diferencia de las numerosas distribuciones de Linux tan sólo hay tres
BSD libres. Cada proyecto BSD mantiene su propio árbol de fuentes y su
propio kernel. En la práctica, sin embargo, las diferencias en el
entorno de usuario entre los distintos BSD son menores que las que hay
en Linux.

Es difícil enumerar los objetivos de cada proyecto puesto que las
diferencias son muy subjetivas. En general,

-   FreeBSD tiene como meta ofrecer alto rendimiento y facilidad de uso
    al usuario final y es uno de los favoritos entre proveedores de
    contenidos web. Funciona en PC y en procesadores Alpha de Compaq. El
    proyecto FreeBSD cuenta con un número de usuarios significativamente
    mayor que los otros proyectos.
-   NetBSD tiene como meta la Portabilidad: No en vano su lema es "of
    course it runs NetBSD" (que podría traducirse como "por supuesto que
    ejecuta NetBSD"). Funciona en máquinas que abarcan desde PDAs a
    grandes servidores e incluso ha sido usado por la NASA en misiones
    espaciales. Es una excelente elección para utilizar viejo hardware
    no Intel.
-   OpenBSD tiene como meta la seguridad y la integridad del código:
    combina el concepto de código abierto y una revisión rigurosa del
    código que dan como fruto un sistema muy correcto, elegido por
    instituciones preocupadas por la seguridad como bancos, entidades de
    cambio y departamentos gubernamentales de los EEUU. Al igual que
    NetBSD funciona en gran variedad de plataformas.

**Existen dos sistemas operativos BSD más que no son de código abierto,
BSD/OS y el MacOS X de Apple:**

-   BSD/OS era un derivado más antíguo de 4.4BSD. No era código abierto
    pero era posible conseguir licencias de su código fuente a un precio
    relativamente bajo. Se parecía a FreeBSD en muchos aspectos. El 31
    de diciembre de 2004 se finalizó el desarrollo y venta de este
    sistema.
-   Mac OS X es la última versión del sistema operativo para la gama
    Macintosh de Apple Computer Inc. El núcleo BSD Unix de éste sistema
    operativo, Darwin, está libremente disponible como sistema operativo
    de fuente abierto totalmente funcional para arquitecturas x86 y PPC.
    El sistema gráfico Aqua/Quartz y la mayoría de las demás aspectos
    característicos de Mac OS X son código cerrado. Varios
    desarrolladores de Darwin son también "committers" de FreeBSD y
    viceversa.

**Diferencias y Similitudes**

Nota: Al referirnos a "BSD" nos estarémos refiriendo a FreeBSD, OpenBSD,
NetBSD y BSDI indistintamente. Al referirnos a "Linux" nos referiremos
al núcleo o kernel.

-   BSD es un sistema operativo descendiente directo del Unix original
    (basados en 4.4BSD-Lite liberado del Computer Systems Research Group
    de la Universidad de California en Berkeley; no contiene código
    residual de AT&T, sin embargo es correcto llamarlo "el Unix BSD"
    porque en efecto lo es. Linux es un clone que no tiene ningún código
    AT&T y ninguna similitud con 4.4BSD-Lite, porque fue escrito desde
    cero, es incorrecto decir que Linux es "un Unix", es mejor decir que
    es "un sistema tipo Unix".
-   BSD son sistemas operativos completos, mantenidos por un grupo
    central de desarrolladores que hacen una única distribución. Linux
    es solamente el kernel, mantenido personalmente por Linus Torvalds
    con la colaboración de Alan Cox, Teo T'so, Stephen Tweedie y muchos
    otros; sin embargo los programas adicionales son parte de una de
    muchas distribuciones. Los sistemas BSD también usan mucho software
    GNU.
-   Los BSD son mucho más conservadores en la integración de
    características, y se autocalifican como "apuntando a sistemas de
    producción estable". Linux es un ambiente que en ocasiones está en
    la punta tecnológica de la innovación, aunque siempre hay un rama
    estable. Cada BSD tiene un énfasis diferente: FreeBSD (Intel y
    Alpha, servicios generales y estaciones de trabajo), NetBSD (corre
    en más plataformas que Linux, servidor) y OpenBSD (varias
    plataformas, servidor seguro). Es mucho más complicado convertir un
    servidor BSD en estación de trabajo porque hay que instalar mucho
    software adicional desde los fuentes.
-   Como los BSD tienen una única distribución, se instalan de una sola
    forma. La facilidad de instalación de Linux depende de la
    distribución. No hay RPM, DEB ni TGZ, ni front-end gráficos para
    instalar paquetes: o está portado y bajas el binario en un TAR, o no
    está portado y deberás realizar todo el proceso manualmente (edición
    de archivos, ejecución de comandos, compilación, etc.)
-   BSD es relativamente desconocido entre los usuarios de software
    libre porque durante mucho tiempo estaba sometido a restricciones de
    licencia de AT&T. Como Linux no le debe código a nadie, fue el
    pionero en ser un "clon Unix gratutito". Hoy en día ambos son
    enteramente gratis y libres, pero la licencia de BSD es más
    permisiva que la licencia GPL de la Free Software Foundation, bajo
    la cual esta desarrollado Linux.
-   Hay muy pocas aplicaciones comerciales nativas para BSD, comparadas
    con las disponibles para Linux. Ejemplos son: Oracle, Informix,
    StarOffice, Acrobat Reader y Realplayer. Sin embargo BSD puede
    emular ejecutables Linux y en consecuencia ejecutar las aplicaciones
    sin mayores problemas. Linux no corre binarios de BSD, básicamente
    porque todo lo que hay para BSD existe para Linux en código fuente.
-   Como la base de usuarios es menor en BSD, es mucho más difícil tener
    controladores (drivers) para los nuevos dispositivos. Los drivers
    Linux no son compatibles con los de BSD, con lo que no es tan simple
    reescribirlos. Sin embargo, recordando el punto 3, los BSD tienen un
    soporte excelente para hardware "serio" (el SCSI, es notablemente
    rápido, y el IDE es bastante regular; las tarjetas de red PCI 3Com o
    WD funcionan mucho mejor que en Linux, pero las NE2000 y cualquier
    basura programable I/O sin DMA ni mapeo de memoria no es tan
    agradable de tener.
-   Los BSD usan el FFS (Fast File System) derivado del original escrito
    por Kirk McKusick; Linux usa ext2, una reimplementación desde cero
    del original de Kirk, escrito por Stephen Tweedie y Teo T'so sobre
    unos fuentes de Minix. También Linux usa ext3, RiserFS, y muchos
    más. Pero Linux ha copiado los ACL y los bits de seguridad de BSD.
    Linux soporta muchos más sistemas de archivo que los BSD.
-   Los argumentos pro-BSD basados en el manejo de memoria son
    absolutamente ciertos hasta que 2.4 se estabilice completamente. La
    escalabilidad de los dos es más o menos igual, salvo quizás en
    plataforma Alpha y UltraSparc donde BSD es completamente 64 bits,
    mientras Linux es 32/64. Sin embargo, BSD no puede funcionar en
    escenarios de memoria escasa ni tiene soporte adecuado para trabajar
    en dispositivos embebidos.
-   Los argumentos pro-BSD de la seguridad son relativos. Como BSD tiene
    una única distribución, se hacen muchos esfuerzos en verificar el
    código de todo, mientras que Linux no se preocupa más allá del
    kernel, siendo responsabilidad de las distribuciones incluir
    versiones estables y verificadas. De manera que en el primer caso se
    trata de que hay una sola tribu haciendo flechas, mientras que en la
    segunda hay una relación de codependencia entre varias tribus. En
    particular OpenBSD se esfuerza en asegurar la máxima seguridad de su
    distribución, y para ello usualmente tiene versiones viejas de
    muchos paquetes comparados con lo que uno puede encontrar en Linux y
    otros BSD.
-   El modelo "todo del mismo proveedor" de BSD implica que las
    actualizaciones son mucho más sencillas de gestionar de lo que con
    frecuencia son en Linux. BSD maneja las actualizaciones de versiones
    de bibliotecas suministrando módulos de compatibilidad para
    versiones anteriores, de modo que es posible ejecutar binarios con
    varios años de antiguedad sin problemas.
-   El ciclo de desarrollo e innovación en Linux es mucho más rápido que
    el de los BSD, y por eso aparece soporte a muchos más dispositivos y
    funcionalidades que en BSD. Esto no quiere decir que todas sean
    útiles para todo el mundo, ni que tengan todas el mismo grado de
    calidad y estabilidad. BSD es mucho más pausado en su evolución.

**Derivados**

-   DesktopBSD: Es un sistema operativo basado en FreeBSD y el live cd
    FreeSBIE. Su principal meta es proporcionar un sistema operativo de
    escritorio que sea fácil de utilizar, pero con toda la funcionalidad
    y potencia de BSD. A largo plazo, DesktopBSD desea construir un
    sistema operativo que reuna la mayoría de los requisitos que los
    usuarios de escritorio tienen, como la instalación de software,
    configuración de la administración de energía o compartir una
    conexión del Internet.http://www.desktopbsd.net/

<!-- -->

-   DragonFly BSD: Es un sistema operativo y un ambiente diseñado para
    ser la continuación lógica de la serie de sistemas operativos
    FreeBSD-4.x. DragonFly BSD es una bifurcación en el camino, dando a
    la base BSD una oportunidad de crecer en una completa nueva
    dirección de la que esta tomando la serie
    FreeBSD-5.http://www.dragonflybsd.org/

<!-- -->

-   FreeSBIE -- FreeBSD Live CD: FreeSBIE es un sistema live-cd, o un
    sistema operativo que tiene la capacidad para cargarse directamente
    desde un cd arrancable, sin ningun proceso de instalación, sin
    necesidad de algún disco duro. Se basa en el sistema operativo
    FreeBSD. Las metas del proyecto FreeSBIE son principalmente dos:
    desarrollar un conjunto de programas que puedan ser usados para
    crear su propio CD, con toda la personalización que desee, y hacer
    varias imágenes de la ISO disponibles, quizás cada una con diversas
    metas y usos posibles. El proyecto es desarrollado por el grupo de
    usuario italiano principal de FreeBSD: GUFI.http://www.freesbie.org/

<!-- -->

-   Frenzy: Es una caja de herramientas del administrador de sistema
    portable, un live CD basado en FreeBSD. Contiene generalmente el
    software para las pruebas de hardware, chequeo del sistema de
    archivos, chequeo de la seguridad y configuración y análisis de la
    red.http://frenzy.org.ua/

<!-- -->

-   GuLIC-BSD: Es un live CD de FreeBSD basado en FreeSBIE y diseñado
    por españoles.http://lebrillo.osl.ull.es/projects/gulicbsd/

<!-- -->

-   m0n0wall: Es un proyecto animado en crear un paquete de software de
    cortafuego completo, proporciona todas las características
    importantes de las cajas comerciales de cortafuegos. m0n0wall se
    basa en una versión de FreeBSD.http://www.m0n0.ch/wall/

<!-- -->

-   MirOS Project: MirOS apunta a los servidores pequeños y los sitios
    de trabajo de los desarrolladores; es un sistema operativo pequeño,
    muy seguro, completamente libre. MirOS BSD es un derivado de OpenBSD
    y NetBSD y funciona actualmente sobre i386 y SPARC -- PPC
    pronto.http://mirbsd.mirsolutions.de/

<!-- -->

-   PC-BSD: Tiene como metas ser fácil de instalar y usar el sistema
    operativo de escritorio, basado en FreeBSD. Para lograr esto, tiene
    actualmente una instalación gráfica, que permitirá a los
    principiantes de UNIX instalarlo y conseguir fácilmente que
    funcione. También vendrá con KDE pre-compilado, para poder utilizar
    el escritorio inmediatamente. Actualmente está en desarrollo un
    programa gráfico para la instalación de software, que hará la
    instalación de software pre-compilado tan fácil como en otros
    sistemas operativos populares.http://www.pcbsd.org/

<!-- -->

-   pfSense: es un sistema operativo derivado de m0n0wall. Utiliza
    filtro de paquetes, FreeBSD 6.x (o DragonFly BSD cuando este
    terminado), y un administrador de paquetes de sistema integrado para
    extender el ambiente con nuevas
    características.http://www.pfsense.com/

**Conclusión**

Se puede escoger entre cualquiera de ellos porque el problema no está en
la herramienta sino en la habilidad del usuario y en la capacidad de
explorar las facilidades que tiene en un tiempo corto y sin arriesgar la
instalación. Un administrador Unix que no es capaz de usar uno diferente
al acostumbrado, no es más que un simple "usuario avanzado de cierta
distribución".

FreeBSD ha resultado más rápido que Linux en un servidor Web con Perl y
mod\_perl en una simulación de 200 hits simultáneos. FreeBSD ha
resultado más rápido que Linux en un servidor de archivos NFS, en un
ambiente 100% Unix de desarrollo. Sin embargo Linux ha resultado mucho
más rápido que FreeBSD en una estación de trabajo multimedia para acceso
a Internet.

Como conclusión podemos indicar que ambos sistemas operativos son
seguros, rápidos, estables, ajustables a las necesidades del usuario,
los dos están basados en estándares, los dos interoperan, los dos dan
independencia, y principalmente, ambos son sistemas operativos Libres.

Escoger uno u otro es una cuestión de experiencia de uso y de gusto.

Link:
<http://portallinux.wordpress.com/2008/08/07/sistemas-bsd-y-linux/>
<http://www.microteknologias.cl/linux_bsdlinux.html>

**4.0. Comparemos BSD y Linux**

De manera que, ¿cuál es la diferencia entre, digamos, Debian Linux y
FreeBSD? Para el usuario avanzado la diferencia es sorprendentemente
pequeña: ambos son sistemas operativos tipo UNIX. Ambos son
desarrollados por proyectos no comerciales (ésto, por supuesto, no es
aplicable a la mayoría del resto de distribuciones de Linux). En el
siguiente apartado tomaremos BSD como punto de partida y lo compararemos
con Linux. La descripción se ajusta más a FreeBSD, que posée
aproximadamente el 80% de los sistemas BSD instalados, pero las
diferencias con NetBSD y OpenBSD son pequeñas. **4.1. ¿Quién posée
BSD?**

Ninguna persona o empresa posée BSD. Su creación y distribución es obra
de una comunidad de voluntarios altamente cualificados y comprometidos a
lo largo y ancho del mundo. Algunos de los componentes de BSD son
proyectos de Código Abierto que cuentan con responsables ajenos al
proyecto BSD. **4.2. ¿Cómo se desarrolla y actualiza BSD?**

Los kernel BSD son desarrollados y actualizados siguiendo el modelo de
desarrollo de Código Abierto. Cada proyecto mantiene un árbol de fuentes
accesible públicamente mediante un "Sistema Concurrente de Versiones"
(Concurrency Versions System, CVS), que contiene todos los ficheros
fuente del proyecto, incluídos los de la documentación y otros ficheros
relacionados. CVS permite a los usuarios "hacer un check out" (en otras
palabras, extraer una copia) de los ficheros que componen la versión
elegida del sistema.

Un gran número de desarrolladores de muy diversas partes del mundo
contribuye con mejoras a BSD. Estan divididos en tres categorías:

-   Contributors son aquellos que escriben código o documentación. No se
    les permite "hacer commit" (es decir, añadir código) directamente al
    árbol de fuentes. Para que su código sea incluído en el sistema debe
    ser revisado y probado por un desarrollador registrado. Un/a
    committer.
-   Committers son desarrolladores que disponen de acceso de escritura
    en el árbol de fuentes. Para convertirse en committer es necesario
    demostrar habilidad en el área en la cual él o ella trabaja.

Depende del criterio individual de cada committer cuándo pedir
autorización antes de hacer cambios en el árbol de fuentes. En general
un committer experimentado puede incluír cambios que son obviamente
correctos sin necesidad de consenso. Por ejemplo, un/a committer que
trabaje en un proyecto de documentación puede corregir errores
tipográficos o gramaticales sin necesidad de revisión. Por otra parte,
se espera de desarrolladores que pretendan realizar cambios de gran
calado o complicados que envíen sus cambios para que sean revisados
antes de ser incluídos. En casos extremos un miembro del "core team" con
una función como la del Principal Architect puede pedir que los cambios
sean retirados del árbol; es lo que llamamos backing out. Todos los/las
committers reciben un correo electrónico acerca de cada cambio concreto
en el árbol de fuentes así que no es posible hacerlo en secreto.

-   El Core team. Tanto FreeBSD como NetBSD disponen de un "core team"
    que coordina el proyecto. Los "core team" dirigen el rumbo de los
    proyectos pero sus funciones no siempre están claras. No es
    necesario ser desarrollador para ser un miembro de un "core team"
    pero suele ser lo habitual. Las normas de un "core team" varían de
    un proyecto a otro pero en general tienen más influencia sobre la
    dirección del proyecto.

**Éte sistema difiere del de Linux en algunos aspectos:**

1\. Nadie posée el principio de autoridad. En la práctica eso es muy
relativo, puesto que el "Chief Architect" puede solicitar que cierta
entrada del árbol de fuentes sea eliminada e incluso en el proyecto
Linux a ciertas personas les está permitido hacer cambios. 2. Por otra
parte hay un repositorio central, un único lugar donde encontrar las
fuentes del sistema operativo íntegro, incluyendo todas las versiones
anteriores. 3. Los BSD mantienen el "Sistema Operativo" completo, no
únicamente el kernel. Ésta distinción es válida únicamente como
definición puesto que ni BSD ni Linux son útiles sin aplicacionesr: las
aplicaciones que se usan en BSD suelen ser las mismas que las que se
usan en Linux. 4. Como resultado del mantenimiento estructurado de un
único árbol de fuentes mediante CVS el desarrollo de BSD es limpio y es
posible acceder a cualquier versión del sistema por su número de versión
o por la fecha. Del mismo modo CVS permite actualizaciones incrementales
del sistema: por ejemplo el repositorio de FreeBSD es actualizado en
torno a 100 veces al día, aunque la mayoría de esos cambios son
pequeños.

**4.3. Versiones de BSD**

Cada proyecto BSD pone a disposición pública tres "releases" (versiones)
distintas. Igual que en Linux, las "releases " tienen asignado un número
como por ejemplo 1.4.1 ó 3.5. Además el número de versión tiene un
sufijo que indica su propósito:

1\. La versión de desarrollo del sistema recibe el nombre de CURRENT.
FreeBSD asigna un número a CURRENT, por ejemplo FreeBSD 5.0-CURRENT.
NetBSD utiliza un sistema ligeramente diferente y añade un sufijo
compuesto por una única letra que indica cambios en las interfaces
internas, por ejemplo NetBSD 1.4.3G. OpenBSD no asigna ningún número
("OpenBSD-current"). Ésta rama es la que incluye todo el desarrollo. 2.
A intervalos regulares, entre dos y cuatro veces al año, los proyectos
liberan una versión RELEASE del sistema, que está disponible en CD-ROM y
mediante FTP para su descarga gratuíta, por ejemplo OpenBSD 2.6-RELEASE
o NetBSD 1.4-RELEASE. La versión RELEASE está dirigida al usuario final
y es la versión "estándar " del sistema. NetBSD también dispone de patch
releases que incluyen un tercer dígito, como por ejemplo NetBSD 1.4.2.
3. A medida que se van encontrando errores en la versión RELEASE son
corregidos y las soluciones son incluídas en el árbol CVS. En FreeBSD la
versión resultante se denomina versión STABLE, mientras que en NetBSD y
OpenBSD continúa siendo la versión RELEASE. Nuevas características más
pequeñas pueden ser añadidas en ésta rama tras un período de pruebas en
la rama CURRENT.

Linux, en cambio, mantiene dos árboles de código separados: la versión
estable y la versión de desarrollo. Las versiones estables añaden un
número par de versión, como 2.0, 2.2 ó 2.4. Las versiones de desarrollo
añaden un número impar, como en 2.1, 2.3 ó 2.5. En ambos casos a ese
número se le añade otro más que indica la versión exacta. Por si fuera
poco cada distribuidor añade sus propios programas y aplicaciones de
entorno de usuario, así que el número de versión es importante. Cada
distribuidor además asigna números de versión a la distribución, así
pues la descripción completa podría ser algo como "TurboLinux 6.0 with
kernel 2.2.14" **4.4. ¿Cuántas versiones de BSD existen?**

A diferencia de las numerosas distribuciones de Linux tan sólo hay tres
BSD libres. Cada proyecto BSD mantiene su propio árbol de fuentes y su
propio kernel. En la práctica, sin embargo, las diferencias en el
entorno de usuario ("userland") entre los distintos BSD son menores que
las que hay en Linux.

Es difícil enumerar los objetivos de cada proyecto puesto que las
diferencias son muy subjetivas. En general,

-   FreeBSD tiene como meta ofrecer alto rendimiento y facilidad de uso
    al usuario final y es uno de los favoritos entre proveedores de
    contenidos web. Funciona en PC y en procesadores Alpha de Compaq. El
    proyecto FreeBSD cuenta con un número de usuarios significativamente
    mayor que los otros proyectos.
-   NetBSD tiene como meta la Portabilidad: No en vano su lema es "of
    course it runs NetBSD" (que podría traducirse como "claro que
    funciona con NetBSD"). Funciona en máquinas que abarcan desde PDAs a
    grandes servidores e incluso ha sido usado por la NASA en misiones
    espaciales. Es una excelente elección para utilizar viejo hardware
    no Intel.
-   OpenBSD tiene como meta la seguridad y la integridad del código:
    combina del concepto de código abierto y una revisión rigurosa del
    código que dan como fruto un sistema muy correcto, elegido por
    instituciones preocupadas por la seguridad como bancos, entidades de
    cambio y departamentos gubernamentales de los EEUU. Al igual que
    NetBSD funciona en gran variedad de plataformas.

**Existen dos sistemas operativos BSD más que no son de código abierto,
BSD/OS y el MacOS X de Apple:**

-   BSD/OS es el derivado más antíguo de 4.4BSD. No es código abierto
    pero es posible conseguir licencias de su código fuente a un precio
    relativamente bajo. Se parece a FreeBSD en muchos aspectos.
-   Mac OS X es la última versión del sistema operativo para la gama
    Macintosh de Apple Computer Inc. El núcleo BSD Unix de éste sistema
    operativo, Darwin, está libremente disponible como sistema operativo
    de fuente abierto totalmente funcional para arquitecturas x86 y PPC.
    El sistema gráfico Aqua/Quartz y la mayoría de las demás aspectos
    característicos de Mac OS X son código cerrado. Varios
    desarrolladores de Darwin son también "committers" de FreeBSD y
    viceversa.

**4.5. ¿Qué diferencias hay entre la licencia BSD y la licencia pública
GNU?**

Linux está disponible bajo la GNU General Public License (GPL), que fué
diseñada para evitar el software cerrado. Más concretamente, cualquier
trabajo derivado de un producto con licencia GPL debe suministrar el
código fuente si es requerido. En contraste, la licencia BSD es menos
restrictiva: permite la distribución en forma exclusivamente binaria.
Éste aspecto es especialmente atractivo para aplicaciones empotradas.
4.6. ¿Qué más debería saber?

Dado que existen menos aplicaciones para BSD que para Linux los
desarrolladores de BSD han creado un paquete de compatibilidad con Linux
que permite hacer funcionar programas de Linux bajo BSD. El paquete
contiene tanto modificaciones del kernel, con el fín de gestionar
correctamente las llamadas al sistema de Linux, como ficheros necesarios
para la compatibilidad con Linux como la Biblioteca C. No hay
diferencias notables en velocidad de ejecución entre una aplicación de
Linux ejecutándose en un sistema Linux y una aplicación Linux
ejecutándose en un sistema BSD de la misma velocidad.

El modelo "todo del mismo proveedor" de BSD implica que las
actualizaciones son mucho más sencillas de gestionar de lo que con
frecuencia son en Linux. BSD maneja las actualizaciones de versiones de
bibliotecas suministrando módulos de compatibilidad para versiones
anteriores, de modo que es posible ejecutar binarios con varios años de
antiguedad sin problemas. 4.7. Entonces ¿Qué debería usar, BSD o Linux?
*\' ¿Qué significa realmente esa pregunta? ¿Quién debería utilizar BSD y
quién Linux?.*\'

Ésta es una pregunta muy difícil de responder. He aquí varias pautas:

"Si no está roto no lo arregles": Si ya usa un sistema operativo de
código abierto y está satisfecho con él, probablemente no hay ninguna
buena razón para cambiar.

Los sistemas BSD, especialmente FreeBSD, pueden proporcionar un
rendimiento notablemente superior que Linux, pero ésto no es una ley
inmutable. En muchos casos no hay diferencia de rendimiento o ésta es
muy pequeña. En algunos casos Linux podría tener un rendimiento mejor
que FreeBSD.

En general los sistemas BSD gozan de una mejor reputación en cuanto a
disponibilidad, principalmente por la mayor madurez de su código base.

La licencia BSD puede resultar más atractiva que la GPL.

BSD puede ejecutar código de Linux, mientras que Linux no puede hacer lo
propio con código de BSD. Como resultado de ésto hay una mayor cantidad
de software disponible para BSD que para Linux.

Extraído de:
<http://www.freebsd.org/doc/es_ES.ISO8859-1/articles/explaining-bsd/x99.html>

Porqué Red Hat prefirió la licencia GPL y no la BSD. La GPL ayuda a los
negocios

Bob Young, cofundador de RedHat se retiró del consejo de dirección de la
empresa hace dos días. para poder dedicarse más a su proyecto Lulu.
RedHat jugó un papel muy importante en el software libre y GNU/Linux,
fue la primera empresa que montó todo un modelo de negocio basado en él,
la primera en salir a bolsa y sin duda alguna, ha creado "marca" y
estilo.

Leyendo un --todavía accesible a suscriptores-- artículo de LWN llego a
una explicación de porqué RedHat seleccionó \[GNU/\]Linux con GPL en vez
de BSD.

Percibimos que deberíamos pedir permiso a alguien para trabajar con lo
desarrolladores BSD, pero Linus fue tan claro expresando su falta de
interés en intentar tener el control, o siquiera influenciar, lo que
otros hacían con Linux que pareció la opción más simple.

Justo ayer estaba comiendo con Pau y le comenté la importancia de que el
software libre permita libertad, no sólo en el código, sino también en
cómo se relaciona la comunidad de desarrolladores. Así las diferentes
comunidades BSD son bastantes cerradas, hay que ser del grupo de core
developers para participar de pleno derecho, mientras que para Linux
sólo hay que enviar un mensaje a la lista o a algunos de los
"lugartenientes" de Linus.

También opino que el hecho de tener todo un conjunto de herramientas GNU
tan potentes alrededor del núcleo, marcaba la diferencia. Y que toda esa
libertad, falta de control y herramientas potentes fomenta q que la
gente haga virguerías y e innovaciones increíbles \[1\].

\[1\] Hablando de innovaciones, si os gusta o tenéis necesidad de
trabajar con sistemas empotrados o microcontroladores y/o tenéis una
oportunidad de verlo en una presentación, no os lo perdáis, Miguel Angel
Guillén hace verdaderas virguerías con sistemas de este tipo y Linux, y
lo explica de una forma que todo el mundo sale con ganas de dedicarse a
trastear con hardware. Es un genio el tío.

Pero lo más importante, siempre he pensado que la GPL es la que da la
confianza necesaria a la comunidad. Así los desarrolladores se sienten
más seguros que su código no será "apropiado" y que siempre tendrá
acceso a las mejoras que se realicen.

Pero según Bob Young, también la GPL le ayudó en los negocios, vale la
pena leerse la historia. Pero en resumen, la GPL les sirve como método
de protección para evitar que una gran empresa, con mucho dinero
--Microsoft-- pudiese coger su trabajo, apropiárselo de forma gratuita y
sin restricciones --como pasaría como la BSD--, modificarlo y
comercializar un M\$-Linux privativo que dejase fuera del mercado a
RedHat.

No ha necesidad de decir, los términos de la GPL requeriría (al menos
hiptéticamente) que Microsoft dejase disponible a RedHat el código de
cualquier "nueva característica espectacular" que ellos hayan agregado a
M\$-Linux, y por lo tanto nivelando el campo de juego en el sentido de
una competición de características técnicas.

Mira tú, hasta los que se han forrado en el negocio reconocen que la GPL
ha jugado un papel importante. Cito aquí un comentario encontrado en
LWN:

Linus fue invitado a dar una charla a la empresa aerea Finnair. Ellos se
dudaban que \[GNU/\]Linux sea una opción creíble y que pudiese contarse
con él. Linus les contentó, "Sólo RedHat tiene un volumen de negocio
mayor que el vuestro, chavales, y no hace mencionar su capitalización
bursátil". Fin de la discusión.

Así que ya sé la respuesta chula-borde cuando pongan en dudas la
posibilidad de hacer negocios con el software libre :-) . *\' Extraído
de:*\' <http://mnm.uib.es/gallir/posts/2005/10/20/476/>
<http://www.vivalinux.com.ar/biz/redhat-gpl-bsd>

Diferencias entre la licencia BSD y GPL

En este articulo, Pablo Usano se explaya sobre los tipos de licencia de
distribuciones de software. De lectura ilustrativa, en este texto hace
referencia a las liciencias comerciales, BSD y GPL dando su vision de
las caracteristicas de cada una de ellas y el encuadre legislativo (en
espa\~na) que poseen; sin dejar afuera los conceptos de copias ilegales,
freeware y shareware. Algo interesante de remarcar estre GPL (Linux) y
BSD (FreeBSD, NetBSD y OpenBSD) es "LA" diferencia que distingue una de
otra: un programa que es liberado bajo GPL lo estara por siempre, aun si
fuera reescrito, imposibilitando su inclusion en programas "cerrados"
(comerciales).

En tanto un software bajo BSD puede pasar a formar parte de programas
cerrados y luego volver a ser BSD. Sino, MAC OS X no hubiese podido
nacer... (y lo que le debe de agradecer!).

Extraído de: <http://www.vivalinux.com.ar/articulos/2622.html>

Diferencias (-o semejanzas) "BSD -- GNU/Linux"

Ya pasado un tiempo desde que he empezado a husmear freeBSD, aunque
vengo con un poco más de horas sobre PC-BSD, bueno, a que va todo esto?,
simplemente para manifestar algunas diferencias que noté en este tiempo
de experiencia con PC-BSD y como tal con freeBSD frente a la variedad de
distros linux que he utilizado.

Para empezar, cabe destacar que tanto GNU/linux como freeBSD son
descendientes de UNIX (podríamos considerarlos casi primos), como saben,
son muchas las distros que trabajan directamente con el kernel de linux,
como también los hay quienes operan bajo freeBSD, aunque este último no
es portador de una cantidad escandalosa de distribuciones (fork's), hay
tantos sabores de linux como variedad de especies marinas, personalmente
creo que este hecho hace un poco lento el proceso de maduración de linux
al existir miles de mentes dispersas con filosofías y éticas diferentes
y objetivos específicos. FreeBSD, por el contrario nos ofrece el núcleo
y el sistema integrados, y como ya mencionaba, muy pocas variedades, sin
duda, mi favorito es PC-BSD, por la sencillez que presenta y porque
hereda todas las características que el padre freeBSD ofrece.
Actualmente existen sistemas operativos que de alguna manera u otra
están incluidos en la descendencia BSD, como Solaris, Mac OS X ...

Una de las diferencias con GNU/linux lo podemos notar en la estructura
de directorios, freeBSD dispone en la raiz un directorio '/compat', el
cual es un enlace hacia /usr/compat, un directorio que contiene los
programas de linux que se pueden ejecutar en freeBSD.

Así mismo /home contiene los directorios de los usuarios del sistema al
igual que en linux, con la diferencia que en freeBSD es un enlace a
/usr/home y en linux es un directorio único presente en la raíz.

Los archivos de configuración ubicados en /etc son los que freeBSD trae
integrado desde la instalación del sistema, los archivos de
configuración de programas que se instalen posteriormente se ubican en
/usr/local/etc.

En el caso de PC-BSD podemos añadir 2 directorios más a esta jerarquía,
'/PCBSD', es el directorio donde se almacenan los ficheros de
configuración del sistema operativo y el directorio '/Programs', un
enlace que apunta a /usr/Programas, y es la carpeta en donde se
desempaquetarán todos los PBI's que instalemos en PC-BSD.

Todos los demás directorios presentes en la raíz de freeBSD tienen la
misma razón de ser que en GNU/Linux.

Otras de las diferencias en cuanto a GNU/Linux son:

BSD puede ejecutar código de Linux, mientras Linux no puede ejecutar
código de BSD.

BSD ha logrado una mayor reputación como sistema sólido y confiable por
contar con un código base maduro, tal es el caso que grades
organizaciones reconocidas mundialmente han optado por freeBSD como
soporte para sus servidores, algunos ejemplos son :

Servidores Web: Yahoo, Sony Japan,netcraft...

Sistemas Embebidos: IBM, intel, nokia...

Otros: Dysney,NASA,Apache...

Tengo entendido que hasta microsoft y su servidor de correo hotmail
cuentan con la solvencia de FreeBSD.

Algo más:

BSD puede diferenciarse notablemente frente a GNU/Linux en aspectos como
solidez y seguridad, sin desmerecer el crédito que GNU/Linux se ha
ganado.

Puedes hacer que linux comparta la misma swap que freeBSD, de esa manera
ahorras mas espacio en disco si cuentas con ambos en el mismo ordenador.

UFS es el sistema de archivo de BSD, estable y con una velocidad
respetable frente a otros sistemas.

En FreeBSD puede montar otros sistemas de archivos como ext2, fat32,
ntfs, iso9660, etc, claro que también puedes montar sistemas ufs en
linux, pero solo de lectura, aún el soporte para escritura está en fase
experimental.

Y por supuesto, FreeBSD cuenta con entornos de escritorio muy conocidos
en GNU/linux como Gnome y KDE.

Todo lo mencionado anteriormente es mínimo pero relevante para tener una
visión de BSD frente a GNU/Linux, la intención de este post no es que te
cambies a BSD, si nunca te ha ido mal con Linux pues continua sin
problemas, o prueba con BSD y tu mismo decide.

Todos los conocimientos que tengas sobre GNU/Linux son aplicables a BSD
y no es necesario desinstalar tu Linux para poder instalar FreeBSD. Si
has decidido empezar a experimentar con BSD, sin importar que tengas o
no conocimientos de GNU/Linux podrías empezar con PC-BSD, un sistema
operativo sencillo pero con la potencia de FreeBSD.

sAfOrAs::the

Extraído de:
<http://saforas.wordpress.com/2008/02/16/diferencias-o-semejanzas-bsd-gnulinux/>

Comparando GNU/Linux con FreeBSD

La revista Free Software Magazine hace una larga comparación de los dos
sistemas operativos \*nix más populares hoy en día: GNU/Linux,
inispirado en MINIX, y FreeBSD, basado en la Berkeley Software
Distribution (BSD), a su vez, una modificación del UNIX de AT&T hecha
por la Universidad de California.

Quizás la diferencia más importante entre ambos es que, para evitar
problemas legales, el proyecto de FreeBSD decidió hacer una completa
re-ingeniería de BSD original en lugar de copiar su código fuente,
desarrollando todo el sistema operativo; mientras que, al contrario, en
GNU/Linux todos sus componentes son desarrollados independientemente uno
de otro y finalmente son "unidos" por cada una de sus distribuciones
disponibles.

Hay, obviamente, muchas otras diferencias, la mayoría de las cuales se
enumera en esa comparación.

Así, las principales diferencias técnicas entre ambos pueden dividirse
en:

-   Nombres de dispositivos.
-   Niveles de ejecución y scripts de inicialización.
-   Shells (Bash para GNU/Linux, csh para FreeBSD).
-   El Kernel (de hecho, más similitudes que diferencias).
-   "Empaquetamiento" del software disponible (RPMs, DEBs, etc. para
    GNU/Linux; Ports para FreeBSD).

Sin embargo, la acertada conclusión de la comparación es que con los dos
OS libres y abiertos y gratuitos, el verdadero ganador de enfrentar a
ambos es la comunidad.

Extraído de: <http://www.vivabsd.com.ar/distros/linux-vs-freebsd.html>

Comparing GNU/Linux and FreeBSD

GNU/Linux is the most popular operating system built with free/open
source software. However, it is not the only one: FreeBSD is also
becoming popular for its stability, robustness and security. In this
article, I'll take a look at their similarities and differences.
Introduction

FreeBSD is an operating system based on the Berkeley Software
Distribution (BSD), which itself is a modification of AT&T's UNIX, and
was created by the University of California. During the development of
FreeBSD, to avoid any legal problems with the owners of the source code,
the developers decided to re-engineer the original BSD, rather than copy
the source code.

In contrast with GNU/Linux, where all the pieces are developed
separately and brought together in distributions, FreeBSD has been
developed as a complete operating system: the kernel, device drivers,
sysadmin's tools and all the other pieces of software are held in the
same revision control system.

Initial development of Linux was started in 1991 by Linus Torvalds who
used Minix---an operating system developed by Andrew Tanenbaum for
teaching purposes---as the basis for his system. By 1990 the GNU
project, which had been started in 1983 by Richard Stallman, had
produced and collected all the libraries, compilers, text editors,
shells and other software necessary to make a free operating
system---except a kernel. The Linux kernel developers decided to adapt
their kernel to work with the GNU software to make a complete operating
system: GNU/Linux was born.

The kernel and the majority of the code in FreeBSD has been released and
distributed under the BSD license although some components use other
open licenses like the GPL, the LGPL or the ISC. The Linux kernel, and
most of the software in the GNU project, has been licensed under the GNU
GPL which was created by the Free Software Foundation.

Technical differences

FreeBSD and Linux both follow the UNIX philosophy but some differences
do exist between the operating systems---let's have a look at some of
these differences on a technical level. Devices

Hardware related dispositives like disks, network cards, printers,
graphics cards, mice and keyboards are referred to using the term device
in the context of operating systems; Linux and FreeBSD use different
nomenclature for this hardware.

Linux uses predefined names for each device type, so eth0 is the first
Ethernet network card for any chip-set. FreeBSD, on the other hand, uses
a different name for each device and their chip-set: for example, a
single network card with the common RealTek 8129 chip-set is called rl0
in FreeBSD.

On Linux hardware information can be obtained by examining the content
of the /proc directory; a command like lspci or lsusb can also be
used---these commands simply reformat the information contained in
/proc. FreeBSD does not use the /proc directory, rather the sysctl
command shows all the information about the hardware devices attached to
the system and can also be used for configuring and tuning them.
Runlevels and startup scripts

Runlevel is the term used to describe a mode of operation for a system
such as reboot, halt, single-user mode or multi-user mode. On GNU/Linux
the /etc/inittab file describes these different runlevels and the init
process allows the system to change its current runlevel. FreeBSD uses
commands like reboot or shutdown -h to change the current runlevel
instead of the telinit command used in GNU/Linux.

On GNU/Linux each runlevel has a subdirectory under /etc/ or /etc/rc.d/,
depending on the distribution: Debian, for example, uses /etc/. These
subdirectories are rc0.d, rc1.d and so on until the last runlevel (there
are usually seven runlevels). Each rc\_x\_.d subdirectory contains
symbolic links to the startup scripts residing in the /etc/init.d/
directory.

On FreeBSD the startup scripts exist in the /etc/rc.d/ directory (for
the system) and in the /usr/local/etc/rc.d/ directory (for third-party
applications). These scripts use parameters such as start or stop to
control which scripts run at startup (start and reboot) and shutdown.
Kernel

Obviously there exist design differences between the Linux kernel and
the FreeBSD kernel but there are also similarities:

-   Modules: support for loading and unloading modules without
    recompiling the kernel or rebooting the system.
-   Versions: each official kernel uses a numbered version.
-   Build a custom kernel: some benefits of building a custom kernel are
    that it can result in a faster boot time, lower memory usage and
    additional hardware support.

Commands for loading and unloading kernel modules, as well as listing
the loaded modules, are different in each system. Linux uses the
modprobe command for loading a module and for seeing a list of the
loaded modules, lsmod just shows a list of the loaded modules and rmmod
unloads any unwanted modules. FreeBSD uses kldstat to list the currently
loaded modules, kldload for loading a module and kldunload for removing
a module.

The Linux kernel uses tree numbers for each version: the first number
represents the major version number---at the moment this is '2'; the
second number indicates if it is a stable (even number) or development
version (odd number) and the last number is the patch version. You can
see this demonstrated in the most recent version, 2.6.17. In FreeBSD the
kernel has two numbers, first for the major version and second for the
minor new releases: for example the most recent release is 6.1.

Building a custom kernel on either system requires you to compile it
from source. However, the steps for achieving this are different on each
system. The first step is downloading the sources or obtaining it from a
physical medium like a DVD or CD---this step is required in both
systems. Linux offers both GUI and CLI tools for creating your own
custom kernel; FreeBSD uses a text editor to comment or uncomment the
options which control the process. Finally, you use the make command to
compile and install the kernel. Software installation

Third party software can be distributed in binary or source format.
Usually this software is packaged using compression tools such as tar
and gzip; however, many GNU/Linux distributions use their own format for
software packaging and provide tools for installing, un-installing and
configuring it. Debian, for example, uses the .deb package format and
tools like apt or dpkg to manage these. Using these tools the process to
install a software application from the command line is easy; the
following example demonstrates how to install The GIMP image editing
program:

1.  apt-get install gimp

FreeBSD provides two interesting technologies for software installation:

Packages

A package is a single compressed file that contains pre-compiled
binaries, documentation and configuration files, including information
which allows the system to install the software under the correct
directories on the filesystem. Tools like pkg\_add, pkg\_delete,
pkg\_info, etc., are used for package management. To automatically
download and install a package is easy using the pkg\_add command:

1.  pkg\_add -r gimp

Ports

In FreeBSD terminology a port is a collection of files designed to
automate the process of compiling a software application from source
code.

The ability of the software management application to understand
software dependencies is a common feature of both packages and ports.
Ports are very useful when we need to have total control over the
compilation parameters to achieve the best performance for our machine;
whereas, packages have the advantage of being smaller than ports because
they do not include source code. Furthermore, installing a package does
not require the user to have any understanding of the compilation
process. By having both technologies present the user can choose which
ever system is better for them.

You can always install software from source in both operating systems by
using the traditional method of compiling and installing with the make
command. Shells

The Bash (Bourne-Again SHell) is the default shell in most GNU/Linux
distributions. In a default FreeBSD installation, however, you won't
find bash. Don't worry though, you can install it:

1.  pkg\_add -r bash

GNU/Linux distributions choose the bash shell by default because this
was written for the GNU project; FreeBSD uses csh, in line with
traditional UNIX systems.

If you prefer other shells, like tcsh or csh, you can install them in
both operating systems using the previously described software
management systems. Choosing an appropriate shell is a personal decision
and depends on what you have experience with and what your day-to-day
work involves. Installation

As I said before, Linux and FreeBSD can be installed on a number of
different platforms. We can even install both operating systems on the
same machine. Firstly, I will show you the main steps involved when
installing each operating system and then later I will show you how to
install them both on the same machine. Installing GNU/Linux

The procedure to install the operating system is slightly different for
each distribution, but the basic steps are similar:

-   Choose the installation media i.e. DVDs, CDs or network
    installation.
-   Get complete information about the target machine.
-   Start installation.
-   Select your language, country and keyboard layout.
-   Partition the hard drive and choose the filesystem types.
-   Automatic installation of base software.
-   Configure hardware devices and install third party software.

Distributions each have their own tools to carry out these installation
steps and some of them are easier than others: YaST is a complete
administration and installation tool with a graphical user interface
(GUI) which is used by SUSE and other GNU/Linux distributions and is
quite simple to use; Debian, on the other hand, uses another interface
which is less intuitive for novice users. Installing FreeBSD

FreeBSD provides a tool called sysinstall for a wizard-like installation
process: it is a console based application divided into a number of
menus and simple screens to control and configure the system during
installation.

After booting your PC, sysinstall starts and FreeBSD is ready to
install. sysinstall has different options but we want to start with the
installation option. It's recommended to use the "standard" installation
option for most users which you can choose from the sysinstall menu as
shown in figure 1.

The main steps for a FreeBSD installation are:

-   Allocate disk space and install a boot manager: FreeBSD uses slices
    to divide a hard drive.
-   Choose what to install: software is organized into sets, for
    example, the User set will install all the software for an average
    user including binaries and documentation. If in doubt, the All set
    is a better option because it includes all the system sources,
    binaries, documentation and the X-Window system.
-   Choose installation media: sysinstall offers different installation
    media like CD/DVD, FTP, HTTP or NFS.
-   Commit installation: this is your last chance to abort the
    installation without making changes to the machine.
-   Post-installation: the configuration process starts when the
    software is installed. Here you can configure network cards,
    services, FTP servers, time zone and keyboard layout as well as
    other hardware devices.

Installing both GNU/Linux and FreeBSD on the same PC

Both operating systems can live on the same PC, and users are able to
choose one or the other during the boot process. In order to install
them we must consider these issues:

-   Boot manager: it's mandatory to choose one system to run.
-   Disk space: GNU/Linux and FreeBSD use different types of file system
    and organize the hard drives in different ways.

Linux uses the letters hd for IDE hard drives and FreeBSD uses the
letter s for slices, which are portions of the hard drive, so the
organization method on the hard drive is different in each case. In
Linux hda1 is the first partition on the first IDE hard drive; whereas,
in FreeBSD, a slice is divided into several partitions: so the first IDE
drive is ad0 and ad0s1a is partition a in the first slice of the first
IDE hard disk.

The first step to installing both operating systems on the same machine
is to install one---for example GNU/Linux. When you arrive at the
partitioning step, you should create two primary partitions: one for
GNU/Linux and the other for FreeBSD. Remember that a PC can only have
four primary partitions, or three primary partitions and only one
extended partition (divided in several logical partitions, one for each
filesystem type).

A simple partition scheme which is ready for both systems is:

-   One primary partition for boot.
-   One primary partition for root filesystem.
-   One primary partition for swap area.
-   One primary partition for FreeBSD.

Once GNU/Linux is installed we are ready to install FreeBSD in the spare
primary partition. In this partition, which we made with GNU/Linux, you
must create a slice for FreeBSD. This slice will have four partitions:

1\. Partition a for root filesystem. 2. Partition b for swap area. 3.
Partition e for /var filesystem. 4. Partition f for /usr filesystem.

The size of each partition depends on the size of your hard drive: you
can assign space for each partition at your convenience (space is not
much of a problem in modern PCs that usually have hard drives with 80GB
or more).

(Optionally it's possible to share swap partition between FreeBSD and
GNU/Linux, for more information see the Linux+FreeBSD miniHOWTO.)

To boot the operating systems a boot manager is needed. When we install
GNU/Linux we can choose a boot manager like GRUB or LiLo. If we install
and configure GRUB during the GNU/Linux installation process, we don't
need to install any boot manager during the FreeBSD installation
process: we can configure GRUB in our GNU/Linux distribution to boot
FreeBSD as well. This is an example of the file /boot/grub/menu.lst (the
GRUB configuration file) which will boot FreeBSD or Debian:

default 0 timeout 5

title Debian GNU/Linux root (hd0,0) kernel /boot/vmlinuz-2.6.8-2-386
root=/dev/hda1 ro initrd /boot/initrd.img-2.6.8-2-386 savedefault boot

title FreeBSD root (hd0,2,a) kernel /boot/loader

Conclusion

FreeBSD and GNU/Linux are two great options: choosing one or the other
depends on many factors. Usually FreeBSD is used as a web server:
companies like Yahoo! or Sony Japan trust FreeBSD to run their internet
portals; on the desktop GNU/Linux wins this battle, but GNU/Linux is
used on many web servers as well. Users will find if they are familiar
with traditional UNIX systems they can use either without many problems.
FreeBSD and Linux: a gift of quality, robustness, security and stability
from the free software community to the world of operating systems.

Extraído de:
<http://www.freesoftwaremagazine.com/articles/comparing_linux_and_freebsd?page=0%2C2>

\'\'\'Más información: \'\'\' [http://es.wikipedia.org/wiki/GNU/Linux
http://es.wikipedia.org/wiki/Linux
http://es.wikipedia.org/wiki/Berkeley\_Software\_Distribution
http://es.wikipedia.org/wiki/GNU\_General\_Public\_License
http://www.opensource.org/licenses/bsd-license.php
http://www.opensource.org/licenses/gpl-2.0.php
http://es.wikipedia.org/wiki/BSD\_(licencia)
http://www.gnu.org/licenses/license-list.es.html
http://www.gnu.org/philosophy/philosophy.es.html
http://www.freebsd.org/doc/es\_ES.ISO8859-1/articles/explaining-bsd/index.html
http://www.freebsd.org/es/docs.html
http://distrowatch.com/dwres.php?resource=major
http://www.wikipedia.org/ http://www.gnu.org/
http://www.vivalinux.com.ar http://www.vivabsd.com.ar/
http://www.freebsd.org/ http://www.openbsd.org/ http://www.netbsd.org/
http://www.distrowatch.com/ http://www.openbsdsupport.com.ar
http://www.openbsderos.org/ http://www.opensource.org/
http://www.fsf.org/ http://www.fsfla.org/ http://www.fsfeurope.org/
http://www.google.com/ http://saforas.wordpress.com
http://www.freesoftwaremagazine.com](http://es.wikipedia.org/wiki/GNU/Linux_http://es.wikipedia.org/wiki/Linux_http://es.wikipedia.org/wiki/Berkeley_Software_Distribution_http://es.wikipedia.org/wiki/GNU_General_Public_License_http://www.opensource.org/licenses/bsd-license.php_http://www.opensource.org/licenses/gpl-2.0.php_http://es.wikipedia.org/wiki/BSD_(licencia)_http://www.gnu.org/licenses/license-list.es.html_http://www.gnu.org/philosophy/philosophy.es.html_http://www.freebsd.org/doc/es_ES.ISO8859-1/articles/explaining-bsd/index.html_http://www.freebsd.org/es/docs.html_http://distrowatch.com/dwres.php?resource=major_http://www.wikipedia.org/_http://www.gnu.org/_http://www.vivalinux.com.ar_http://www.vivabsd.com.ar/_http://www.freebsd.org/_http://www.openbsd.org/_http://www.netbsd.org/_http://www.distrowatch.com/_http://www.openbsdsupport.com.ar_http://www.openbsderos.org/_http://www.opensource.org/_http://www.fsf.org/_http://www.fsfla.org/_http://www.fsfeurope.org/_http://www.google.com/_http://saforas.wordpress.com_http://www.freesoftwaremagazine.com "wikilink")
