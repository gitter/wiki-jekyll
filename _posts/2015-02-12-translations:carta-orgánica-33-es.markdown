---
title: Translations:Carta Orgánica/33/es
layout: post
categories: []
---

Estando ligada la difusión gratuita de noticias y la de contenidos
reflexivos, sea en forma científica, explícita y metonímica o artística,
implícita y metafórica, a los principios de formación y actualización
del ciudadano, pues en cada uno de ellos recae la soberanía, es que para
defender su ejercicio y control efectivo, defendiendo los principios
constitucionales de gobierno, cada afiliado podrá crear en la red grupos
de noticias y bases de datos educativas con derecho a enlazar los sitios
homólogos.
