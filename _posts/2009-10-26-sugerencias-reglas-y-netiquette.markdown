---
title: Sugerencias reglas y netiquette
layout: post
categories: []
---

Upload de Archivos
==================

Por cuestiones legales y posibles aprietes en la lista se resolvio que
la mejor forma de agregar archivos a la wiki seria subirlos en un sitio
publico y enlazarlos desde aqui.

-   <http://www.megaupload.com/>
-   <http://www.rapidshare.com/>
-   <http://www.2shared.com/>
-   <http://www.4shared.com/dir/22009525/6c531da9/PPArg.html>

Problemas Personales
====================

Al comunicarnos por escrito con personas que no conocemos personalmente
pueden surgir malentendidos, desacuerdos y mas. Es una buena practica
releer cualquier parrafo que encontremos ofensivo y considerar que puede
tener algun significado alternativo, asi mismo se recomienda releer
nuestros mensajes antes de enviarlos para asegurarnos que no haya algun
parrafo/oracion que pueda ser malinterpretado.

En caso de que surga algun malentendido, desacuerdo, etc. se recomienda
tratarlo por privado con esa persona y asi evitar mails innecesarios y
la inclusion de mas personas al conflicto.
