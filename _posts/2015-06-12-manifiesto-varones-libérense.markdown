---
title: Manifiesto Varones Libérense
layout: post
categories:
- Manifiestos
- Traducción
---

[Categoría:Manifiestos](Categoría:Manifiestos "wikilink")
[Categoría:Traducción](Categoría:Traducción "wikilink")

Traducido de [Piratas do
Brasil](https://www.loomio.org/d/lzpaqTMZ/grupo-de-homens-antipatriarcais-projeto-homens-libertem-se)

-   Quiero el fin del Servicio Militar obligatorio
-   Puede ser que no se me pare. El tamaño de mi pija tampoco importa.
-   Puedo fallar. Quiero ser amado por lo que soy y no por lo que tengo.
-   Puedo ser frágil, sentir miedo, pedir ayuda, llorar y gritar cuando
    pase por una situación difícil.
-   Puedo cuidarme, hacer lo que quiera con mi apariencia y mi postura,
    cuidar mi salud, de mi bienestar y hacerme un examen de próstata.
-   Puedo ser sensible y expresar mi sensibilidad como quiera.
-   Puedo ser peluquero, decorador, artista, actor, bailarín; puedo
    maravillarme ante la belleza de una flor o el vuelo de los pájaros.
-   Puedo rehusarme a embriagarme o drogarme.
-   Puedo rehusarme a pelear, ser violento, formar parte de patotas o
    cualquier grupo segregador.
-   Puedo no disfrutar el fútbol o cualquier otro deporte.
-   Puedo manifestar cariño y decir que amo a un amigo. Quiero vivir en
    una sociedad en que los hombres se amen sin que ello sea un tabú.
-   Puedo ser tomado en serio sin tener que usar corbata, puedo usar
    pollera si me siento más cómodo.
-   Puedo cambiar pañales, dar la mamadera y quedarme en casa cuidando a
    los chicos.
-   Puedo dejar a mi hijo vestirse y expresarse lúdicamente como quiera
    y haré todo lo posible para incentivarlo a demostrar sus
    sentimientos, permitiendo que llore cuando tenga ganas.
-   Puedo tratar a mi hija con el mismo respeto, libertad e incentivarla
    de la misma forma que a mi hijo.
-   Puedo admirar a una mujer que me parezca bella con respeto, sin
    griterío en la calle y aproximarme a ella con gentileza, sin
    forzarla a nada.
-   Sé que una mujer viste pollera - o cualquier otra ropa- porque
    quiere y no porque me esté invitando a hacer nada.
-   Sé que una mujer que coje con el que quiera o coje en la primera
    cita no es una puta, al igual que un hombre que lo hace no es un
    macho. Solo son personas que sintieron el deseo de hacerlo.
-   Nunca me cojí a una mina, siempre nos cojimos mutuamente.
-   No temo a que tanto hombres como mujeres tengan poder, y me comporto
    de manera que ningún poder anule al otro.
-   Sé que el feminismo es una lucha por la igualdad entre todos los
    individuos.
-   Nunca voy a pegarle a una mujer, no acepto que ninguna mujer me
    pegue y mi posición es que ningún hombre o mujer crea que tiene el
    derecho de hacerlo.
-   Voy a liberarme, no para oprimir más a las mujeres, sino para que
    podamos ser libres juntos.
-   Fui educado por la sociedad para ser machista y necesito ayuda para
    darme cuenta si estoy oprimiendo a alguien con mis actitudes.
-   No quiero oir más la frase \"¡Sé hombre!\" como si hubiera un modelo
    cerrado a seguir de hombre. No soy una etiqueta cualquiera.
-   Quiero poder ser yo mismo, masculino, femenino, loco, sano, frágil,
    fuerte, todo y nada de eso. Y que me amen y acepten, no por quien
    crean que debo ser, sino por quien soy. Y por todo eso, no soy ni
    más ni menos hombre.
-   Quiero ser más que un hombre, ¡quiero ser humano!
-   ¡El machismo también me oprime y quiero ser un hombre libre!
