---
title: Archivo:Cara libre de copyright.jpg
layout: post
categories: []
---

[Cara libre de
copyright](https://secure.flickr.com/photos/christopherdombres/4565830570/in/photostream)
\-- por [Christopher
Drombres](https://secure.flickr.com/photos/christopherdombres/),
[cc-by](http://creativecommons.org/licenses/by/2.0/deed.en)

[Category:Media](Category:Media "wikilink")
