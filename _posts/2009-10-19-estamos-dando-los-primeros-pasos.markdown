---
title: Estamos dando los primeros pasos
layout: post
categories: []
---

Consolidad una Infraestructura
------------------------------

Lograr una plataforma que permita la comunicación fluida entre nosotros,
y la difusión de las ideas hacia los nuevos visitantes.

-   Para la generación de documentos internos y la clasificación de la
    literatura, es necesaria una wiki
-   Para tener comunicación masiva, y la cara visible con los recién
    llegados, es necesario un Blog
-   Para la comunicación mas ágil, y para poder intercambiar ideas sobre
    algunos temas, un canal de irc (\#ppar en freenode.net)
-   Para discusiones participativas es necesario una lista de Correo y
    un Foro.
-   continuar \...

Definir unos Principios Básicos
-------------------------------

-   [Borradores](Borradores "wikilink") del Manifiesto

Elaborar un Programa
--------------------

-   a definir

\--[200.117.132.43](Especial:Contribuciones/200.117.132.43 "wikilink")
04:12 25 sep 2009 (UTC)
