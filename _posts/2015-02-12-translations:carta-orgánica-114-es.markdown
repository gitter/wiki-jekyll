---
title: Translations:Carta Orgánica/114/es
layout: post
categories: []
---

Es deber de los delegados consultar en forma permanente la Declaración
de Principios y mantenerse en contacto con el resto del Partido Pirata
según los mecanismos definidos en el Título III de este Documento, de
manera de asegurarse que su actuar en representación del Partido sea
fiel a los Principios y accionar propuestos por éste.
